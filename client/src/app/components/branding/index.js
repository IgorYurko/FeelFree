import './branding.styl';
import React from 'react';

export const Branding = ({logo}) => (
    <div className="branding">
        <img src={logo} alt="Feel Free"/>
    </div>
);

