import './footer.styl';
import React, {Component} from 'react';

import {Container} from 'kit/components/container';
import {Nav, NavItem} from 'kit/components/nav';
import {IconButton} from 'kit/components/icon-button';
import {IconTypes} from 'kit/components/icon';
import {Modal} from 'kit/components/modal';
import {NeedHelpForm} from './components/need-help-form';
import autobind from 'autobind-decorator';


@autobind
export class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalOpen: false
        };
    }

    openModal() {
        this.setState({modalOpen: true});
    }

    closeModal() {
        this.setState({modalOpen: false});
    }

    render() {
        return (
            <div className="footer">
                <Container className="footer__container">
                    <Nav className="footer__nav">
                        <a className="footer__nav_link" onClick={this.openModal}>Need help?</a>
                    </Nav>
                    <Nav className="footer__nav footer__nav_align">
                        <NavItem to="/privacy-policy">Privacy Policy</NavItem>
                        <a
                            className="footer__nav_space"
                            href="https://www.facebook.com/Feel-Free-318123325272905"
                            target="_blank"
                        >
                            <IconButton icon={IconTypes.facebook}/>
                        </a>
                        <a
                            className="footer__nav_space"
                            href="https://twitter.com/FeelFree_UA"
                            target="_blank"
                        >
                            <IconButton icon={IconTypes.twitter}/>
                        </a>
                    </Nav>
                </Container>
                <Modal
                    isOpen={this.state.modalOpen}
                    closeModal={this.closeModal}
                    onRequestClose={this.closeModal}
                >
                    <NeedHelpForm closeModal={this.closeModal}/>
                </Modal>
            </div>
        );
    }
}

