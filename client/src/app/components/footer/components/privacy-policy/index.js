import './privacy-policy.styl';
import React from 'react';
import {Container} from 'kit/components/container';

export const PrivacyPolicy = () => (
    <div className="privacy-policy">
        <Container>
            <div>
                <h3 className="privacy-policy_center">Privacy Policy</h3>
                <p/>
                <h4 className="privacy-policy_center">FEEL FREE PRIVACY POLICY</h4>
                <p>Feel Free (hereinafter referred to as “Feel Free”, “we”, “us” or “our”) operates a marketplace that
                    helps
                    people
                    form offline experiences and relationships directly with one another, where they can create, list,
                    discover
                    and
                    book unique accommodations around the world through our website (“Platform”).</p>
                <p>This Privacy Policy is intended to inform you about how we treat Personal Information that we process
                    about
                    you.
                    If you do not agree to any part of this Privacy Policy, then we cannot provide the Platform or
                    Services
                    to
                    you,
                    and you should stop accessing the Platform and deactivate your Feel Free Account. </p>
                <p>WHAT TYPES OF INFORMATION DOES FEEL FREE GATHER ABOUT ITS USERS? Information that you give us</p>
                <p>We receive, store and process information, including Personal Information, that you make available to
                    us
                    when
                    accessing or using our Platform. Examples include when you:</p>
                    <ul>
                        <li>
                            fill in any form on the Platform, such as when you register or update the details of your user
                            account;
                        </li>
                        <li>
                            access or use the Platform, such as to post Accommodations, comments or reviews;
                        </li>
                        <li>
                            link your account on a Third-Party site (e.g. Facebook) to your Feel Free Account, in which case we
                            will
                            obtain
                            the Personal Information that you have provided to the Third-Party site, to the extent allowed by
                            your
                            settings
                            with the Third-Party site and authorized by you;
                        </li>
                    </ul>
                <p/>
                <p>HOW FEEL FREE USES AND PROCESSES THE INFORMATION THAT YOU PROVIDE OR MAKE AVAILABLE </p>
                <p>We use, store and process information about you for the following general purposes:</p>
                <ul>
                <li>to enable you to access and use the Platform;</li>
                <li>to send you service and support messages.</li>
                </ul>
                <p/>
                <p className="privacy-policy_center">HOW TO ACCESS, CHANGE OR DELETE YOUR INFORMATION</p>
                <p>You may review, update, correct or delete the Personal Information in your Feel Free Account. If you
                    would
                    like
                    to correct your information, you can do so by logging in to your Feel Free Account.</p>
                <p className="privacy-policy_center">SECURING YOUR PERSONAL INFORMATION</p>
                <p>We are continuously implementing and updating administrative, technical, and physical security
                    measures
                    to
                    help
                    protect your Personal Information against unauthorized access, destruction or alteration. However,
                    no
                    method
                    of
                    transmission over the Internet, and no method of storing electronic information, can be 100% secure.
                    So,
                    we
                    cannot guarantee the security of your transmissions to us and of your Personal Information that we
                    store.</p>
                <p className="privacy-policy_center">YOUR PRIVACY WHEN YOU ACCESS THIRD-PARTY WEBSITES AND RESOURCES</p>
                <p>The Platform will contain links to other websites not owned or controlled by Feel Free. Feel Free
                    does
                    not
                    have
                    any control over Third-Party websites. These other websites may place their own cookies, web beacons
                    or
                    other
                    files on your device, or collect and solicit Personal Information from you. They will have their own
                    rules
                    about
                    the collection, use and disclosure of Personal Information. We encourage you to read the terms of
                    use
                    and
                    privacy policies of the other websites that you visit.</p>
                <p>Some portions of the Platform implement Google Maps/Earth mapping services, including Google Maps
                    API(s).
                    Your
                    use of Google Maps/Earth is subject to Google’s terms of use (located at
                    www.google.com/intl/en_us/help/terms_maps.html) and Google’s privacy policy (located at
                    www.google.com/privacy.html), as may be amended by Google from time to time.</p>
                <p className="privacy-policy_center">COOKIE POLICY</p>
                <p>Feel Free uses “cookies” in conjunction with the Platform to obtain information. A cookie is a small
                    data
                    file
                    that is transferred to your device (e.g. your computer) for record-keeping purposes. For example, a
                    cookie
                    could
                    allow the Platform to recognize your browser, while another could store your preferences and other
                    information.</p>
                <p>Your browser may allow you to set how it handles cookies, such as declining all cookies or prompting
                    you
                    to
                    decide whether to accept each cookie. But please note that some parts of the Platform may not work
                    as
                    intended
                    or may not work at all without cookies.</p>
                <p className="privacy-policy_center">GOT FEEDBACK?</p>
                <p>Your opinion matters to us! If you’d like to provide feedback to us about this Privacy Policy, please
                    email
                    us. </p>
                <p/>
            </div>
        </Container>
    </div>
);
