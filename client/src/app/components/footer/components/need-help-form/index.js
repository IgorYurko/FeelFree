import './need-help-form.styl';
import React from 'react';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import {sendHelpMessage} from 'app/services/account/actions';
import {Button, ButtonTypes} from 'kit/components/button';
import {InputGroup} from './components/input-group';
import {TextAreaMessage} from './components/textarea-message';
import {validate} from './validate';
import classNames from 'classnames';
import {FormMessage, FormMessageTypes} from 'app/components/form-message';

export const NeedHelpForm = connect(null, {
    sendHelpMessage
})(reduxForm({
    form: 'helpRequest',
    validate
})(props => {

    const submit = values => {
        props.sendHelpMessage(values);
        props.reset();
    };

    return (
        <form onSubmit={props.handleSubmit(submit)} className="help-form">
            {props.submitSucceeded && (
                <FormMessage
                    message="Thank you for your letter, we will contact you soon"
                    type={FormMessageTypes.success}
                    className="help-form__form-message"
                />
            )}
            <div className={classNames({'help-form_hide': props.submitSucceeded})}>
                <h4>Email</h4>
                <Field name="email" component={InputGroup} label="Email"/>
                    <h4>Title</h4>
                    <Field name="title" component={InputGroup} label="Title"/>
                    <h4>Message</h4>
                    <Field name="messageBody" component={TextAreaMessage} label="Message"/>
                    <Button
                        buttonType={ButtonTypes.accent}
                        caption="Send"
                        type="submit"
                        className="help-form__button"
                    />
            </div>
        </form>
    );
}));

