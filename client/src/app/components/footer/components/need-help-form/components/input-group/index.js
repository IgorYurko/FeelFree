import React from 'react';
import {Input, InputTypes} from 'kit/components/input';
import {ValidationError} from 'kit/components/validation-error';

export const InputGroup = ({ input, label, type, meta: {error, touched} }) => (
    <div>
    <Input
        type={type}
        {...input}
        placeholder={label}
        inputType={InputTypes.primary}
    />
    {touched && error && <ValidationError>{error}</ValidationError>}
    </div>
);
