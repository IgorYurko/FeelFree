import './request-list.styl';
import React, {Component} from 'react';
import {isEmpty} from 'lodash';
import moment from 'moment';
import autobind from 'autobind-decorator';
import classNames from 'classnames';

import {Spinner} from 'kit/components/spinner';
import {Button, ButtonTypes, ButtonSizes} from 'kit/components/button';

@autobind
export class RequestList extends Component {
    constructor() {
        super();

        this.state = {
            requests: [],
            loading: false
        };
    }

    componentDidMount() {
        const {getRentRequests, announcement} = this.props;
        this.setState({loading: true});

        getRentRequests(announcement.uuid).then(obj => {
            this.setState({requests: obj.response, loading: false});
        });
    }

    confirmRentRequest(id) {
        const {confirmRentRequest, announcement} = this.props;

        confirmRentRequest({
            id,
            uuid: announcement.uuid,
            status: 'CONFIRMED'
        }).then(json => {
            const index = this.state.requests.findIndex(request => request.id === json.response.id);

            this.setState({
                requests: [
                    ...this.state.requests.slice(0, index),
                    ...this.state.requests.slice(index + 1)
                ]
            })
        })
    }

    rejectRentRequest(id) {
        const {rejectRentRequest, announcement} = this.props;

        rejectRentRequest({
            id,
            uuid: announcement.uuid,
            status: 'REJECTED'
        }).then(json => {
            const index = this.state.requests.findIndex(request => request.id === json.response.id);

            this.setState({
                requests: [
                    ...this.state.requests.slice(0, index),
                    ...this.state.requests.slice(index + 1)
                ]
            })
        })
    }

    render() {
        const {requests, loading} = this.state;

        return (
            <div className="request-list">
                <h3>Requests</h3>
                {loading ?
                    (
                        <div className="request-list__spinner-container"><Spinner/></div>
                    )
                    :
                    (
                        <div>
                            {isEmpty(requests) ?
                                (<p>No requests</p>)
                                :
                                (
                                    <ul className="request-list__list">
                                        {requests.map(request => (
                                            <li key={request.id} className={
                                                classNames('request-list__request', {
                                                    'request-list__request_rejected': request.status === 'REJECTED',
                                                    'request-list__request_confirmed': request.status === 'CONFIRMED'
                                                })
                                            }>
                                                <div>
                                                    <div className="request-list__from">
                                                    <span className="request-list__label">
                                                        From:
                                                    </span> {request.user.firstName}
                                                    </div>
                                                    <div>
                                                        <span className="request-list__label">Dates: </span>
                                                        from {moment(request.rents[0]).format('MMMM Do YYYY')}{' '}
                                                        to {moment(request.rents[request.rents.length - 1])
                                                        .format('MMMM Do YYYY')}
                                                    </div>
                                                </div>
                                                <div>
                                                    <Button
                                                        buttonType={ButtonTypes.accent}
                                                        buttonSize={ButtonSizes.small}
                                                        caption="Accept"
                                                        onClick={() => {
                                                            this.confirmRentRequest(request.id)
                                                        }}
                                                    />
                                                    <Button
                                                        className="request-list__reject-btn"
                                                        buttonType={ButtonTypes.default}
                                                        buttonSize={ButtonSizes.small}
                                                        caption="Reject"
                                                        onClick={() => {
                                                            this.rejectRentRequest(request.id)
                                                        }}
                                                    />
                                                </div>
                                            </li>
                                        ))}
                                    </ul>
                                )
                            }
                        </div>
                    )
                }
            </div>
        );
    }
};
