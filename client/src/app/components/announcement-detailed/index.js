import './announcement-detailed.styl';
import React, {Component} from 'react';
import classNames from 'classnames';
import autobind from 'autobind-decorator';

import {RequestList} from './components/request-list';

import {Carousel, CarouselTypes} from 'kit/components/carousel';
import {Link} from 'kit/components/link';
import {Icon, IconTypes} from 'kit/components/icon';
import {Button, ButtonTypes, ButtonSizes} from 'kit/components/button';
import {Modal} from 'kit/components/modal';

@autobind
export class AnnouncementDetailed extends Component {
    constructor() {
        super();

        this.state = {
            requestsModalIsOpen: false
        }
    }

    openModal() {
        this.setState({requestsModalIsOpen: true});
    }

    closeModal() {
        this.setState({requestsModalIsOpen: false});
    }

    render() {
        const {announcement, addFavourite, removeFavourite, toEdit, deleteAnnouncement,
            toggleHideAnnouncement, getRentRequests, confirmRentRequest, rejectRentRequest} = this.props;

        const favouriteClasses = classNames('announcement-detailed__favourite', {
            'announcement-detailed__favourite_true': announcement.favorite === 'TRUE',
            'announcement-detailed__favourite_false': announcement.favorite === 'FALSE'
        });

        const toggleFavourite = () => {
            if (announcement.favorite === 'TRUE') {
                removeFavourite(announcement.uuid);
            } else if (announcement.favorite === 'FALSE') {
                addFavourite(announcement.uuid);
            }
        };

        const canFavourite = announcement.favorite && announcement.favorite !== 'UNDEFINED';

        const announcementClasses = classNames('announcement-detailed', {
            'announcement-detailed_hidden': announcement.hidden
        });

        return (
            <div className={announcementClasses}>
                <div className="announcement-detailed__preview">
                    <Carousel
                        type={CarouselTypes.small}
                        imgUrls={announcement.images.map(image => image.value)}
                    />
                    {canFavourite && (
                        <div
                            className={favouriteClasses}
                            onClick={() => {
                                toggleFavourite();
                            }}
                        >
                            <Icon type={IconTypes.heart}/>
                        </div>
                    )}
                </div>
                <div className="announcement-detailed__info">
                    <h3 className="announcement-detailed__title">{announcement.title}</h3>
                    <div className="announcement-detailed__price">
                        <span className="announcement-detailed__label">Price:</span> {' '}
                        {announcement.price.value}$ / {announcement.price.period.toLowerCase()}
                    </div>
                    <div className="announcement-detailed__location">
                        <span className="announcement-detailed__label">Location:</span>{' '}
                        {[
                            announcement.address.city,
                            announcement.address.region,
                            announcement.address.country
                        ].filter(term => !!term).join(', ')}
                    </div>
                    <p className="announcement-detailed__description">
                        <span className="announcement-detailed__label">Description:</span> {announcement.shortDescription}
                    </p>
                    <div className="announcement-detailed__more">
                        <Link to={`/announcement/${announcement.uuid}`}>MORE</Link>
                    </div>
                    {announcement.hidden !== undefined && (
                        <div className="announcement-detailed__controls">
                            <div>
                                <Button
                                    className="announcement-detailed__edit-btn"
                                    buttonType={ButtonTypes.accent}
                                    buttonSize={ButtonSizes.small}
                                    caption="Edit"
                                    onClick={() => {
                                        toEdit(announcement.uuid);
                                    }}
                                />
                                <Button
                                    buttonType={announcement.hidden ? ButtonTypes.accent : ButtonTypes.default}
                                    buttonSize={ButtonSizes.small}
                                    caption={announcement.hidden ? 'Show' : 'Hide'}
                                    onClick={() => {
                                        toggleHideAnnouncement({
                                            uuid: announcement.uuid,
                                            hidden: !announcement.hidden
                                        });
                                    }}
                                />
                            </div>
                            <div>
                                <Button
                                    buttonType={ButtonTypes.accent}
                                    buttonSize={ButtonSizes.small}
                                    caption="Requests"
                                    onClick={this.openModal}
                                />
                                <Button
                                    className="announcement-detailed__delete-btn"
                                    buttonType={ButtonTypes.default}
                                    buttonSize={ButtonSizes.small}
                                    caption="Delete"
                                    onClick={() => {
                                        deleteAnnouncement(announcement.uuid);
                                    }}
                                />
                            </div>
                        </div>
                    )}
                </div>
                <Modal
                    isOpen={this.state.requestsModalIsOpen}
                    closeModal={this.closeModal}
                    onRequestClose={this.closeModal}
                >
                    <RequestList
                        announcement={announcement}
                        getRentRequests={getRentRequests}
                        confirmRentRequest={confirmRentRequest}
                        rejectRentRequest={rejectRentRequest}
                    />
                </Modal>
            </div>
        );
    }
}
