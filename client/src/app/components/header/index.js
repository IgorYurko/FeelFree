import './header.styl';
import React from 'react';

import {UserControl} from '../user-control';
import {Branding} from '../branding';
import {Container} from 'kit/components/container';
import {Nav, NavItem} from 'kit/components/nav';
import {Link} from 'kit/components/link';

export const Header = ({logo, user, location}) => (
    <div className="header">
        <Container className="header__container">
            <Link to="/home">
                <Branding logo={logo}/>
            </Link>
            <div className="header__controls">
                <Nav className="header__nav">
                    <NavItem to="/home">Home</NavItem>
                    {/*<NavItem to="/demo">Demo</NavItem>*/}
                </Nav>
                <UserControl user={user} location={location}/>
            </div>
        </Container>
    </div>
);

