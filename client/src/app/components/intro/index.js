import './intro.styl';
import React from 'react';

export const Intro = ({children}) => (
    <div className="intro">
        {children}
    </div>
);

