import './announcement-preview.styl';
import React from 'react';
import classNames from 'classnames';

import {Carousel, CarouselTypes} from 'kit/components/carousel';
import {Link} from 'kit/components/link';
import {Icon, IconTypes} from 'kit/components/icon';

export const AnnouncementPreview = ({announcement, addFavourite, removeFavourite}) => {
    const favouriteClasses = classNames(
        'announcement-preview__favourite',
        {
            'announcement-preview__favourite_true': announcement.favorite === 'TRUE',
            'announcement-preview__favourite_false': announcement.favorite === 'FALSE'
        }
    );

    const toggleFavourite = () => {
        if (announcement.favorite === 'TRUE') {
            removeFavourite(announcement.uuid);
        } else if (announcement.favorite === 'FALSE') {
            addFavourite(announcement.uuid);
        }
    };

    const canFavourite = announcement.favorite && announcement.favorite !== 'UNDEFINED';

    return (
        <div className="announcement-preview">
            <Carousel
                type={CarouselTypes.small}
                imgUrls={announcement.images.map(image => image.value)}
            />
            {canFavourite && (
                <div
                    className={favouriteClasses}
                    onClick={() => {
                        toggleFavourite();
                    }}
                >
                    <Icon type={IconTypes.heart}/>
                </div>
            )}
            <div className="announcement-preview__price">
                {announcement.price.value}$ / {announcement.price.period.toLowerCase()}
            </div>
            <div className="announcement-preview__city">
                {announcement.address.city}
            </div>
            <Link to={`/announcement/${announcement.uuid}`}>
                <h3 className="announcement-preview__title">{announcement.title}</h3>
            </Link>
        </div>
    );
};
