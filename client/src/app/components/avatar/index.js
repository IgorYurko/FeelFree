import './avatar.styl';
import React, {PropTypes} from 'react';
import classNames from 'classnames';

import defaultAvatar from './default-avatar.png';

export const AvatarTypes = {
    small: 'small',
    medium: 'medium',
    large: 'large'
};

const Classes = {
    [AvatarTypes.small]: 'avatar_small',
    [AvatarTypes.medium]: 'avatar_medium',
    [AvatarTypes.large]: 'avatar_large'
};

export const Avatar = ({image, type}) => {
    const avatarClassNames = classNames('avatar', Classes[type]);

    return <img className={avatarClassNames} src={image ? image.value : defaultAvatar} alt="User Avatar"/>;
};

Avatar.propTypes = {
    type: PropTypes.string.isRequired,
    image: PropTypes.oneOfType([
        React.PropTypes.object,
        React.PropTypes.null
    ])
};

Avatar.defaultProps = {
    type: AvatarTypes.small,
    image: null
};
