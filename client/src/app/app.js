import 'kit/stylus/main.styl';
import React from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import {KitDemo} from './modules/kit-demo';
import {Root} from './modules/root';

import ScrollToTop from './components/scroll-to-top';

import store from './store';

export const App = () => (
    <Provider store={store}>
        <Router>
            <ScrollToTop>
                <Switch>
                    <Route path="/demo" component={KitDemo}/>
                    <Route path="/" component={Root}/>
                </Switch>
            </ScrollToTop>
        </Router>
    </Provider>
);


