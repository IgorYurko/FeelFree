import './period-toggle.styl';
import React from 'react';
import {Toggle} from 'kit/components/toggle';

export const PeriodToggle = ({input, name, toggleType, toggleLeft, toggleRight, value}) => (
    <Toggle
        className="search-form__toggle"
        {...input}
        name={name}
        toggleType={toggleType}
        toggleLeft={toggleLeft}
        toggleRight={toggleRight}
        value={value}
    />
);
