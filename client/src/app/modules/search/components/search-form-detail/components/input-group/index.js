import './input-group.styl';
import React from 'react';
import {Input, InputTypes} from 'kit/components/input';

export const InputGroup = ({ input, label, type }) => (
    <Input
        type={type}
        {...input}
        className="search-form__input"
        placeholder={label}
        inputType={InputTypes.primary}
    />
);
