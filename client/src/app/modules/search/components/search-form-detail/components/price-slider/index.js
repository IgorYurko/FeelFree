import React from 'react';
import {RangeSlider} from 'kit/components/range-slider';

export const PriceSlider = ({input, name, minValue, maxValue, step}) => (
    <RangeSlider
        {...input}
        name={name}
        minValue={minValue}
        maxValue={maxValue}
        step={step}
    />
);
