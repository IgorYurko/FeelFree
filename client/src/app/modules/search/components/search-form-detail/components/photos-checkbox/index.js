import './photos-checkbox.styl';
import React from 'react';
import {Checkbox} from 'kit/components/checkbox';

export const PhotosCheckbox = ({ input, type }) => (
    <Checkbox
        type={type}
        {...input}
        className="search-form__checkbox"
    />
);
