export const validate = values => {
    const errors = {};
    if (!values.profileEmail) {
        errors.profileEmail = 'Please enter valid email address';
    }
    if (!values.password) {
        errors.password = 'Please enter password';
    }
    return errors;
};
