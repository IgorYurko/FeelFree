import './registration-form.styl';
import React from 'react';
import {reduxForm, Field} from 'redux-form';

import {register} from 'app/services/registration/requests';

import {validate} from './validate';

import {ConfirmCheckbox, InputGroup} from './components';

import {FormMessage, FormMessageTypes} from 'app/components/form-message';
import {Button, ButtonTypes} from 'kit/components/button';

export const RegistrationForm = reduxForm({
    form: 'registration',
    validate,
    initialValues: {
        terms: false
    }
})(props => {
    const submit = values => {
        return register(values).then(() => {
            props.reset();
        });
    };

    return (
        <div>
        <form className="register-form" onSubmit={props.handleSubmit(submit)}>
            <h4 className="register-form__title">Register</h4>

            {props.submitSucceeded && (
                <FormMessage
                    message="Success, please check your email"
                    type={FormMessageTypes.success}
                    className="register-form__form-message"
                />
            )}

            {props.error && (
                <FormMessage message={props.error} type={FormMessageTypes.error} className="login-form__form-message"/>
            )}

            {!props.submitSucceeded && (
                <div>
                    <Field name="firstName" component={InputGroup} label="First Name"/>
                    <Field name="lastName" component={InputGroup} label="Last Name"/>
                    <Field name="profileEmail" component={InputGroup} label="Email*"/>
                    <Field name="password" component={InputGroup} label="Password*" type="password"/>
                    <Field name="passwordConfirm" component={InputGroup} label="Repeat password*" type="password"/>
                    <Field name="terms" component={ConfirmCheckbox} type="checkbox" label="Checkbox"/>
                    <Button
                        type="submit"
                        className="register-form__button-primary"
                        caption="Register"
                        buttonType={ButtonTypes.default}
                        busy={props.submitting}
                        disabled={props.submitting}
                    />
                    <div className="register-form__login">
                        <p className="register-form__login-caption" href="#">Already have account?</p>
                        <Button caption="Login" buttonType={ButtonTypes.default} onClick={props.toLogin}/>
                    </div>
                </div>
            )}
        </form>
            {!props.submitSucceeded && (
                <div>
        <form action="http://feelfree.ddns.net:8060/signin/google" method="post" className="register-form__google">
            <Button
                caption="Sign in with google"
                buttonType={ButtonTypes.primary}
                secondary={true}
                type="submit"
            />
        </form>
        <form action="http://feelfree.ddns.net:8060/signin/facebook" method="post" className="register-form__facebook">
            <Button
                caption="Sign in with facebook"
                buttonType={ButtonTypes.primary}
                ternary={true}
                type="submit"
            />
        </form>
                </div>)}
        </div>
    );
});


