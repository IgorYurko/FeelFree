import React from 'react';
import {Checkbox} from 'kit/components/checkbox';
import {ValidationError} from 'kit/components/validation-error';

export const ConfirmCheckbox = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div className="register-form__confirm-container">
        <div className="register-form__confirm">
            <Checkbox {...input} className="register-form__confirm-checkbox"/>
            <p className="register-form__checkbox-caption">
                By signing up, I agree to Fell Free Terms of Service, Nondiscrimination Policy, Payments Terms of
                Service, <a href="/privacy-policy">Privacy Policy</a>, Guest Refund Policy, and Host Guarantee Terms.
            </p>
        </div>
        {touched && error && <ValidationError>{error}</ValidationError>}
    </div>
);
