import './confirm-registration.styl';
import React, {Component} from 'react';

import {FormMessage, FormMessageTypes} from 'app/components/form-message';

import {Link} from 'kit/components/link';

const CONFIRMATION_SUCCESS = 'success';
const CONFIRMATION_FAILURE = 'error';
const REDIRECTION_DELAY = 4000;

const getCookie = name => {
    let value = '; ' + document.cookie;
    let parts = value.split('; ' + name + '=');
    if (parts.length === 2) return parts.pop().split(';').shift();
}

export class ConfirmRegistration extends Component {
    constructor(props) {
        super(props);

        this.redirectionTimeout = null;

        this.state = {
            errorMessage: '',
            successMessage: ''
        }
    }

    componentDidMount() {
        // const {status} = this.props.match.params;

        const cookieError = getCookie('error_message');
        const cookieSuccess = getCookie('success_message');

        if (cookieError) {
            this.setState({errorMessage: cookieError})
            return;
        }
        if (cookieSuccess) {
            this.setState({successMessage: cookieSuccess})
        }

        // if (status === CONFIRMATION_SUCCESS) {
        //     this.props.history.push('/home');
        // }
    }

    componentWillUnmount() {
        clearTimeout(this.redirectionTimeout);
    }

    render() {
        const {status} = this.props.match.params;

        return (
            <div className="registration-confirm">
                {status === CONFIRMATION_SUCCESS && (
                    <div>
                        <FormMessage
                            type={FormMessageTypes.success}
                            message={atob(this.state.successMessage) || (
                                <div>
                                    Your account has been activated, now you can <Link to="/login">Login</Link>
                                </div>
                            )}
                        />
                    </div>
                )}
                {status === CONFIRMATION_FAILURE && (
                    <div>
                        <FormMessage
                            type={FormMessageTypes.error}
                            message={atob(this.state.errorMessage) || 'ACCOUNT ACTIVATION ERROR'}
                        />
                    </div>
                )}
            </div>
        );
    }

}
