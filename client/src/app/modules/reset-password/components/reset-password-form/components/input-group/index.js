import React from 'react';
import {Input, InputTypes} from 'kit/components/input';
import {ValidationError} from 'kit/components/validation-error';

export const InputGroup = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div className="reset-password-form__input-group">
        <Input
            type={type}
            {...input}
            className="reset-password-form__input"
            placeholder={label}
            inputType={InputTypes.primary}
        />
        {touched && error && <ValidationError>{error}</ValidationError>}
    </div>
);
