import './reset-password-form.styl';
import React from 'react';
import {reduxForm, Field} from 'redux-form';

import {resetPassword} from 'app/services/user/requests';

import {validate} from './validate';

import {InputGroup} from './components/input-group';
import {FormMessage, FormMessageTypes} from 'app/components/form-message';
import {Button, ButtonTypes} from 'kit/components/button';

export const ResetPasswordForm = reduxForm({
    form: 'resetPassword',
    validate
})(props => {
    const submit = values => {
        return resetPassword(values);
    };

    return (
        <form className="reset-password-form" onSubmit={props.handleSubmit(submit)}>
            <h4 className="reset-password-form__title">Reset password</h4>

            {props.submitSucceeded && (
                <FormMessage
                    message="Success, check your email"
                    type={FormMessageTypes.success}
                    className="login-form__form-message"
                />
            )}

            {props.error && (
                <FormMessage message={props.error} type={FormMessageTypes.error} className="login-form__form-message"/>
            )}

            {!props.submitSucceeded && (
                <div>
                    <Field name="email" component={InputGroup} label="Email"/>

                    <Button
                        type="submit"
                        className="reset-password-form__button-primary"
                        caption="Reset password"
                        buttonType={ButtonTypes.default}
                    />
                </div>
            )}
        </form>
    );
});

