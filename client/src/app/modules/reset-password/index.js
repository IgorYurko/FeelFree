import './reset-password.styl';
import React, {Component} from 'react';

import {ResetPasswordForm} from './components/reset-password-form';

export class ResetPassword extends Component {
    render() {
        return (
            <div className="reset-password">
                <ResetPasswordForm/>
            </div>
        );
    }
}
