import './user.styl';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import autobind from 'autobind-decorator';
import {Route, Redirect, Switch} from 'react-router-dom';

import {logout, getUser} from 'app/services/user/actions';

import {UserSidebar} from './components/user-sidebar';
import {UserProfile} from './components/user-profile';
import {UserMessages} from './components/user-messages';
import {UserFavourites} from './components/user-favouritess';
import {UserAnnouncements} from './components/user-announcements';
import {AddAnnouncement} from './components/add-announcement';
import {EditAnnouncement} from './components/edit-announcement';

import {Container} from 'kit/components/container';

export const User = connect(state => ({user: state.user}), {
    logout, getUser
})(class extends Component {
    @autobind
    logout() {
        this.props.logout();
    }

    @autobind
    updateUser() {
        this.props.getUser();
    }

    render() {
        const {user} = this.props;

        return (
            <div className="user">
                <Container className="user__container">
                    <UserSidebar user={user}/>
                    <div className="user__main">
                        <Switch>
                            <Route
                                path="/user/profile"
                                component={withProps(UserProfile, {user, updateUser: this.updateUser})}
                            />
                            <Route path="/user/messages" component={UserMessages}/>
                            <Route path="/user/favourites" component={UserFavourites}/>
                            <Route path="/user/announcements" component={UserAnnouncements}/>

                            <Route path="/user/add" component={AddAnnouncement}/>
                            <Route path="/user/edit/:id" component={EditAnnouncement}/>

                            <Redirect to="/user/profile"/>
                        </Switch>
                    </div>
                </Container>

                <Route path="/user/logout" component={logoutWrap(this.logout)}/>
            </div>
        );
    }
});

const withProps = (component, props) => () => React.createElement(component, {...props});

const logoutWrap = logoutHandler => {
    return class extends Component {
        componentDidMount() {
            logoutHandler();
        }

        render() {
            return <Redirect to="/home"/>;
        }
    };
};
