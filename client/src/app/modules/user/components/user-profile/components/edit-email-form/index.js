import './edit-email-form.styl';
import React from 'react';
import {reduxForm, Field} from 'redux-form';

import {editEmail} from 'app/services/user/requests';

import {validate} from './validate';

import {InputGroup} from '../input-group';

import {FormMessage, FormMessageTypes} from 'app/components/form-message';
import {Button, ButtonTypes} from 'kit/components/button';

export const EditEmailForm = reduxForm({
    form: 'editEmail',
    validate
})(props => {
    const submit = values => {
        return editEmail(values).then(() => {
            props.reset();
        });
    };

    return (
        <form className="edit-email-form" onSubmit={props.handleSubmit(submit)}>
            <h4 className="edit-email-form__title">Edit email</h4>

            {props.submitSucceeded && (
                <FormMessage
                    message="Success, check your new email for confirmation letter"
                    type={FormMessageTypes.success}
                    className="edit-email__form-message"
                />
            )}

            {props.error && (
                <FormMessage message={props.error} type={FormMessageTypes.error} className="edit-email__form-message"/>
            )}

            <div>
                <Field name="email" component={InputGroup} label="New Email*"/>

                <Button
                    type="submit"
                    className="edit-email__submit-button"
                    caption="Change email"
                    buttonType={ButtonTypes.accent}
                    busy={props.submitting}
                    disabled={props.submitting}
                />
            </div>
        </form>
    );
});

