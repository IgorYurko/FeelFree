export const validate = values => {
    const errors = {};
    if (!values.oldPassword) {
        errors.oldPassword = 'Please enter your old password';
    }
    if (!values.password) {
        errors.password = 'Please enter new password';
    } else if (values.password.length < 7 || values.password.length > 15) {
        errors.password = 'Your new password must be 7-15 characters';
    } else if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{1,}$/.test(values.password)) {
        errors.password = 'Your new password is not strong enough';
    }
    if (!values.passwordConfirm) {
        errors.passwordConfirm = 'Please repeat your new password';
    } else if (values.passwordConfirm !== values.password) {
        errors.passwordConfirm = 'Password does not match';
    }
    return errors;
};
