import './edit-password-form.styl';
import React from 'react';
import {reduxForm, Field} from 'redux-form';

import {editPassword} from 'app/services/user/requests';

import {validate} from './validate';

import {InputGroup} from '../input-group';

import {FormMessage, FormMessageTypes} from 'app/components/form-message';
import {Button, ButtonTypes} from 'kit/components/button';

export const EditPasswordForm = reduxForm({
    form: 'editPassword',
    validate
})(props => {
    const submit = values => {
        return editPassword(values).then(() => {
            props.reset();
        });
    };

    return (
        <form className="edit-password-form" onSubmit={props.handleSubmit(submit)}>
            <h4 className="edit-password-form__title">Edit password</h4>

            {props.submitSucceeded && (
                <FormMessage
                    message="Success, your password has been changed"
                    type={FormMessageTypes.success}
                    className="edit-password__form-message"
                />
            )}

            {props.error && (
                <FormMessage
                    message={props.error}
                    type={FormMessageTypes.error}
                    className="edit-password__form-message"
                />
            )}

            <div>
                <Field name="oldPassword" component={InputGroup} label="Old Password*" type="password"/>
                <Field name="password" component={InputGroup} label="New Password*" type="password"/>
                <Field name="passwordConfirm" component={InputGroup} label="Repeat new password*" type="password"/>

                <Button
                    type="submit"
                    className="edit-password__submit-button"
                    caption="Change password"
                    buttonType={ButtonTypes.accent}
                />
            </div>
        </form>
    );
});

