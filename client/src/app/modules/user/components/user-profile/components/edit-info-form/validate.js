import {isEmpty} from 'lodash';

export const validate = values => {
    const errors = {};
    if (values.firstName && values.firstName.length > 20) {
        errors.firstName = 'Can\'t be longer then 20 character';
    }
    if (values.lastName && values.lastName.length > 20) {
        errors.lastName = 'Can\'t be longer then 20 character';
    }

    if (!isEmpty(values.emails)) {
        const emailsArrayErrors = [];

        values.emails.forEach((email, index) => {
            if (!/^[_A-Za-z0-9-\+]+(\..[_A-Za-z0-9-]+)*@[A-Za-zА-Яа-я0-9-_]+(\.[A-Za-zА-Яа-я0-9-_]+)*(\.[A-Za-zА-Яа-я0-9-_]{2,})*([^0-9!@#$%+^&*\(\);:\'\"\\?.,/\[\]{}<>№ ]+)$/i.test(email)) {
                emailsArrayErrors[index] = 'Please enter valid email address';
            }
            if (!email) {
                emailsArrayErrors[index] = 'Cant be empty';
            }
        })

        if (emailsArrayErrors.length) {
            errors.emails = emailsArrayErrors;
        }
    }

    if (!isEmpty(values.phones)) {
        const phonesArrayErrors = [];

        values.phones.forEach((phone, index) => {

            if (phone && phone.length > 15) {
                phonesArrayErrors[index] = 'Can\'t be longer then 15 character';
            }
        })

        if (phonesArrayErrors.length) {
            errors.phones = phonesArrayErrors;
        }
    }
    return errors;
};
