import './edit-info-form.styl';
import React from 'react';
import {reduxForm, Field, FieldArray} from 'redux-form';

import {editInfo} from 'app/services/user/requests';

import {validate} from './validate';

import {InputGroup} from '../input-group';
import {RenderFields} from '../render-fields';

import {FormMessage, FormMessageTypes} from 'app/components/form-message';
import {Button, ButtonTypes} from 'kit/components/button';



const trim = value => value.trim();

export const EditInfoForm = reduxForm({
    form: 'editInfo',
    validate
})(props => {
    const submit = values => {
        const newValues = {
            ...values,
            emails: values.emails.map(email => ({value: email})),
            phones: values.phones.map(phone => ({value: phone}))
        };

        return editInfo(newValues).then(() => {
            props.updateUser();
        });
    };

    return (
        <form className="edit-info-form" onSubmit={props.handleSubmit(submit)}>
            <h4 className="edit-info-form__title">Edit Info</h4>

            {props.submitSucceeded && (
                <FormMessage
                    message="Success, your Info has been changed"
                    type={FormMessageTypes.success}
                    className="edit-info__form-message"
                />
            )}

            {props.error && (
                <FormMessage message={props.error} type={FormMessageTypes.error} className="edit-info__form-message"/>
            )}

            <div>
                <Field name="firstName" component={InputGroup} label="First Name" normalize={trim}/>
                <Field name="lastName" component={InputGroup} label="Last Name" normalize={trim}/>
                <FieldArray name="emails" component={RenderFields('email')} label="Emails"/>
                <FieldArray name="phones" component={RenderFields('phone')} label="Phones"/>

                <Button
                    type="submit"
                    className="edit-info__submit-button"
                    caption="Change Info"
                    buttonType={ButtonTypes.accent}
                />
            </div>
        </form>
    );
});

