import './user-sidebar.styl';
import React, {Component} from 'react';
import autobind from 'autobind-decorator';

import {ChangeAvatarForm} from './components/change-avatar-form';

import {Avatar, AvatarTypes} from 'app/components/avatar';
import {Nav, NavItem} from 'kit/components/nav';
import {Modal} from 'kit/components/modal';

@autobind
export class UserSidebar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalOpen: false
        };
    }

    openModal() {
        this.setState({modalOpen: true});
    }

    closeModal() {
        this.setState({modalOpen: false});
    }

    render() {
        const {user} = this.props;

        return (
            <div className="user-sidebar">
                <div>
                    <div className="user-sidebar__avatar-container" onClick={this.openModal}>
                        <Avatar type={AvatarTypes.large} image={user.data.image} className="user-sidebar__avatar"/>
                    </div>
                    <div>{user.data.firstName} {user.data.lastName}</div>
                    <Nav className="user-sidebar__nav">
                        <NavItem to="/user/profile">Profile</NavItem>
                        <NavItem to="/user/messages">Messages</NavItem>
                        <NavItem to="/user/favourites">Favourites</NavItem>
                        <NavItem to="/user/announcements">Announcements</NavItem>
                    </Nav>
                </div>
                <Modal isOpen={this.state.modalOpen} closeModal={this.closeModal} onRequestClose={this.closeModal}>
                    <ChangeAvatarForm/>
                </Modal>
            </div>
        );
    }
}