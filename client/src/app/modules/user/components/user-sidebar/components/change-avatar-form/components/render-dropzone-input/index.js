import React from 'react';
import classNames from 'classnames';
import {isEmpty, noop} from 'lodash';
import Dropzone from 'react-dropzone';

import {Avatar, AvatarTypes} from 'app/components/avatar';

import {ValidationError} from 'kit/components/validation-error';

export const renderDropzoneInput = field => {
    const files = field.input.value;
    return (
        <div className="change-avatar-form__file-upload">
            <div className="change-avatar-form__preview">
                <Avatar
                    type={AvatarTypes.large}
                    image={files && !isEmpty(files) ? {value: files[0].preview} : null}
                    className="change-avatar-form__avatar"
                />
            </div>

            <Dropzone
                name={field.name}
                className="change-avatar-form__dropzone-container"
                accept="image/jpeg, image/png"
                maxSize={4194304}
                onDrop={noop}
                onDropAccepted={filesToUpload => field.input.onChange(filesToUpload)}
                onDropRejected={noop}
                multiple={false}
            >
                {({ isDragActive, isDragReject }) => {
                    const classes = classNames('change-avatar-form__dropzone', {
                        'change-avatar-form__dropzone_active': isDragActive,
                        'change-avatar-form__dropzone_rejected': isDragReject
                    });
                    return (
                        <div className={classes}>
                            {!isDragReject && (
                                <div>
                                    <p>Click, or drop files here to upload.</p>
                                    <p>Supported formats: .jpeg, .jpg, .png</p>
                                    <p>Maximum size 4 MB</p>
                                </div>
                            )}
                            {isDragReject && (<p>This type of file is not supported</p>)}
                        </div>
                    );
                }}
            </Dropzone>

            {field.meta.submitFailed && field.meta.error && (
                <ValidationError>{field.meta.error}</ValidationError>
            )}
        </div>
    );
};
