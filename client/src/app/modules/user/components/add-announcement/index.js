import React from 'react';


import {CreateAnnouncementForm} from './components/create-announcement-form';

export const AddAnnouncement = ({history}) => {
    const toUserAnnouncements = () => {
        history.push('/user/announcements');
    };

    const toNewAnnouncement = uuid => {
        history.push(`/user/edit/${uuid}`);
    };

    return (
        <div className="add-announcement">
            <CreateAnnouncementForm
                toUserAnnouncements={toUserAnnouncements}
                toNewAnnouncement={toNewAnnouncement}
            />
        </div>
    );
};
