import './create-announcement-form.styl';
import React from 'react';
import {reduxForm, Field} from 'redux-form';
import {isEmpty} from 'lodash';

import {addImages} from 'app/services/account/requests';

import {createAnnouncement} from 'app/services/account/requests';

import {validate} from './validate';

import {options} from './options';

import {LocationInput} from './components/location-input';
import {renderDropzoneInput} from './components/render-dropzone-input';
import {DetailsSelect} from './components/details-select';
import {InputGroup} from './components/input-group';
import {InputNumberGroup} from './components/input-number-group';
import {TextAreaGroup} from './components/textarea-group';
import {PeriodToggle} from './components/period-toggle';

import {FormMessage, FormMessageTypes} from 'app/components/form-message';
import {Button, ButtonTypes} from 'kit/components/button';

export const CreateAnnouncementForm = reduxForm({
    form: 'createAnnouncement',
    validate,
    initialValues: {
        'location': {
            'address': ''
        },
        'street': '',
        'appNumber': '',
        'period': 'MONTH',
        'price': '',
        'roomCount': '1',
        'livingPlacesCount': '1',
        'title': '',
        'shortDescription': '',
        'wiFi': false,
        'washingMachine': false,
        'refrigerator': false,
        'hairDryer': false,
        'iron': false,
        'smoking': false,
        'animals': false,
        'kitchenStuff': false,
        'conditioner': false,
        'balcony': false,
        'tv': false,
        'essentials': false,
        'shampoo': false,
        'details': []
    }
})(props => {
    const onSubmit = values => {
        let location = {
            city: '',
            region: '',
            country: '',
            latitude: values.location ? values.location.latitude : 0,
            longitude: values.location ? values.location.longitude : 0,
            placeId: values.location ? values.location.placeId : ''
        };

        let locationArray = values.location ? values.location.address.split(', ') : [];
        if (locationArray.length === 3) {
            location.city = locationArray[0];
            location.region = locationArray[1];
            location.country = locationArray[2];
        } else if (locationArray.length === 2) {
            location.city = locationArray[0];
            location.country = locationArray[1];
        }

        let newValues = {
            ...values,
            ...location,
            ...values.details.map(detail => detail.value).reduce((m, detail) => ({...m, [detail]: true}), {})

        };
        delete newValues.location;
        delete newValues.files;

        return createAnnouncement(newValues).then(({uuid}) => {
            if (isEmpty(values.files)) {
                return;
            }

            let formData = new FormData();

            formData.append('uuid', uuid);

            for (let i = 0; i < values.files.length; i++) {
                formData.append('files', values.files[i]);
            }

            addImages(formData).then(() => {
                props.toNewAnnouncement(uuid);
            });
        });
    };

    return (
        <form className="create-announcement-form" onSubmit={props.handleSubmit(onSubmit)}>
            <h3 className="create-announcement-form__title">Create announcement</h3>

            {props.submitSucceeded && (
                <FormMessage
                    message="Success, you created new announcement"
                    type={FormMessageTypes.success}
                    className="create-announcement-form__form-message"
                />
            )}

            {props.error && (
                <FormMessage
                    message={props.error}
                    type={FormMessageTypes.error}
                    className="create-announcement__form-message"
                />
            )}

            <Field name="location" component={LocationInput} label="Start typing location"/>

            <div>
                <Field name="title" component={InputGroup} label="Title"/>

                <Field name="street" component={InputGroup} label="Street"/>
                <Field name="appNumber" component={InputGroup} label="Appartments number"/>

                <Field name="roomCount" component={InputNumberGroup} label="Rooms" min={1} max={10}/>
                <Field name="livingPlacesCount" component={InputNumberGroup} label="Living places" min={1} max={20}/>

                <Field
                    name="period"
                    toggleType="text"
                    toggleLeft="Day"
                    toggleRight="Month"
                    component={PeriodToggle}
                />
                <Field name="price" component={InputGroup} label="Price" type="number"/>

                <Field name="shortDescription" component={TextAreaGroup} label="Short Description"/>

                <Field
                    name="details"
                    component={DetailsSelect}
                    options={options}
                    multi={true}
                />

                <h4>Add images</h4>

                <Field name="files" component={renderDropzoneInput}/>

                <Button
                    type="submit"
                    className="create-announcement-form__submit-button"
                    caption="Create"
                    buttonType={ButtonTypes.accent}
                />
            </div>
        </form>
    );
});



