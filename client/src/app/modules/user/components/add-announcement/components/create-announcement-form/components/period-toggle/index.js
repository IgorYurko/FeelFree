import './period-toggle.styl';
import React from 'react';
import {Toggle} from 'kit/components/toggle';
import {ValidationError} from 'kit/components/validation-error';

export const PeriodToggle = ({input, name, toggleType, toggleLeft, toggleRight, value, meta: { touched, error }}) => (
    <div className="create-announcement-form__toggle-group">
        <Toggle
            className="create-announcement-form__toggle"
            {...input}
            name={name}
            toggleType={toggleType}
            toggleLeft={toggleLeft}
            toggleRight={toggleRight}
            value={input.value === 'DAY'}
        />
        {touched && error && <ValidationError>{error}</ValidationError>}
    </div>
);
