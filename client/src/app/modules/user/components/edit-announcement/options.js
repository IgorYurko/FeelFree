import {IconTypes} from 'kit/components/icon/index';

export const options = {
    [IconTypes.tv]: {'value': IconTypes.tv, 'label': 'TV'},
    [IconTypes.wiFi]: {'value': IconTypes.wiFi, 'label': 'WI-FI'},
    [IconTypes.shampoo]: {'value': IconTypes.shampoo, 'label': 'Shampoo'},
    [IconTypes.essentials]: {'value': IconTypes.essentials, 'label': 'Essentials'},
    [IconTypes.conditioner]: {'value': IconTypes.conditioner, 'label': 'Conditioner'},
    [IconTypes.washingMachine]: {'value': IconTypes.washingMachine, 'label': 'Washing machine'},
    [IconTypes.balcony]: {'value': IconTypes.balcony, 'label': 'Balcony'},
    [IconTypes.iron]: {'value': IconTypes.iron, 'label': 'Iron'},
    [IconTypes.refrigerator]: {'value': IconTypes.refrigerator, 'label': 'Refrigerator'},
    [IconTypes.animals]: {'value': IconTypes.animals, 'label': 'Animals'},
    [IconTypes.smoking]: {'value': IconTypes.smoking, 'label': 'Smoking'},
    [IconTypes.hairDryer]: {'value': IconTypes.hairDryer, 'label': 'Hair dryer'},
    [IconTypes.kitchenStuff]: {'value': IconTypes.kitchenStuff, 'label': 'Kitchen stuff'}
};
