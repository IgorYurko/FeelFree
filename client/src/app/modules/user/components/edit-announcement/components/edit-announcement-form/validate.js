export const validate = values => {
    const errors = {};
    if (!values.title) {
        errors.title = 'Title is required';
    }

    if (!values.country) {
        errors.country = 'Country is required';
    }
    if (!values.city) {
        errors.city = 'City is required';
    }
    if (!values.region) {
        errors.region = 'Region is required';
    }


    if (!values.roomCount) {
        errors.roomCount = 'Room count is required';
    }
    if (!values.livingPlacesCount) {
        errors.livingPlacesCount = 'Living places count is required';
    }
    if (!values.price) {
        errors.price = 'Price is required';
    }
    return errors;
};
