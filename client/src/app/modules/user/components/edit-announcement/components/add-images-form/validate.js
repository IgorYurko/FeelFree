import {isEmpty} from 'lodash';

export const validate = values => {
    const errors = {};
    if (isEmpty(values.files)) {
        errors.files = 'No images selected';
    } else if (values.files.length > 10) {
        console.log(values.files.length)
        errors.files = 'Maximum 10 images allowed';
    }
    return errors;
};
