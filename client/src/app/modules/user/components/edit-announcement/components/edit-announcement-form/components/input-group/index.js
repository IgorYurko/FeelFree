import React from 'react';
import {Input, InputTypes} from 'kit/components/input/index';
import {ValidationError} from 'kit/components/validation-error/index';

export const InputGroup = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div className="edit-announcement-form__input-group">
        <Input
            type={type}
            {...input}
            className="edit-announcement-form__input"
            placeholder={label}
            inputType={InputTypes.primary}
        />
        {touched && error && <ValidationError>{error}</ValidationError>}
    </div>
);
