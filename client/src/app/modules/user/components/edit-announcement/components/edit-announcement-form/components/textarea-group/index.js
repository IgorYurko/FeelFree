import React from 'react';
import {TextArea} from 'kit/components/textarea/index';
import {ValidationError} from 'kit/components/validation-error/index';

export const TextAreaGroup = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div className="edit-announcement-form__textarea-group">
        <TextArea
            type={type}
            {...input}
            className="edit-announcement-form__textarea"
            placeholder={label}
        />
        {touched && error && <ValidationError>{error}</ValidationError>}
    </div>
);
