import {IconTypes} from 'kit/components/icon/index';

export const options = [
    {'value': IconTypes.tv, 'label': 'TV'},
    {'value': IconTypes.wiFi, 'label': 'WI-FI'},
    {'value': IconTypes.shampoo, 'label': 'Shampoo'},
    {'value': IconTypes.essentials, 'label': 'Essentials'},
    {'value': IconTypes.conditioner, 'label': 'Conditioner'},
    {'value': IconTypes.washingMachine, 'label': 'Washing machine'},
    {'value': IconTypes.balcony, 'label': 'Balcony'},
    {'value': IconTypes.iron, 'label': 'Iron'},
    {'value': IconTypes.refrigerator, 'label': 'Refrigerator'},
    {'value': IconTypes.animals, 'label': 'Animals'},
    {'value': IconTypes.smoking, 'label': 'Smoking'},
    {'value': IconTypes.hairDryer, 'label': 'Hair dryer'},
    {'value': IconTypes.kitchenStuff, 'label': 'Kitchen stuff'}
];
