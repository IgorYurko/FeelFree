import React from 'react';
import classNames from 'classnames';
import {isEmpty, noop} from 'lodash';
import Dropzone from 'react-dropzone';

import {ValidationError} from 'kit/components/validation-error';

export const renderDropzoneInput = field => {
    const files = field.input.value;
    return (
        <div className="add-images-form__file-upload">
            <Dropzone
                name={field.name}
                className="add-images-form__dropzone-container"
                accept="image/jpeg, image/png"
                maxSize={4194304}
                onDrop={noop}
                onDropAccepted={filesToUpload => field.input.onChange([...files, ...filesToUpload])}
                onDropRejected={noop}
            >
                {({ isDragActive, isDragReject }) => {
                    const classes = classNames('add-images-form__dropzone', {
                        'add-images-form__dropzone_active': isDragActive,
                        'add-images-form__dropzone_rejected': isDragReject
                    });
                    return (
                        <div className={classes}>
                            {!isDragReject && (
                                <div>
                                    <p>Click, or drop files here to upload.</p>
                                    <p>Supported formats: .jpeg, .jpg, .png</p>
                                    <p>Maximum 10 files up to 4 MB each</p>
                                </div>
                            )}
                            {isDragReject && (<p>This type of file is not supported</p>)}
                        </div>
                    );
                }}
            </Dropzone>

            {field.meta.submitFailed && field.meta.error && (
                <ValidationError>{field.meta.error}</ValidationError>
            )}

            {files && !isEmpty(files) && (
                <ul className="add-images-form__preview-list">
                    {files.map((file, i) => (
                        <li className="add-images-form__preview-item" key={i}>
                            <div
                                className="add-images-form__delete-item"
                                onClick={() => field.input.onChange([
                                    ...files.slice(0, i),
                                    ...files.slice(i + 1)
                                ])}
                            >
                                &#x2715;
                            </div>
                            <img className="add-images-form__preview-img" src={file.preview}/>
                        </li>
                    ))}
                </ul>
            )}
        </div>
    );
};
