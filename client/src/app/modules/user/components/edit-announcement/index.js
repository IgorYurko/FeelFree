import './edit-announcement.styl';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {isEmpty} from 'lodash';
import autobind from 'autobind-decorator';

import {getSingleUserAnnouncement, deleteImage} from 'app/services/account/actions';

import {EditAnnouncementForm} from './components/edit-announcement-form';
import {AddImagesForm} from './components/add-images-form';
import {EditImageList} from './components/edit-images-list';

import {Spinner} from 'kit/components/spinner';

export const EditAnnouncement = connect(({account}, ownProps) => ({
    announcement: account.announcements[ownProps.match.params.id],
    pending: account.pending
}), {
    getSingleUserAnnouncement, deleteImage
})(class extends Component {
    componentDidMount() {
        this.props.getSingleUserAnnouncement(this.props.match.params.id);
    }

    @autobind
    updateAnnouncement() {
        this.props.getSingleUserAnnouncement(this.props.match.params.id);
    }

    render() {
        const {announcement, pending} = this.props;
        const toEditedAnnouncement = () => {
            this.props.history.push(`/announcement/${announcement.uuid}`);
        };

        if (!announcement) {
            return null;
        }

        return (
            <div className="edit-announcement">
                {!pending && announcement.description ?
                    (
                        <div>
                            <EditAnnouncementForm
                                initialValues={{
                                    'uuid': announcement.uuid,
                                    'location': {
                                        'address': [
                                            announcement.address.city,
                                            announcement.address.region,
                                            announcement.address.country
                                        ].filter(term => !!term).join(', '),
                                        'lat': announcement.geolocation.latitude,
                                        'lng': announcement.geolocation.longitude
                                    },
                                    'street': announcement.address.street,
                                    'appNumber': announcement.address.appNumber,
                                    ...Object.keys(announcement.description).map(key => ({[key]: false}))
                                        .reduce((m, detail) => ({...m, ...detail}), {}),
                                    'livingPlacesCount': announcement.description.livingPlacesCount,
                                    'roomCount': announcement.description.roomCount,
                                    'details': Object.entries(announcement.description).filter(entry => {
                                        return entry[1] === true;
                                    }).map(entry => entry[0]),
                                    'title': announcement.title,
                                    'period': announcement.price.period,
                                    'price': announcement.price.value,
                                    'shortDescription': announcement.shortDescription
                                }}
                                toEditedAnnouncement={toEditedAnnouncement}
                                updateAnnouncement={this.updateAnnouncement}
                            />
                            {!isEmpty(announcement.images) && (
                                <EditImageList
                                    images={announcement.images}
                                    deleteImage={this.props.deleteImage(announcement.uuid)}
                                />
                            )}
                            <AddImagesForm
                                initialValues={{
                                    'uuid': announcement.uuid
                                }}
                                updateAnnouncement={this.updateAnnouncement}
                            />
                        </div>
                    )
                    :
                    (
                        <div className="edit-announcement__spinner-container"><Spinner/></div>
                    )
                }
            </div>
        );
    }
});
