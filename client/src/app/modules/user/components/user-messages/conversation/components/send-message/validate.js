export const validate = values => {
    const errors = {};
    if (!values.message) {
        errors.message = 'Please enter message';
    }
    return errors;
};
