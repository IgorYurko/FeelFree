import './conversation.styl';
import React, {Component} from 'react';
import {SendMessage} from './components/send-message';
import moment from 'moment';
import autobind from 'autobind-decorator';
import classNames from 'classnames';

export class Conversation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showMessages: false
        };
    }

    @autobind
    clickMessages() {
        this.setState({showMessages: !this.state.showMessages});
    }

    render() {
        const {messages, id, conversationTitle} = this.props.conversation;
        const sendConversationMessage = this.props.sendConversationMessage;
        const userFullName = this.props.user.data.firstName + ' ' + this.props.user.data.lastName;

        return (
            <div className="messages">
                {this.state.showMessages ?
                    (<a className="messages__link" onClick={this.clickMessages}>Hide conversation</a>):
                    (<a className="messages__link" onClick={this.clickMessages}>Show conversation</a>)
                }
                {this.state.showMessages && (
                    <div>
                        <div className="messages__box">
                            {messages.map((message, index) =>
                                <div
                                    key={index}
                                    className={classNames(
                                        'messages__message',
                                        {'messages__message_right': userFullName !== message.fullName},
                                        {'messages__message_left': userFullName === message.fullName}
                                    )}
                                >
                                    {message.title !== conversationTitle && message.title !== '' &&(
                                        <p className="messages__message_title">
                                            Title changed to "{message.title}"
                                        </p>
                                    )}
                                    <p className="messages__message_text">{message.text}</p>
                                    <p className="messages__message_date">
                                        {message.fullName}
                                    </p>
                                    <p className="messages__message_date">
                                        {moment(message.createMessageTime).format('MMMM Do YYYY, h:mm:ss a')}
                                    </p>
                                </div>
                            )}
                        </div>
                        <SendMessage
                            id={id}
                            sendConversationMessage={sendConversationMessage}
                            form={`sendConversationMessage${id}`}
                        />
                    </div>
                )}
            </div>
        );
    }
}

