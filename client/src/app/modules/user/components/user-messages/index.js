import './user-messages.styl';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {isEmpty} from 'lodash';

import {getConversations, sendConversationMessage} from 'app/services/account/actions';

import {Conversation} from './conversation';
import {Spinner} from 'kit/components/spinner';


export const UserMessages = connect(state => ({account: state.account, user: state.user}), {
    getConversations, sendConversationMessage
})(class extends Component {

    componentDidMount() {
        this.props.getConversations();
    }

    render() {
        const {pending, conversations} = this.props.account;
        const userFullName = this.props.user.data.firstName + ' ' + this.props.user.data.lastName;

        return (
            <div>
                {!pending && isEmpty(conversations) && <p>No conversations</p>}
                {pending ?
                    <div className="user-conversations__spinner-container"><Spinner/></div>
                    :
                    <div>
                        {conversations.map((conversation, index) => (
                            <div key={index} className="conversation">
                                <h4>Title: <span className="conversation__title">{conversation.conversationTitle}</span>
                                </h4>
                                {(conversation.messages.find(message => message.fullName !== userFullName) !== undefined) ?
                                    (<h4>User: <span className="conversation__title">
                                        {conversation.messages.find(message => message.fullName !== userFullName).fullName}
                                        </span>
                                    </h4>)
                                :
                                    (<h4>User: <span className="conversation__title">
                                    {conversation.messages[0].fullName}
                                    </span>
                                    </h4>)
                                }
                                <Conversation
                                    conversation={conversation}
                                    sendConversationMessage={this.props.sendConversationMessage}
                                    user={this.props.user}
                                />
                            </div>
                        ))}
                    </div>
                }
            </div>
        );
    }
});

