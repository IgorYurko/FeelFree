import './user-favourites.styl';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {isEmpty} from 'lodash';

import {getFavourites, removeFavourite} from 'app/services/account/actions';

import {AnnouncementList} from 'app/components/announcement-list';
import {AnnouncementPreview} from 'app/components/announcement-preview';

import {Spinner} from 'kit/components/spinner';

export const UserFavourites = connect(state => ({account: state.account}), {
    getFavourites, removeFavourite
})(class extends Component {
    componentDidMount() {
        this.props.getFavourites();
    }

    render() {
        const {pending, favourites} = this.props.account;

        return (
            <div>
                {!pending && isEmpty(favourites) && <p>No favourites</p>}
                {pending ?
                    <div className="user-favourites__spinner-container"><Spinner/></div>
                    :
                    <AnnouncementList>
                        {Object.values(favourites).map(announcement => (
                            <AnnouncementPreview
                                key={announcement.uuid}
                                announcement={announcement}
                                removeFavourite={this.props.removeFavourite}
                            />
                        ))}
                    </AnnouncementList>
                }
            </div>
        );
    }
});
