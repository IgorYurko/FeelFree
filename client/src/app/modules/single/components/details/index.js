import './details.styl';
import React, {PropTypes} from 'react';
import {Icon} from 'kit/components/icon';
import {IconLabel} from './labels';
import {groupBy} from 'lodash';


export const Details = ({icons = []}) => {
    const {available = [], unavailable = []} = groupBy(icons, item => item.value ? 'available': 'unavailable');

    return (
        <div className="detail">
            <h4 className="detail__title">Amenities:</h4>
            <div className="detail__container">
                {available.map((item, index) => (
                    <div className="detail__view" key={index}>
                        <Icon className="detail__icon" type={item.type}/>
                        <span className="detail__view_label">{IconLabel[item.type]}</span>
                    </div>
                ))}
                {unavailable.map((item, index) => (
                    <span
                        key={index}
                        className="detail__view_label detail__view detail__view_text"
                    >
                        {IconLabel[item.type]}
                    </span>
                ))}
            </div>
        </div>
    );
};

Details.propTypes = {
    iconsTrue: PropTypes.object,
    iconsFalse: PropTypes.object
};

Details.defaultProps = {
    iconsTrue: {},
    iconsFalse: {}
};

