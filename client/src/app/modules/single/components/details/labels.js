import {IconTypes} from 'kit/components/icon';

export const IconLabel = {
    [IconTypes.tv]: 'TV',
    [IconTypes.wiFi]: 'WI-FI',
    [IconTypes.shampoo]: 'Shampoo',
    [IconTypes.essentials]: 'Essentials',
    [IconTypes.conditioner]: 'Conditioner',
    [IconTypes.washingMachine]: 'Washing machine',
    [IconTypes.balcony]: 'Balcony',
    [IconTypes.iron]: 'Iron',
    [IconTypes.refrigerator]: 'Refrigerator',
    [IconTypes.animals]: 'Animals',
    [IconTypes.smoking]: 'Smoking',
    [IconTypes.hairDryer]: 'Hair dryer',
    [IconTypes.kitchenStuff]: 'Kitchen stuff'
};
