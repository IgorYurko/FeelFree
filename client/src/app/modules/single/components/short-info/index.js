import './short-info.styl';
import React, {PropTypes} from 'react';

export const ShortInfo = ({description: {roomCount, livingPlacesCount}}) => {
    return (
        <div className="info-short">
            <span className="info-short__rooms">Rooms: {roomCount}</span>
            <span>Living places: {livingPlacesCount}</span>
        </div>
    );
};

ShortInfo.propTypes = {
    description: PropTypes.object
};

ShortInfo.defaultProps = {
    description: {}
};
