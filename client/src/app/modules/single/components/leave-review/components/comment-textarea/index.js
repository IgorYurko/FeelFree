import React from 'react';
import {TextArea} from 'kit/components/textarea';

export const CommentTextArea = ({ input, type }) => (
    <TextArea
        type={type}
        {...input}
    />
);
