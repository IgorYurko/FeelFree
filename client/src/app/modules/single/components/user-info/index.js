import './user-info.styl';
import React, {Component, PropTypes} from 'react';

import {Avatar, AvatarTypes} from 'app/components/avatar';
import {Button, ButtonTypes} from 'kit/components/button';
import {Icon, IconTypes} from 'kit/components/icon';
import {Modal} from 'kit/components/modal';
import {MessageForm} from './components/message-form';
import {isEmpty} from 'lodash';
import autobind from 'autobind-decorator';

@autobind

export class UserInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalOpen: false
        };
    }

    onClickMessage() {
        this.setState({modalOpen: true});
    }

    closeModal() {
        this.setState({modalOpen: false});
    }

    render() {
        const {firstName, phones, image, emails, profileEmail} = this.props.user;
        let checkOwner = this.props.userEmail === profileEmail;

        return (
            <div className="user-info">
                <Avatar type={AvatarTypes.medium} image={image}/>
                <div className="user-info__data">
                    <p className="user-info__data_name">{firstName}</p>
                    <div className="user-info__contacts">
                        {!isEmpty(phones) &&
                            (<Button className="user-info__button user-info__button_phone"
                                        buttonType={ButtonTypes.default}
                             >
                                <Icon className="user-info__icon" type={IconTypes.phone}/>
                                <div className="user-info__phones-list">
                                {phones.map((phone, index) => (
                                        <span key={index}>{phone.value}</span>
                                    )
                                )}
                                </div>
                            </Button>)
                        }
                        {!isEmpty(emails) &&
                        (<Button className="user-info__button user-info__button_email"
                                 buttonType={ButtonTypes.default}
                         >
                            <Icon className="user-info__icon" type={IconTypes.massage}/>
                            <div className="user-info__phones-list">
                                {emails.map((email, index) => (
                                        <span key={index}>{email.value}</span>
                                    )
                                )}
                            </div>
                        </Button>)
                        }
                        {this.props.isLoggedIn && !checkOwner &&(
                            <Button
                                className="user-info__button"
                                buttonType={ButtonTypes.default}
                                onClick={this.onClickMessage}
                            >
                                <Icon className="user-info__icon user-info__icon_color" type={IconTypes.massage}/>
                                <span className="user-info__button_message">Write to owner</span>
                            </Button>
                        )}
                    </div>
                </div>
                <Modal isOpen={this.state.modalOpen} closeModal={this.closeModal} onRequestClose={this.closeModal}>
                    <MessageForm
                        sendMessage={this.props.sendMessage}
                        id={this.props.id}
                        initialValues={{title: this.props.title}}
                        closeModal={this.closeModal}
                    />
                </Modal>
            </div>
        );
    }
}

UserInfo.propTypes = {
    user: PropTypes.object
};

UserInfo.defaultProps = {
    user: {}
};

