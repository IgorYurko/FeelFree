export const validate = values => {
    const errors = {};
    if (!values.title) {
        errors.title = 'Please enter title';
    }
    if (!values.message) {
        errors.message = 'Please enter message';
    }
    return errors;
};
