import './reviews.styl';
import React from 'react';

import {UserInfo} from '../user-info';
import {Button, ButtonTypes} from 'kit/components/button';

import moment from 'moment';
import {isEmpty} from 'lodash';

export const Reviews = ({reviews = {}, pageNumber, totalPages, pendingComments, getNextAnnouncementComments, id}) => {

    const getNexComments = () => {
        getNextAnnouncementComments(id, {page: pageNumber + 1});
    };

    return (
        <div className="reviews">
                <h4 className="reviews__title">Reviews:</h4>
                <div>
                    {reviews.content.map((info, index) => (
                        <div key={index} className="reviews__review">
                            <UserInfo user={reviews.content[index].user}/>
                            <p className="reviews__review_message">{reviews.content[index].value}</p>
                            <p className="reviews__review_date">
                                {moment(reviews.content[index].date).format('MMMM Do YYYY, h:mm:ss a')}
                            </p>
                        </div>
                    ))}
                </div>
            {pageNumber !== totalPages - 1 && !isEmpty(reviews) && (
                <Button
                    onClick={() => getNexComments()}
                    buttonType={ButtonTypes.accent}
                    caption="MORE"
                    className="reviews__button"
                    busy={pendingComments}
                    disabled={pendingComments}
                />
            )}
        </div>
    );
};

