import React from 'react';
import {DatePicker} from 'kit/components/date-picker';

export const DatePickerBook = ({ input, type, minDate, excludeDates}) => (
    <DatePicker
        type={type}
        {...input}
        minDate={minDate}
        excludeDates={excludeDates}
    />
);

