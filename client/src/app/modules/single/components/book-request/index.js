import './book-request.styl';
import React, {PropTypes} from 'react';
import {Field, reduxForm} from 'redux-form';
import {noop} from 'lodash';
import {DatePickerBook} from './components/date-picker-book';
import {Button, ButtonTypes} from 'kit/components/button';
import {FormMessage, FormMessageTypes} from 'app/components/form-message';
import {Link} from 'kit/components/link';
import moment from 'moment';
import classNames from 'classnames';

export const BookRequest = (reduxForm({
    form: 'booking',
    initialValues: {
        range: {startDate: moment(), endDate: moment().add(1, 'days')}
    }
})(props => {
    const submit = values => {
        let newValues = {
            checkIn: values.range.startDate,
            checkOut: values.range.endDate,
            uuid: props.uuid
        };
        props.bookAnnouncement(newValues);
        props.reset();
    };

    let checkOwner = props.userEmail === props.ownerEmail;

    const dates = [...props.bookedDates, moment().subtract(1, 'days')];

    return (
        <form className="booking" onSubmit={props.handleSubmit(submit)}>
            <header className="booking__header">
                <p><span>{props.price}$ </span>per {props.period}</p>
            </header>
            <div className="booking__wrapper">
                <Field
                    name="range"
                    minDate={moment()}
                    excludeDates={dates}
                    component={DatePickerBook}
                />
                {props.submitSucceeded && (
                    <FormMessage
                        message="Request has been sent"
                        type={FormMessageTypes.success}
                        className="booking__form-message"
                    />

                )}
                {!props.submitSucceeded && props.isLoggedIn && !checkOwner &&(
                    <Button
                        type="submit"
                        className={classNames('booking__button', {'booking__button_success': props.submitSucceeded})}
                        caption="REQUEST A BOOK"
                        buttonType={ButtonTypes.accent}
                        busy={props.announcements.pendingBooking}
                        disabled={props.announcements.pendingBooking}
                    />
                )}
                {!props.isLoggedIn &&
                (<div className="booking__links">
                    To book an apartment,
                    <Link to="/login"> login</Link> or <Link to="/registration">register</Link>, please
                </div>)
                }
            </div>
        </form>
    );
}));

BookRequest.propTypes = {
    onClick: PropTypes.func,
    price: PropTypes.number,
    period: PropTypes.string,
    bookedDates: PropTypes.array
};

BookRequest.defaultProps = {
    onClick: noop,
    price: null,
    period: 'month',
    bookedDates: []
};
