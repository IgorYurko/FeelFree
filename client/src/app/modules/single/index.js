import './single.styl';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import {getSingleAnnouncement, getAnnouncementComments, addAnnouncementComment,
        getNextAnnouncementComments, bookAnnouncement} from 'app/services/announcements/actions';
import {sendMessage} from 'app/services/account/actions';
import {Container} from 'kit/components/container';
import {Carousel, CarouselTypes} from 'kit/components/carousel';
import {UserInfo} from './components/user-info';
import {BookRequest} from './components/book-request';
import {IconTypes} from 'kit/components/icon';
import {Link} from 'kit/components/link';
import {Details} from './components/details';
import {Reviews} from './components/reviews';
import {ShortInfo} from './components/short-info';
import {LeaveReview} from './components/leave-review';
import {isEmpty} from 'lodash';
import {SingleMap} from './components/single-map';

export const Single = connect(({announcements, user, account}, ownProps) => ({
    announcement: announcements.data[ownProps.match.params.id],
    announcements: announcements,
    isLoggedIn: user.isLoggedIn,
    userEmail: user.data.profileEmail
}), {
    getSingleAnnouncement, getAnnouncementComments, addAnnouncementComment, getNextAnnouncementComments,
    bookAnnouncement, sendMessage
})(class extends Component {
    componentDidMount() {
        this.props.getSingleAnnouncement(this.props.match.params.id);
        this.props.getAnnouncementComments(this.props.match.params.id);
    }

    render() {
        const {announcement} = this.props;
        if (!announcement) {
            return null;
        }
        const {
            rented = [], description = [], images = [], price = {}, comments = {}, title = '', user = {}
        } = announcement || {};

        const icons = Object.keys(description)
            .filter(key => IconTypes[key])
            .map(type => ({
                type,
                value: description[type] || false
            }));

        const bookedDates = rented[0] ? rented[0].rents.map(date => moment(date)) : null

        const userEmail = this.props.userEmail;
        const ownerEmail = user.profileEmail;

        return (
            <div className="single">
                <Container>
                        <Carousel
                            type={CarouselTypes.large}
                            imgUrls={images.map(image => image.value)}
                        />
                        <div className="single__main">
                            <div className="single__info">
                                <div className="single__info-block single__info-block_border">
                                    <h2 className="single__info-title">{announcement.title}</h2>
                                    {announcement.address && (
                                        <p>{[
                                            announcement.address.city,
                                            announcement.address.region,
                                            announcement.address.country
                                        ].filter(term => !!term).join(', ')}</p>
                                    )}
                                    <ShortInfo description={announcement.description}/>
                                </div>
                                <div className="single__info-block  single__info-block_main">
                                    {announcement.shortDescription &&
                                        (
                                            <div className="single__info-description">
                                                {announcement.shortDescription.split('\n').map((p, i) => {
                                                    return <p key={i}>{p}</p>;
                                                })}
                                            </div>
                                        )
                                    }
                                </div>
                                <div className="single__info-block">
                                    <Details icons={icons}/>
                                </div>
                                {announcement.geolocation && !!announcement.geolocation.latitude && (
                                    <SingleMap
                                        announcement={announcement}
                                        center={{
                                            lat: announcement.geolocation.latitude,
                                            lng: announcement.geolocation.longitude
                                        }}
                                    />
                                )}
                                {this.props.isLoggedIn &&
                                    (
                                        <div className="single__info-block single__info-block_border">
                                            <LeaveReview
                                                addAnnouncementComment={this.props.addAnnouncementComment}
                                                uuid={this.props.match.params.id}
                                                announcements={this.props.announcements}
                                            />
                                        </div>
                                    )
                                }
                                {!isEmpty(comments.content) &&
                                <div className="single__info-block single__info-block_border">
                                    <Reviews
                                        reviews={comments}
                                        pageNumber={this.props.announcements.pageNumberComments}
                                        totalPages={this.props.announcements.totalPagesComments}
                                        pendingComments={this.props.announcements.pendingComments}
                                        getNextAnnouncementComments={this.props.getNextAnnouncementComments}
                                        id={this.props.match.params.id}
                                    />
                                </div>
                                }
                                {!this.props.isLoggedIn &&
                                    <div
                                        className="single__info-block single__info-block_border single__info-block_link"
                                    >
                                        To leave a review,
                                        <Link to="/login"> login</Link> or <Link to="/registration">register</Link>,
                                        please
                                    </div>
                                }
                            </div>
                            <div className="single__book single__book_box">
                                {bookedDates && (
                                    <BookRequest
                                        price={price.value}
                                        period={price.period}
                                        bookedDates={bookedDates}
                                        uuid={this.props.match.params.id}
                                        bookAnnouncement={this.props.bookAnnouncement}
                                        announcements={this.props.announcements}
                                        isLoggedIn={this.props.isLoggedIn}
                                        userEmail={userEmail}
                                        ownerEmail={ownerEmail}
                                        initialValues={{
                                            range: {
                                                startDate: bookedDates[bookedDates.length - 1],
                                                endDate: bookedDates[bookedDates.length - 1]
                                            }
                                        }}
                                    />
                                )}
                                <div className="single__user-info">
                                    <UserInfo
                                        user={user}
                                        sendMessage={this.props.sendMessage}
                                        isLoggedIn={this.props.isLoggedIn}
                                        id={this.props.match.params.id}
                                        title={title}
                                        userEmail={userEmail}
                                    />
                                </div>
                            </div>
                        </div>
                </Container>
            </div>
        );
    }
});


