import './search-form.styl';
import React, {Component} from 'react';

import { Field, reduxForm } from 'redux-form';

import {LocationAutocomplete} from 'app/components/location-autocomplete';

import {Button, ButtonTypes} from 'kit/components/button';
import {Icon, IconTypes} from 'kit/components/icon';
import {PeriodToggle} from './components/period-toggle';
import {InputNumberGroup} from './components/input-number-group';

import autobind from 'autobind-decorator';

export const SearchForm = reduxForm({
    form: 'search',
    initialValues: {
        period: 'MONTH'
    }
})(class extends Component {

    @autobind
    onClickFilters() {
        this.props.toSearch();
    }

    render() {
        const {getAnnouncements, handleSubmit, setSearchParameters, toSearch} = this.props;

        const submit = values => {
            let location = {
                city: '',
                region: '',
                country: '',
                placeId: values.location ? values.location.placeId || '' : ''
            };

            if (values.location) {
                let locationArray = values.location.address.split(', ');
                if (locationArray.length === 3) {
                    location.city = locationArray[0];
                    location.region = locationArray[1];
                    location.country = locationArray[2];
                } else if (locationArray.length === 2) {
                    location.city = locationArray[0];
                    location.country = locationArray[1];
                }
            }

            let newValues = {
                ...values,
                ...location
            };
            delete newValues.location;

            getAnnouncements(newValues);

            toSearch();

            setSearchParameters({values, ...{openFilters: false}});
        };

        return (
            <form onSubmit={handleSubmit(submit)}>
                <div className="search-form">
                    <div className="search-form_main">
                        <div>
                            <span className="search-form__item-label">Location</span>
                            <Field name="location" component={LocationAutocomplete} label="Start typing location"/>
                        </div>
                        <div className="search-form__item">
                            <span className="search-form__item-label">Rooms</span>
                            <Field name="rooms" component={InputNumberGroup} min={1} max={10}/>
                        </div>
                        <div className="search-form__item">
                            <span className="search-form__item-label">Living places</span>
                            <Field name="livingPlaces" component={InputNumberGroup} min={1} max={20}/>
                        </div>
                        <div className="search-form__item">
                            <span className="search-form__item-label">Per</span>
                            <Field
                                name="period"
                                toggleType="text"
                                toggleLeft="Day"
                                toggleRight="Month"
                                component={PeriodToggle}
                            />
                        </div>
                        <Button
                            type="submit"
                            caption="SEARCH"
                            buttonType={ButtonTypes.accent}
                            className="search-form__button"
                        />
                    </div>
                    <div className="search-form__more-label">
                        <Icon className="search-form__icon-plus" type={IconTypes.plus}/>
                        <a onClick={this.onClickFilters}>More filters</a>
                    </div>
                </div>
            </form>
        );
    }
});
