import React from 'react';
import {InputNumber} from 'kit/components/input-number';

export const InputNumberGroup = ({input, name, min, max}) => (
    <InputNumber
        {...input}
        name={name}
        min={min}
        max={max}
    />
);
