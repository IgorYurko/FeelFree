import './home.styl';
import React, {Component} from 'react';
import {connect} from 'react-redux';

import {getAnnouncements, getNextAnnouncements, setSearchParameters} from 'app/services/announcements/actions';
import {addFavourite, removeFavourite} from 'app/services/account/actions';

import {Intro} from 'app/components/intro';
import {AnnouncementList} from 'app/components/announcement-list';
import {AnnouncementPreview} from 'app/components/announcement-preview';
import {SearchForm} from 'app/modules/home/components/search-form';
import {Button, ButtonTypes} from 'kit/components/button';
import {Container} from 'kit/components/container';
import {Spinner} from 'kit/components/spinner';

import autobind from 'autobind-decorator';
import {isEmpty} from 'lodash';

@autobind
export const Home = connect(state => ({announcements: state.announcements}), {
    getAnnouncements, getNextAnnouncements, setSearchParameters, addFavourite, removeFavourite
})(class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hideButton: false
        };
    }

    componentDidMount() {
        this.props.getAnnouncements();
    }

    toSearch() {
        this.props.history.push('/search');
    }

    getNextPage() {
        const {pageNumberAnnouncements} = this.props.announcements;
        this.props.getNextAnnouncements({page: pageNumberAnnouncements + 1});
    }

    render() {
        const {pageNumberAnnouncements, totalPagesAnnouncements, data, pending} = this.props.announcements;

        return (
            <div className="home">
                <Intro>
                    <Container>
                        <SearchForm
                            getAnnouncements={this.props.getAnnouncements}
                            announcements={this.props.announcements}
                            toSearch={this.toSearch}
                            setSearchParameters={this.props.setSearchParameters}
                        />
                    </Container>
                </Intro>
                <div className="home__latest-announcements">
                    <Container className="home__latest-announcements-container">
                        {isEmpty(this.props.announcements.data) ?
                            (<div className="home__spinner"><Spinner/></div>)
                            :
                            (
                                <div>
                                    <AnnouncementList>
                                        {Object.values(data).map(announcement => (
                                            <AnnouncementPreview
                                                key={announcement.uuid}
                                                announcement={announcement}
                                                addFavourite={this.props.addFavourite}
                                                removeFavourite={this.props.removeFavourite}
                                            />
                                        ))}
                                    </AnnouncementList>
                                    {pageNumberAnnouncements !== totalPagesAnnouncements - 1
                                    && !isEmpty(this.props.announcements)
                                    && (
                                        <Button
                                            onClick={this.getNextPage}
                                            buttonType={ButtonTypes.accent}
                                            caption="MORE"
                                            className="home__button"
                                            busy={pending}
                                            disabled={pending}
                                        />
                                    )}
                                </div>
                            )
                        }
                    </Container>
                </div>
            </div>
        );
    }
});
