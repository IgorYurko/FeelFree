import {post, postImage} from 'app/api';
import {SubmissionError} from 'redux-form';

const submitFormCreateAnnouncement = endpoint => endpoint()
    .then(res => res.json().then(json => {
        if (!res.ok) {
            throw new SubmissionError({...json.errors, _error: 'Error'});
        }
        return json;
    }));

const submitFormAddImages = endpoint => endpoint()
    .then(res => res.json().then(json => {
        if (!res.ok) {
            throw new SubmissionError({...json.errors, _error: 'Error'});
        }
        return json;
    }));

export const createAnnouncement = values => submitFormCreateAnnouncement(() =>
    post('v1/account/announcement', values));

export const editAnnouncement = values => submitFormCreateAnnouncement(() =>
    post('v1/account/announcement', values));

export const addImages = formData => submitFormAddImages(() => postImage('v1/account/announcement/image', formData));

export const changeAvatar = formData => submitFormAddImages(() => postImage('v1/user/edit/image', formData));
