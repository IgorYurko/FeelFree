import {CALL_API, get, post} from 'app/api';
import * as ActionTypes from './constants';

export const getAnnouncements = values => (dispatch, getState) => {
    const {loaded, pending} = getState().announcements;

    if (loaded || pending) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.GET_ANNOUNCEMENTS_REQUEST,
                ActionTypes.GET_ANNOUNCEMENTS_SUCCESS,
                ActionTypes.GET_ANNOUNCEMENTS_FAILURE
            ],
            endpoint: () => get('v1/search/announcement', values)
        }
    });
};

export const getSingleAnnouncement = (id) => (dispatch, getState) => {
    const {pending} = getState().announcements;

    if (pending) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.GET_SINGLE_ANNOUNCEMENT_REQUEST,
                ActionTypes.GET_SINGLE_ANNOUNCEMENT_SUCCESS,
                ActionTypes.GET_SINGLE_ANNOUNCEMENT_FAILURE
            ],
            endpoint: () => get(`v1/search/announcement/${id}`)
        }
    });
};

export const getNextAnnouncements = values => (dispatch, getState) => {
    const {pending} = getState().announcements;

    if (pending) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.GET_NEXT_ANNOUNCEMENTS_REQUEST,
                ActionTypes.GET_NEXT_ANNOUNCEMENTS_SUCCESS,
                ActionTypes.GET_NEXT_ANNOUNCEMENTS_FAILURE
            ],
            endpoint: () => get('v1/search/announcement', values)
        }
    });
};

export const setSearchParameters = values => (dispatch, getState) =>
    dispatch({
        type: ActionTypes.SET_SEARCH_PARAMETERS,
        values: values
    });

export const getPriceRange = () => (dispatch, getState) => {
    const {pendingPriceRange} = getState().announcements;

    if (pendingPriceRange) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.GET_PRICE_RANGE_REQUEST,
                ActionTypes.GET_PRICE_RANGE_SUCCESS,
                ActionTypes.GET_PRICE_RANGE_FAILURE
            ],
            endpoint: () => get('v1/search/announcement/price-range')
        }
    });
};

export const getAnnouncementComments = (id) => (dispatch, getState) => {
    const {pendingComments} = getState().announcements;

    if (pendingComments) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.GET_ANNOUNCEMENT_COMMENTS_REQUEST,
                ActionTypes.GET_ANNOUNCEMENT_COMMENTS_SUCCESS,
                ActionTypes.GET_ANNOUNCEMENT_COMMENTS_FAILURE
            ],
            endpoint: () => get(`v1/search/announcement/${id}/comment`)
        },
        id: id
    });
};

export const addAnnouncementComment = (comment) => (dispatch, getState) => {
    getState();
    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.ADD_ANNOUNCEMENT_COMMENT_REQUEST,
                ActionTypes.ADD_ANNOUNCEMENT_COMMENT_SUCCESS,
                ActionTypes.ADD_ANNOUNCEMENT_COMMENT_FAILURE
            ],
            endpoint: () => post('v1/account/comment', comment)
        },
        id: comment.uuid
    });
};

export const getNextAnnouncementComments = (id, values) => (dispatch, getState) => {
    const {pendingComments} = getState().announcements;

    if (pendingComments) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.GET_NEXT_ANNOUNCEMENT_COMMENTS_REQUEST,
                ActionTypes.GET_NEXT_ANNOUNCEMENT_COMMENTS_SUCCESS,
                ActionTypes.GET_NEXT_ANNOUNCEMENT_COMMENTS_FAILURE
            ],
            endpoint: () => get(`v1/search/announcement/${id}/comment`, values)
        },
        id: id
    });
};

export const bookAnnouncement = (date) => (dispatch, getState) => {
    const {pendingBooking} = getState().announcements;

    if (pendingBooking) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.BOOK_ANNOUNCEMENT_REQUEST,
                ActionTypes.BOOK_ANNOUNCEMENT_SUCCESS,
                ActionTypes.BOOK_ANNOUNCEMENT_FAILURE
            ],
            endpoint: () => post('v1/account/rent', date)
        }
    });
};
