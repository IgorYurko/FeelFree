import {put, putCred, postCred} from 'app/api';
import {SubmissionError} from 'redux-form';

import {CONTENT_TYPE_JSON, CONTENT_TYPE_FORM} from 'app/api/const';
import urlResolver from 'app/api/urlResolver';

const objToURL = obj =>
    Object.keys(obj).map(key => `${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`).join('&');

const POST_LOGIN_CONFIG = {
    method: 'POST',
    headers: {
        'Authorization': 'Basic ' + btoa('my-client-with-secret:secret'),
        'Accept': CONTENT_TYPE_JSON,
        'Content-Type': CONTENT_TYPE_FORM
    }
};
const postLogin = (url, body, extraParams = {}) =>
    fetch(urlResolver(url, extraParams), {...POST_LOGIN_CONFIG, body: objToURL({...body, grant_type: 'password'})});


//refactor submit forms
const submitFormLogin = endpoint => endpoint()
    .then(res => res.json().then(json => {
        if (!res.ok) {
            throw new SubmissionError({_error: json.error_description});
        }
        return json;
    }));

const submitFormResetPass = endpoint => endpoint()
    .then(res => res.json().then(json => {
        if (!res.ok) {
            throw new SubmissionError({_error: json.message || 'Please enter valid email address'});
        }
        return json;
    }));

const submitFormEditPassword = endpoint => endpoint()
    .then(res => res.json().then(json => {
        if (!res.ok) {
            throw new SubmissionError({...json, _error: json.message || 'Error changing password'});
        }
        return json;
    }));

const submitFormEditEmail = endpoint => endpoint()
    .then(res => res.json().then(json => {
        if (!res.ok) {
            throw new SubmissionError({_error: json.message || 'Error changing email'});
        }
        return json;
    }));

const submitFormEditInfo = endpoint => endpoint()
    .then(res => res.json().then(json => {
        if (!res.ok) {
            throw new SubmissionError({_error: json.message});
        }
        return json;
    }));


export const login = values => submitFormLogin(() => postLogin('v1/oauth/token', values));

export const resetPassword = values => submitFormResetPass(() => put('v1/stuff/passwordreset', values));

export const editPassword = values => submitFormEditPassword(() => put('v1/user/edit/password', values));

export const editInfo = values => submitFormEditInfo(() => put('v1/user/edit/info', values));


// wip
export const editEmail = values => submitFormEditEmail(() => put('v1/user/edit/email', values));

export const cookieTest = () => submitFormEditEmail(() => postCred('v1/user/fakecookie'));


