import {CALL_API, get} from 'app/api';

import * as ActionTypes from './constants';

export const loginSuccess = authData => dispatch =>
    dispatch({
        type: ActionTypes.USER_LOGIN_SUCCESS,
        authData
    });

export const logout = () => dispatch =>
    dispatch({
        type: ActionTypes.USER_LOGOUT
    });

export const checkAuthentication = authData => dispatch =>
    dispatch({
        type: ActionTypes.USER_CHECK_AUTHENTICATION
    });

export const getUser = () => (dispatch, getState) => {
    const {pending} = getState().user;

    if (pending) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.GET_USER_REQUEST,
                ActionTypes.GET_USER_SUCCESS,
                ActionTypes.GET_USER_FAILURE
            ],
            endpoint: () => get('v1/user')
        }
    });
};

export const avatarChanged = image => dispatch =>
    dispatch({
        type: ActionTypes.USER_AVATAR_CHANGE,
        image
    });
