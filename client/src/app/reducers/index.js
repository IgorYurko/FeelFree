import {combineReducers} from 'redux';
import {reducer as reduxFormReducer} from 'redux-form';

import {announcementsReducer} from '../services/announcements/reducers';
import {userReducer} from '../services/user/reducers';
import {accountReducer} from '../services/account/reducers';

export default combineReducers({
    user: userReducer,
    announcements: announcementsReducer,
    account: accountReducer,
    form: reduxFormReducer
});
