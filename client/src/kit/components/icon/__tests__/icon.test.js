import React from 'react';
import {shallow} from 'enzyme';
import {Icon, IconTypes} from '../index';


describe('Icon', () => {
    it('Should exist right className(google icon)', () => {
        const wrapper = shallow(<Icon type={IconTypes.google}/>);
        expect(wrapper.find('.icon').hasClass('icon_google')).toBeTruthy();
    });

    it('Should exist right className(wifi icon)', () => {
        const wrapper = shallow(<Icon type={IconTypes.wiFi}/>);
        expect(wrapper.find('.icon').hasClass('icon_wifi')).toBeTruthy();
    });
});
