import './carousel.styl';
import React, {Component, PropTypes} from 'react';
import autobind from 'autobind-decorator';
import classNames from 'classnames';
import {Icon} from '../icon';

export const CarouselTypes = {
    small: 'small',
    large: 'large'
};

const Sizes = {
    [CarouselTypes.small]: {width: 360, height: 244},
    [CarouselTypes.large]: {width: 1140, height: 600}
};

const Classes = {
    [CarouselTypes.small]: 'carousel_small',
    [CarouselTypes.large]: 'carousel_large'
};

@autobind
export class Carousel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentIndex: 1,
            currentX: 0,
            urlsLength: props.imgUrls.length,
            width: Sizes[props.type].width,
            showControls: false
        };
    }

    next() {
        this.setState(slideNext);
    }

    prev() {
        this.setState(slidePrev);
    }

    showControls() {
        this.setState({showControls: true});
    }

    hideControls() {
        this.setState({showControls: false});
    }

    render() {
        const {imgUrls, type} = this.props;
        const {currentIndex, currentX, urlsLength, showControls} = this.state;

        const carouselClassName = classNames('carousel', Classes[type]);

        const containerStyle = {
            transform: `translate3d(${currentX}px, 0px, 0px)`
        };

        const imgStyle = {
            width: `${Sizes[type].width}px`,
            height: `${Sizes[type].height}px`
        };

        const controlsStyle = {
            display: `${showControls ? 'block' : 'none'}`
        };

        return (
            <div className={carouselClassName} onMouseEnter={this.showControls} onMouseLeave={this.hideControls}>
                <div className="carousel__container" style={containerStyle}>
                    {imgUrls.map((url, index) => (
                        <div key={index} className="carousel__image-container">
                            <img src={url} style={imgStyle}/>
                        </div>
                    ))}
                </div>
                <div className="carousel__controls" style={controlsStyle}>
                    {currentIndex > 1 && (
                        <div className="carousel__control-prev" onClick={this.prev}>
                            <Icon type="chevron-left"/>
                        </div>
                    )}
                    {currentIndex < urlsLength && (
                        <div className="carousel__control-next" onClick={this.next}>
                            <Icon type="chevron-right"/>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

Carousel.propTypes = {
    type: PropTypes.string.isRequired,
    imgUrls: PropTypes.arrayOf(PropTypes.string).isRequired
};

Carousel.defaultProps = {
    type: CarouselTypes.small,
    imgUrls: []
};

const slideNext = state => {
    const {currentIndex, urlsLength, width, currentX} = state;

    if (currentIndex < urlsLength) {
        return {
            currentIndex: currentIndex + 1,
            currentX: currentX - width
        };
    }
    return null;
};

const slidePrev = state => {
    const {currentIndex, width, currentX} = state;

    if (currentIndex > 1) {
        return {
            currentIndex: currentIndex - 1,
            currentX: currentX + width
        };
    }
    return null;
};
