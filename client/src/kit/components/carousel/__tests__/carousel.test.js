import React from 'react';
import {mount} from 'enzyme';
import {Carousel, CarouselTypes} from '../index';

describe('Carousel', () => {
    const imgUrls = ['http://placehold.it/360x244', 'http://placehold.it/360x244', 'http://placehold.it/360x244'];

    it(`should have default type ${CarouselTypes.small}`, () => {
        const wrapper = mount(<Carousel/>);
        expect(wrapper.props().type).toBe(CarouselTypes.small);
    });

    it(`should have correct type ${CarouselTypes.large} if  passed`, () => {
        const wrapper = mount(<Carousel type={CarouselTypes.large}/>);
        expect(wrapper.props().type).toBe(CarouselTypes.large);
    });

    it('should show controls on mouseenter and hide on mouseleave', () => {
        const wrapper = mount(<Carousel imgUrls={imgUrls}/>);
        expect(wrapper.find('.carousel__controls').node.style.display).toBe('none');
        wrapper.simulate('mouseenter');
        expect(wrapper.find('.carousel__controls').node.style.display).toBe('block');
        wrapper.simulate('mouseleave');
        expect(wrapper.find('.carousel__controls').node.style.display).toBe('none');
    });

    it('should slide to next image on next click', () => {
        const wrapper = mount(<Carousel imgUrls={imgUrls}/>);
        const nextControl = wrapper.find('.carousel__control-next');


        expect(nextControl.length).toBe(1);
        expect(wrapper.state().currentIndex).toBe(1);
        nextControl.simulate('click');
        expect(wrapper.state().currentIndex).toBe(2);
    });

    it('should slide to prev image on prev click', () => {
        const wrapper = mount(<Carousel imgUrls={imgUrls}/>);
        const nextControl = wrapper.find('.carousel__control-next');


        expect(nextControl.length).toBe(1);

        expect(wrapper.state().currentIndex).toBe(1);
        nextControl.simulate('click');
        expect(wrapper.state().currentIndex).toBe(2);

        expect(wrapper.find('.carousel__control-prev').length).toBe(1);

        wrapper.find('.carousel__control-prev').simulate('click');
        expect(wrapper.state().currentIndex).toBe(1);
    });
});


