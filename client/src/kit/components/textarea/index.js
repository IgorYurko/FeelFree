import './textarea.styl';
import React, {Component, PropTypes} from 'react';
import autobind from 'autobind-decorator';
import {noop} from 'lodash';

export class TextArea extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
            this.setState({value: nextProps.value});
        }
    }

    @autobind
    onChange(event) {
        this.setState({value: event.target.value});
        this.props.onChange(event);
    }

    render() {
        return (
            <textarea
                className="text-area"
                name={this.props.name}
                value={this.state.value}
                disabled={this.props.disabled}
                placeholder={this.props.placeholder}
                onChange={this.onChange}
                onBlur={this.props.onBlur}
                onFocus={this.props.onFocus}
            />
        );
    }
}

TextArea.propTypes = {
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    name: PropTypes.string,
    value: PropTypes.string
};

TextArea.defaultProps = {
    placeholder: '',
    onChange: noop,
    disabled: false,
    name: '',
    value: ''
};
