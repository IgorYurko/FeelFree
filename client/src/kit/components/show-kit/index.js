import React, {Component} from 'react';
import {Button} from '../button';
import {Input} from '../input';
import {Icon, IconTypes} from '../icon';
import {TextArea} from '../textarea';
import {Checkbox} from '../checkbox';
import {IconButton} from '../icon-button';
import {Toggle} from '../toggle';
import {Container} from '../container';
import {Select} from '../select';
import {InputNumber} from '../input-number';

import {Carousel, CarouselTypes} from '../carousel';

import {Header} from 'app/components/header';
import {Footer} from 'app/components/footer';

const urls = [
    'http://lorempixel.com/800/600/sports/',
    'http://lorempixel.com/800/600/sports/',
    'http://lorempixel.com/800/600/sports/'
];

export class ShowKit extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Container>
                    <Carousel imgUrls={urls} type={CarouselTypes.small}/>
                    <Button caption="Add new" type="default"/><br/><br/>
                    <Button caption="Register" type="primary"/><br/><br/>
                    <Button caption="Log in" type="primary"/><br/><br/>
                    <Input placeholder="First Name" type="primary"/><br/><br/>
                    <Input placeholder="Country*" type="secondary"/><br/><br/>
                    {Object.values(IconTypes).map((iconType, index) => (
                        <span key={index} style={{padding: '0 10px'}}>
                        <Icon type={iconType}/>
                    </span>
                    ))}
                    <br/><br/>
                    <Button caption="Connect with google" type="primary" icon="google"/><br/><br/>
                    <Button caption="Connect with facebook" type="primary" icon="facebook"/><br/><br/>
                    <TextArea placeholder="placeholder"/><br/><br/>
                    <Checkbox/><br/><br/>
                    <Checkbox checked={true}/><br/><br/>
                    <IconButton icon="facebook"/><br/><br/>
                    <IconButton icon="twitter"/><br/><br/>
                    <Toggle toggleLeft={<Icon type="list"/>} toggleRight={<Icon type="grid"/>} value={false}/><br/><br/>
                    <Toggle toggleLeft="Day" toggleRight="Month" value={true}/><br/><br/>
                    <InputNumber/><br/><br/>
                    <Select/><br/><br/>
                </Container>
                <Footer/>
            </div>
        );
    }
}

