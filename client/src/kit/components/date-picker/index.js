import './date-picker.styl';
import React, {Component, PropTypes} from 'react';
import DatePickerDefault from 'react-datepicker';
import moment from 'moment';
import autobind from 'autobind-decorator';
import {noop} from 'lodash';
import classNames from 'classnames';

@autobind
export class DatePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value
        };
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
            this.setState({value: nextProps.value});
        }
    }

    handleChangeStart(date) {
        if (this.state.value.endDate < date ) {
            this.setState({value: {startDate: date, endDate: moment(date).add(1, 'day')}});
            this.props.onChange({startDate: date, endDate: moment(date).add(1, 'day')});
        } else {
            this.setState({value: {endDate: this.state.value.endDate, startDate: date}});
            this.props.onChange({startDate: date, endDate: this.state.value.endDate});
        }
    }

    handleChangeEnd(date) {
        if (this.state.value.startDate > date) {
            this.setState({value: {startDate: moment(date).subtract(1, 'day'), endDate: date}});
            this.props.onChange({startDate: moment(date).subtract(1, 'day'), endDate: date});
        } else {
            this.setState({value: {startDate: this.state.value.startDate, endDate: date}});
            this.props.onChange({startDate: this.state.value.startDate, endDate: date});
        }
    }

    render() {
        return (
            <div className={classNames('date-picker', this.props.className)} name={this.props.name}>
                <DatePickerDefault
                    className="date-picker__date date-picker__date_start"
                    selected={this.state.value.startDate}
                    startDate={this.state.value.startDate}
                    endDate={this.state.value.endDate}
                    onChange={this.handleChangeStart}
                    excludeDates={this.props.excludeDates}
                    minDate={this.props.minDate}
                />
                <DatePickerDefault
                    className="date-picker__date"
                    selected={this.state.value.endDate}
                    startDate={this.state.value.startDate}
                    endDate={this.state.value.endDate}
                    onChange={this.handleChangeEnd}
                    excludeDates={[...this.props.excludeDates, moment()]}
                    minDate={this.props.minDate}
                />
            </div>
        );
    }
}
DatePicker.propTypes = {
    onChange: PropTypes.func,
    name: PropTypes.string,
    className: PropTypes.string,
    excludeDates: PropTypes.array,
    minDate: PropTypes.object,
    value: PropTypes.object
};
DatePicker.defaultProps = {
    onChange: noop,
    name: '',
    className: '',
    excludeDates: [],
    minDate: moment(),
    value: {startDate: moment(), endDate: moment()}
};
