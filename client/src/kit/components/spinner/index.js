import './spinner.styl';
import React, {PropTypes} from 'react';
import classNames from 'classnames';

export const Spinner = ({className}) => {
    const classes = classNames('spinner', className);

    return <div className={classes}/>;
};

Spinner.propTypes = {
    className: PropTypes.string
};
