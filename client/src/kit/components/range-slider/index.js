import './range-slider.styl';
import React, {Component, PropTypes} from 'react';
import InputRange from 'react-input-range';
import autobind from 'autobind-decorator';
import {noop} from 'lodash';

@autobind
export class RangeSlider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
            this.setState({value: nextProps.value});
        }
    }

    onChange(value) {
        this.setState({value});
    }

    onChangeComplete(value) {
        this.props.onChange(value);
    }

    render() {
        return (
            <InputRange
                maxValue={this.props.maxValue}
                minValue={this.props.minValue}
                formatLabel={value => `${value} $`}
                value={this.state.value}
                onChange={this.onChange}
                onChangeComplete={this.onChangeComplete}
                step={this.props.step}
            />
        );
    }
}

RangeSlider.PropTypes = {
    value: PropTypes.shape({
        min: React.PropTypes.number,
        max: React.PropTypes.number
    }),
    maxValue: PropTypes.number,
    minValue: PropTypes.number,
    step: PropTypes.number,
    onChange: PropTypes.func
};

RangeSlider.defaultProps = {
    value: {
        min: 20,
        max: 50
    },
    maxValue: 100,
    minValue: 0,
    step: 1,
    onChange: noop
};

