import './validation-error.styl';
import React from 'react';

export const ValidationError = ({children}) => (
    <div className="validation-error">{children}</div>
);
