import './select.styl';
import React, {Component, PropTypes} from 'react';
import autobind from 'autobind-decorator';
import {noop} from 'lodash';
import SelectDefault from 'react-select';
import classNames from 'classnames';

export class Select extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
            this.setState({value: nextProps.value});
        }
    }

    @autobind
    onChange(value) {
        this.setState({value: value});
        this.props.onChange(value);
    }

    render() {
        return (
            <SelectDefault
                className={classNames('select-default', this.props.className)}
                name={this.props.name}
                value={this.state.value}
                options={this.props.options}
                onChange={this.onChange}
                multi={this.props.multi}
            />
        );
    }
}

Select.propTypes = {
    onChange: PropTypes.func,
    name: PropTypes.string,
    value: PropTypes.array,
    options: PropTypes.arrayOf(PropTypes.object),
    className: PropTypes.string,
    multi: PropTypes.bool
};

Select.defaultProps = {
    onChange: noop,
    name: '',
    value: [],
    options: [],
    className: '',
    multi: false
};


