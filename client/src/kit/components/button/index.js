import './button.styl';
import React, {PropTypes} from 'react';
import classNames from 'classnames';
import {Icon, IconTypes} from '../icon';
import {Spinner} from 'kit/components/spinner';
import {noop} from 'lodash';

export const ButtonTypes = {
    primary: 'primary',
    default: 'default',
    accent: 'accent'
};

export const ButtonSizes = {
    small: 'small',
    medium: 'medium'
};

const Classes = {
    [ButtonTypes.primary]: 'btn_primary',
    [ButtonTypes.default]: 'btn_default',
    [ButtonTypes.accent]: 'btn_accent',
    [ButtonSizes.small]: 'btn_small',
    [ButtonSizes.medium]: 'btn_medium'
};

export const Button = props => {
    let className = classNames(
        props.className,
        Classes[props.buttonType],
        Classes[props.buttonSize],
        'btn',
        {
            'btn_secondary': props.secondary,
            'btn_ternary': props.ternary
        },
        {
            'btn_busy': props.busy
        }
    );

    return (
        <button type={props.type} className={className} onClick={props.onClick} disabled={props.disabled}>
            {props.children}
            {props.caption}
            {props.secondary &&
            <div className="btn__icon">
                <Icon type={IconTypes.google}/>
            </div>
            }
            {props.ternary &&
            <div className="btn__icon">
                <Icon type={IconTypes.facebook}/>
            </div>
            }
            {props.busy &&
            <div className="btn__disabled"><Spinner/></div>
            }
        </button>
    );
};

Button.propTypes = {
    type: PropTypes.string,
    caption: PropTypes.string,
    buttonType: PropTypes.string.isRequired,
    buttonSize: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    className: PropTypes.string,
    secondary: PropTypes.bool,
    ternary: PropTypes.bool,
    busy: PropTypes.bool
};

Button.defaultProps = {
    type: 'button',
    caption: '',
    buttonType: '',
    buttonSize: ButtonSizes.medium,
    onClick: noop,
    disabled: false,
    className: '',
    secondary: false,
    ternary: false,
    busy: false
};
