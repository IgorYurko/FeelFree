import React from 'react';
import {shallow} from 'enzyme';
import {Link} from '../index';
import {NavLink as NavigationLink} from 'react-router-dom';

describe('Link', () => {
    const defaultPath = '/';
    const className = 'test__link';

    it('should return correct component NavigationLink from react-router', () => {
        const wrapper = shallow(<Link className={className}/>);

        expect(wrapper.find(NavigationLink).length).toBe(1);
    });

    it('should have correct classes when passed additional className prop', () => {
        const wrapper = shallow(<Link className={className}/>);

        expect(wrapper.hasClass(className)).toBe(true);
    });

    it(`should set prop "to"="${defaultPath}" by default, if no prop "to" passed`, () => {

        const wrapper = shallow(<Link/>);

        expect(wrapper.props().to).toBe(defaultPath);
    });
});


