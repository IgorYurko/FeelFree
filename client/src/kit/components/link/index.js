import React, {PropTypes} from 'react';
import {NavLink as NavigationLink} from 'react-router-dom';
import classNames from 'classnames';

export const Link = props => {
    let className = classNames('link', props.className);

    return <NavigationLink {...props} className={className}/>;
};

Link.propTypes = {
    to: PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.object
    ])
};

Link.defaultProps = {
    to: '/'
};
