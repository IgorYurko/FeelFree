import './container.styl';
import React, {PropTypes} from 'react';
import classNames from 'classnames';

export const Container = props => {
    let className = classNames('container', props.className);

    return <div className={className}>{props.children}</div>;
};

Container.PropTypes = {
    className: PropTypes.string,
    children: PropTypes.arrayOf(PropTypes.element).isRequired
};
