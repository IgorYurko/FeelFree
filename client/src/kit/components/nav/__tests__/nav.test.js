import React from 'react';
import {shallow} from 'enzyme';
import {Nav} from '../index';

describe('Nav', () => {
    const className = 'test__nav';

    it('should have correct classes when passed additional className prop', () => {
        const wrapper = shallow(<Nav className={className}/>);

        expect(wrapper.hasClass(className)).toBe(true);
    });
});


