import React from 'react';
import {shallow} from 'enzyme';
import {NavItem} from '../index';
import {Link} from '../../link';

describe('NavItem', () => {
    it('should pass prop "to" down to the NavItem', () => {
        const testPath = '/test';

        const wrapper = shallow(<NavItem to={testPath}/>);

        expect(wrapper.find(Link).props().to).toBe(testPath);
    });
});


