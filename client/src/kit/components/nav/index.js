import './nav.styl';
import React, {PropTypes} from 'react';
import classNames from 'classnames';
export {NavItem} from './nav-item';

export const Nav = props => {
    let className = classNames('nav', props.className);

    return <ul className={className}>{props.children}</ul>;
};

Nav.PropTypes = {
    className: PropTypes.string,
    children: PropTypes.arrayOf(PropTypes.element).isRequired
};


