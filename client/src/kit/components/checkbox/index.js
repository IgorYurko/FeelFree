import React, {Component, PropTypes} from 'react';
import autobind from 'autobind-decorator';
import {noop} from 'lodash';

export class Checkbox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: this.props.value
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.checked !== this.props.checked) {
            this.setState({checked: nextProps.checked});
        }
    }

    @autobind
    onChange(event) {
        this.setState({checked: event.target.checked});
        this.props.onChange(event);
    }

    render() {
        return (
            <input
                type="checkbox"
                className={this.props.className}
                name={this.props.name}
                checked={this.state.checked}
                disabled={this.props.disabled}
                placeholder={this.props.placeholder}
                onChange={this.onChange}
                onBlur={this.props.onBlur}
                onFocus={this.props.onFocus}
            />
        );
    }
}

Checkbox.propTypes = {
    value: PropTypes.bool,
    onChange: PropTypes.func,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    name: PropTypes.string
};

Checkbox.defaultProps = {
    value: false,
    onChange: noop,
    className: '',
    disabled: false,
    name: ''
};
