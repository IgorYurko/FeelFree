import './input.styl';
import React, {Component, PropTypes} from 'react';
import classNames from 'classnames';
import autobind from 'autobind-decorator';
import {noop} from 'lodash';

export const InputTypes = {
    primary: 'primary',
    secondary: 'secondary'
};

const Classes = {
    [InputTypes.primary]: 'input_primary',
    [InputTypes.secondary]: 'input_secondary'
};

export class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
            this.setState({value: nextProps.value});
        }
    }

    @autobind
    onChange(event) {
        this.setState({value: event.target.value});
        this.props.onChange(event);
    }

    render() {
        let className = classNames('input', Classes[this.props.inputType], this.props.className);
        return (
            <input
                type={this.props.type}
                className={className}
                name={this.props.name}
                value={this.state.value}
                disabled={this.props.disabled}
                placeholder={this.props.placeholder}
                onChange={this.onChange}
                onBlur={this.props.onBlur}
                onFocus={this.props.onFocus}
            />
        );
    }
}

Input.propTypes = {
    type: PropTypes.string,
    placeholder: PropTypes.string,
    inputType: PropTypes.string,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    name: PropTypes.string,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    className: PropTypes.string
};

Input.defaultProps = {
    type: 'text',
    placeholder: '',
    inputType: '',
    onChange: noop,
    disabled: false,
    name: '',
    value: '',
    className: ''
};

