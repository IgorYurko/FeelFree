import React from 'react';
import {shallow, mount} from 'enzyme';
import {Input, InputTypes} from '../';


describe('Input', () => {
    const placeholder = 'newPlaceholder';

    const newValue = 'newValue';

    it('Should exist right className(primary type)', () => {
        const wrapper = shallow(<Input inputType={InputTypes.primary}/>);
        expect(wrapper.find('.input').hasClass('input_primary')).toBeTruthy();
    });

    it('Should exist right className(secondary type)', () => {
        const wrapper = shallow(<Input inputType={InputTypes.secondary}/>);
        expect(wrapper.find('.input').hasClass('input_secondary')).toBeTruthy();
    });

    it('Should exist right placeholder', () => {
        const wrapper = mount(<Input placeholder={placeholder}/>);
        expect(wrapper.find('.input').node.placeholder).toEqual(placeholder);
    });

    it('Should change value state after input change', () => {
        const wrapper = shallow(<Input/>);
        expect(wrapper.state('value')).toEqual('');
        wrapper.find('.input').simulate('change', {target: {value: newValue}});
        expect(wrapper.state('value')).toEqual(newValue);
    });

    it('Should update the state of the component when the value prop is changed', () => {
        const component = mount(<Input/>);
        expect(component.find('input').node.value).toEqual('');
        component.setProps({ value: newValue });
        expect(component.state('value')).toEqual(newValue);
    });

    it('Should not update the state of the component', () => {
        const component = mount(<Input/>);
        expect(component.find('input').node.value).toEqual('');
        component.setProps({ value: '' });
        expect(component.state('value')).toEqual('');
    });
});
