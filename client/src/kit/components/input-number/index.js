import './input-number.styl';
import React, {Component, PropTypes} from 'react';
import {Icon, IconTypes} from '../icon';
import autobind from 'autobind-decorator';
import {noop, toNumber, toString} from 'lodash/fp';

@autobind
export class InputNumber extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: toString(this.props.value)
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
            this.setState({value: nextProps.value});
        }
    }

    onChange(event) {
        let newValue = event.target.value;
        if (toNumber(newValue) < 0) {
            let minValue = toString(this.props.min);
            this.setState({value: minValue});
            this.props.onChange(minValue);
        } else if (toNumber(newValue) > this.props.max) {
            let maxValue = toString(this.props.max);
            this.setState({value: maxValue});
            this.props.onChange(maxValue);
        } else if (isNaN(newValue)) {
            this.setState({value: ''});
        } else {
            this.setState({value: newValue});
            this.props.onChange(newValue);
        }
    }

    valueDown() {
        if (toNumber(this.state.value) > this.props.min) {
            const newValue = toString(toNumber(this.state.value) - 1);
            this.setState({value: newValue});
            this.props.onChange(newValue);
        } else if (toNumber(this.state.value) === this.props.min) {
            this.setState({value: ''});
            this.props.onChange('');
        }
    }

    valueUp() {
        if (toNumber(this.state.value) < this.props.max) {
            const newValue = toString(toNumber(this.state.value) + 1);
            this.setState({value: newValue});
            this.props.onChange(newValue);
        }
    }

    render() {
        return (
            <div className="input-number">
                <button className="input-number__down" onClick={this.valueDown} type="button">
                    <Icon type={IconTypes.chevronLeft}/>
                </button>
                <input
                    className="input-number__content"
                    type="tel"
                    value={this.state.value}
                    onChange={this.onChange}
                    name={this.props.name}
                    min={this.props.min}
                    max={this.props.max}
                />
                <button className="input-number__up" onClick={this.valueUp} type="button">
                    <Icon type={IconTypes.chevronRight}/>
                </button>
            </div>
        );
    }
}

InputNumber.propTypes = {
    onChange: PropTypes.func,
    name: PropTypes.string,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    min: PropTypes.number,
    max: PropTypes.number
};

InputNumber.defaultProps = {
    onChange: noop,
    name: '',
    min: 1,
    max: 10,
    value: ''
};
