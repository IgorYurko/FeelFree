let webpack = require('webpack');
let webpackMerge = require('webpack-merge');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let commonConfig = require('./webpack.config.common.js');

module.exports = webpackMerge.smart(commonConfig, {
    plugins: [
        new ExtractTextPlugin('styles.css'),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ],
    module: {
        rules: [
            {
                test: /\.(styl)$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-loader',
                        {
                            loader: 'stylus-loader',
                            options: {
                                'resolve url': true
                            }
                        }
                    ]
                })
            }
        ]
    }
});
