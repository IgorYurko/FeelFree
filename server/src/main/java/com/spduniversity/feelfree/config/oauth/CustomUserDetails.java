package com.spduniversity.feelfree.config.oauth;

import com.spduniversity.feelfree.domain.dto.RoleDTO;
import com.spduniversity.feelfree.domain.dto.UserDetailsDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class CustomUserDetails implements UserDetails {
	
	private static final long serialVersionUID = -4796846438725232955L;
	private static final String ROLE_PREFIX = "ROLE_";
	private Collection<? extends GrantedAuthority> authorities;
	private String password;
	private String username;
	
	public CustomUserDetails(UserDetailsDTO user) {
		this.username = user.getProfileEmail();
		this.password = user.getPassword();
		this.authorities = translate(user.getRoles());
	}
	
	private Collection<? extends GrantedAuthority> translate(Collection<RoleDTO> roles) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (RoleDTO role : roles) {
			String roleName = StringUtils.prependIfMissing(role.getValue().toUpperCase(), ROLE_PREFIX);
			authorities.add(new SimpleGrantedAuthority(roleName));
		}
		return authorities;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}
	
	@Override
	public String getPassword() {
		return password;
	}
	
	@Override
	public String getUsername() {
		return username;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	
	@Override
	public boolean isEnabled() {
		return true;
	}
}
