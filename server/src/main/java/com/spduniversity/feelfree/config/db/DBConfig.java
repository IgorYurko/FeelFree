package com.spduniversity.feelfree.config.db;

import com.spduniversity.feelfree.config.ApplicationConfig.YmlConfig;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ComponentScan(basePackages = {"com.spduniversity.feelfree.domain"})
@PropertySource({"classpath:config/dbConfig.properties"})
@EnableJpaRepositories(basePackages = {"com.spduniversity.feelfree.domain.repository"})
@EnableTransactionManagement
@Import({DbConfigDev.class, DbConfigTest.class})
public class DBConfig {
	
	private static final String profile = YmlConfig.get().getProperty("spring.profiles.active");
	
	@Resource
	private Environment env;
	
	@Bean(initMethod = "migrate")
	@Primary
	public Flyway flyway(DataSource dataSource) {
		Flyway flyway = new Flyway();
		flyway.setBaselineOnMigrate(false);
		flyway.setLocations(env.getProperty(profile + ".db.config.flyway.locations", String[].class));
		flyway.setDataSource(dataSource);
		return flyway;
	}
	
	@Bean
	@DependsOn("flyway")
	@Primary
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, @Qualifier("hibernateProperties") Properties properties) {
		LocalContainerEntityManagerFactoryBean managerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		managerFactoryBean.setDataSource(dataSource);
		managerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		managerFactoryBean.setPackagesToScan(env.getProperty("common.db.config.entityManager.packagesToScan", String[].class));
		managerFactoryBean.setJpaProperties(properties);
		managerFactoryBean.setPersistenceUnitName(env.getProperty("common.db.config.entityManager.unitName"));
		return managerFactoryBean;
	}
	
	@Bean
	public HibernateJpaSessionFactoryBean sessionFactory(EntityManagerFactory entityManagerFactory) {
		HibernateJpaSessionFactoryBean hibernateJpaSessionFactoryBean = new HibernateJpaSessionFactoryBean();
		hibernateJpaSessionFactoryBean.setEntityManagerFactory(entityManagerFactory);
		hibernateJpaSessionFactoryBean.setPersistenceUnitName(env.getProperty("common.db.config.sessionFactory.unitName"));
		return hibernateJpaSessionFactoryBean;
	}
	
	@Bean
	public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
	
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}
	
	@Bean
	public Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.max_fetch_depth", env.getProperty(profile + ".db.properties.hibernate.maxFetchDepth"));
		properties.put("hibernate.jdbc.fetch_size", env.getProperty(profile + ".db.properties.hibernate.jdbc.fetchSize"));
		properties.put("hibernate.jdbc.batch_size", env.getProperty(profile + ".db.properties.hibernate.jdbc.batchSize"));
		properties.put("hibernate.show_sql", env.getProperty(profile + ".db.properties.hibernate.showSql"));
		properties.put("hibernate.format_sql", env.getProperty(profile + ".db.properties.hibernate.formatSql"));
		properties.put("hibernate.use_sql_comments", env.getProperty(profile + ".db.properties.hibernate.useSqlComments"));
		properties.put("hibernate.generate_statistics", env.getProperty(profile + ".db.properties.hibernate.generateStatistics"));
		properties.put("hibernate.order_inserts", env.getProperty(profile + ".db.properties.hibernate.orderInserts"));
		properties.put("hibernate.order_updates", env.getProperty(profile + ".db.properties.hibernate.orderUpdates"));
		properties.put("hibernate.jdbc.batch_versioned_data", env.getProperty(profile + ".db.properties.hibernate.jdbc.batchVersionedData"));
		properties.put("hibernate.id.new_generator_mappings", env.getProperty(profile + ".db.properties.hibernate.id.newGeneratorMappings"));
		properties.put("hibernate.jdbc.use_streams_for_binary", env.getProperty(profile + ".db.properties.hibernate.jdbc.useStreamsForBinary"));
		properties.put("hibernate.connection.useUnicode", env.getProperty(profile + ".db.properties.hibernate.connection.useUnicode"));
		properties.put("hibernate.connection.characterEncoding", env.getProperty(profile + ".db.properties.hibernate.connection.characterEncoding"));
		properties.put("hibernate.connection.useJDBCCompliantTimezoneShift", env.getProperty(profile + ".db.properties.hibernate.connection.useJDBCCompliantTimezoneShift"));
		properties.put("hibernate.connection.useLegacyDatetimeCode", env.getProperty(profile + ".db.properties.hibernate.connection.useLegacyDatetimeCode"));
		properties.put("hibernate.connection.serverTimezone", env.getProperty(profile + ".db.properties.hibernate.connection.serverTimezone"));
		properties.put("hibernate.connection.useSSL", env.getProperty(profile + ".db.properties.hibernate.connection.useSSL"));
		properties.put("hibernate.connection.maxAllowedPacket", env.getProperty(profile + ".db.properties.hibernate.connection.maxAllowedPacket"));
		properties.put("hibernate.current_session_context_class", "org.springframework.orm.hibernate5.SpringSessionContext");
		properties.put("hibernate.dialect", env.getProperty(profile + ".db.properties.hibernate.dialect"));
		return properties;
	}
}
