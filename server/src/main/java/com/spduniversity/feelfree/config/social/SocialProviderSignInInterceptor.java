package com.spduniversity.feelfree.config.social;

import com.spduniversity.feelfree.service.common.CookieService;
import com.spduniversity.feelfree.service.common.SocialAuthorizeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.social.connect.Connection;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.spduniversity.feelfree.service.common.extra.VariablesContainer.CLIENT_ID_SOCIAL;

public abstract class SocialProviderSignInInterceptor {
	
	public static final String DEFAULT_TOKEN_VALUE_SESSION_NAME = "social-token";
	public static final String DEFAULT_SOCIAL_ERROR_SESSION_NAME = "social-error";
	public static final String DEFAULT_ACTIVATION_ERROR_SESSION_NAME = "activation-error";
	protected SocialAuthorizeService socialAuthorizeService;
	protected CookieService cookieService;
	
	protected SocialProviderSignInInterceptor(SocialAuthorizeService socialAuthorizeService, CookieService cookieService) {
		this.socialAuthorizeService = socialAuthorizeService;
		this.cookieService = cookieService;
	}
	
	protected void authorize(ServletWebRequest request, Connection<?> connection, String id) {
		HashMap<String, Object> sessionAttributes = new HashMap<>();
		if (socialAuthorizeService.isAuthorize(id)) {
			OAuth2AccessToken token = socialAuthorizeService.authorizeUser(id, CLIENT_ID_SOCIAL);
			if (token != null) {
				sessionAttributes.put(DEFAULT_TOKEN_VALUE_SESSION_NAME, token.getValue());
				setSessionAttributes(request, sessionAttributes);
				return;
			}
			sessionAttributes.put(DEFAULT_SOCIAL_ERROR_SESSION_NAME, "Can not authorize, please try again or contact for support");
			setSessionAttributes(request, sessionAttributes);
			return;
		}
		if (socialAuthorizeService.isTemporary(id)) {
			sessionAttributes.put(DEFAULT_SOCIAL_ERROR_SESSION_NAME, "Please Confirm your activation in you mail box");
			setSessionAttributes(request, sessionAttributes);
			return;
		}
		sessionAttributes.put(DEFAULT_ACTIVATION_ERROR_SESSION_NAME, "Activation time has expired, re-register, please");
		setSessionAttributes(request, sessionAttributes);
		socialAuthorizeService.deleteConnection(connection);
	}
	
	protected void setSessionAttributes(ServletWebRequest request, Map<String, Object> sessionAttributes) {
		HttpSession session = request.getRequest().getSession(true);
		sessionAttributes.entrySet()
						 .stream()
						 .filter(e -> StringUtils.isNotBlank(e.getKey()) && Objects.nonNull(e.getValue()))
						 .forEach(e -> session.setAttribute(e.getKey(), e.getValue()));
	}
}
