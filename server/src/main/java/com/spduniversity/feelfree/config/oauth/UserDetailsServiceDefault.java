package com.spduniversity.feelfree.config.oauth;

import com.spduniversity.feelfree.service.domain.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.annotation.Resource;
import java.util.Optional;

public class UserDetailsServiceDefault implements UserDetailsService {

    @Resource
    public UserService service;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return Optional.ofNullable(service.getUserDetailsData(username))
                       .map(CustomUserDetails::new)
                       .orElseThrow(() -> new UsernameNotFoundException("User email not found"));
    }
}
