package com.spduniversity.feelfree.config.oauth;

import com.google.common.base.Splitter;
import com.spduniversity.feelfree.config.ApplicationConfig.YmlSettings;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorsFilter implements Filter {
	
	private static final List<String> ALLOW_ORIGIN = Splitter.on(",")
															 .trimResults()
															 .omitEmptyStrings()
															 .splitToList(YmlSettings.get().getProperty("base.cors.allow.origins"));;
															 
	private static final String BASE_ORIGIN = YmlSettings.get().getProperty("base.url.server");
	private static final String ALL_ORIGIN = "*";
	
	public CorsFilter() {}
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		String origin = getOrigin(request);
		HttpServletResponse response = (HttpServletResponse) res;
		response.setHeader("Access-Control-Allow-Origin", origin);
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
		if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
			response.setStatus(HttpServletResponse.SC_OK);
		} else {
			chain.doFilter(req, res);
		}
	}
	
	@Override
	public void init(FilterConfig filterConfig) {}
	
	@Override
	public void destroy() {}
	
	private String getOrigin(HttpServletRequest req) {
		String origin = req.getHeader(HttpHeaders.ORIGIN);
		if (StringUtils.isBlank(origin)) {
			return BASE_ORIGIN;
		}
		if (ALLOW_ORIGIN.contains(origin)) {
			return origin;
		}
		return ALL_ORIGIN;
	}
}