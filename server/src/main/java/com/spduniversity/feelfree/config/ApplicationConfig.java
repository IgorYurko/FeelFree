package com.spduniversity.feelfree.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.spduniversity.feelfree.config.oauth.UserDetailsServiceDefault;
import com.spduniversity.feelfree.service.common.extra.EntityMapper;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.authentication.encoding.BasePasswordEncoder;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;
import java.util.Properties;
import java.util.concurrent.Executor;

@Configuration
@EnableAsync
@PropertySource({"classpath:config/mapperConfig.properties"})
@ComponentScan(basePackages = {"com.spduniversity.feelfree.web", "com.spduniversity.feelfree.service"})
public class ApplicationConfig {
	
	@Resource
	private Environment env;
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
		configurer.setProperties(YmlSettings.get());
		return configurer;
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new EntityMapper(env.getProperty("entityMapper.prefixFilter"), env.getProperty("entityMapper.suffixBean"));
		objectMapper.findAndRegisterModules();
		objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
		objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		objectMapper.configure(SerializationFeature.FAIL_ON_SELF_REFERENCES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		objectMapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
		return objectMapper;
	}
	
	@Bean
	@Primary
	public UserDetailsService userDetailsService() {
		return new UserDetailsServiceDefault();
	}
	
	@Bean
	public PasswordEncoder bCryptEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public BasePasswordEncoder md5Encoder() {
		return new Md5PasswordEncoder();
	}
	
	@Bean
	public Executor taskExecutor() {
		return new SimpleAsyncTaskExecutor();
	}

	public static class YmlConfig {
		public static Properties get() {
			return Inner.getInstance();
		}
		private static class Inner {
			private static Properties instance = null;
			private static Properties getInstance() {
				if (instance == null) {
					YamlPropertiesFactoryBean yml = new YamlPropertiesFactoryBean();
					yml.setResources(new ClassPathResource("application.yml"));
					instance = yml.getObject();
				}
				return instance;
			}
		}
	}
	
	public static class YmlSettings {
		public static Properties get() {
			return Inner.getInstance();
		}
		private static class Inner {
			private static Properties instance = null;
			private static Properties getInstance() {
				if (instance == null) {
					YamlPropertiesFactoryBean yml = new YamlPropertiesFactoryBean();
					if (ApplicationProfile.PROD.equalsIgnoreCase(YmlConfig.get().getProperty("spring.profiles.active"))) {
						yml.setResources(new ClassPathResource("settings.prod.yml"));
					} else {
						yml.setResources(new ClassPathResource("settings.yml"));
					}
					instance = yml.getObject();
				}
				return instance;
			}
		}
	}
}
