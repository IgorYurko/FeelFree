package com.spduniversity.feelfree.config.oauth;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.bind.annotation.CrossOrigin;

@Configuration
@EnableResourceServer
@CrossOrigin
public class OAuth2ResourceConfig extends ResourceServerConfigurerAdapter {
    
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
             .and()
                .authorizeRequests()
                .antMatchers("/api/secure/**").hasAnyRole("USER")
                .antMatchers("/api/v1/user/**").hasAnyRole("USER")
                .antMatchers("/api/v1/account/**").hasAnyRole("USER", "ADMIN")
                .anyRequest().permitAll()
             .and()
                .csrf()
                .disable();
    }

}
