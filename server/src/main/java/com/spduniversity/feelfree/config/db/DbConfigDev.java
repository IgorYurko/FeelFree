package com.spduniversity.feelfree.config.db;

import com.spduniversity.feelfree.config.ApplicationConfig.YmlConfig;
import com.spduniversity.feelfree.config.ApplicationProfile;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@Profile({ApplicationProfile.DEV, ApplicationProfile.PROD})
public class DbConfigDev {
	
	private static final String profile = YmlConfig.get().getProperty("spring.profiles.active");
	
	@Resource
	private Environment env;
	
	@Bean
	@Primary
	public DataSource dataSource(@Qualifier("dataSourceProperties") Properties properties) {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty(profile + ".db.config.datasource.driver"));
		dataSource.setUrl(env.getProperty(profile + ".db.config.datasource.url"));
		dataSource.setUsername(YmlConfig.get().getProperty("dev.db.config.datasource.user"));
		dataSource.setPassword(YmlConfig.get().getProperty("dev.db.config.datasource.password"));
		dataSource.setConnectionProperties(properties);
		return dataSource;
	}
	
	@Bean
	public Properties dataSourceProperties() {
		Properties properties = new Properties();
		properties.put("useUnicode", env.getProperty(profile + ".db.properties.dataSource.useUnicode"));
		properties.put("characterEncoding", env.getProperty(profile + ".db.properties.dataSource.characterEncoding"));
		properties.put("useJDBCCompliantTimezoneShift", env.getProperty(profile + ".db.properties.dataSource.useJDBCCompliantTimezoneShift"));
		properties.put("useLegacyDatetimeCode", env.getProperty(profile + ".db.properties.dataSource.useLegacyDatetimeCode"));
		properties.put("serverTimezone", env.getProperty(profile + ".db.properties.dataSource.serverTimezone"));
		properties.put("useSSL", env.getProperty(profile + ".db.properties.dataSource.useSSL"));
		properties.put("maxAllowedPacket", env.getProperty(profile + ".db.properties.dataSource.maxAllowedPacket"));
		properties.put("blobSendChunkSize", env.getProperty(profile + ".db.properties.dataSource.blobSendChunkSize"));
		properties.put("autoReconnect", env.getProperty(profile + ".db.properties.dataSource.autoReconnect"));
		properties.put("failOverReadOnly", env.getProperty(profile + ".db.properties.dataSource.failOverReadOnly"));
		properties.put("maxReconnects", env.getProperty(profile + ".db.properties.dataSource.maxReconnects"));
		properties.put("useOldAliasMetadataBehavior", env.getProperty(profile + ".db.properties.dataSource.useOldAliasMetadataBehavior"));
		return properties;
	}
}
