package com.spduniversity.feelfree.config.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.Resource;

@EnableSwagger2
@EnableWebMvc
@PropertySource({"classpath:config/swaggerConfig.properties"})
@Configuration
public class SwaggerConfig extends WebMvcConfigurerAdapter {
	
	@Resource
	private Environment env;
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("swagger-ui.html")
				.addResourceLocations("classpath:/META-INF/resources/");
		
		registry.addResourceHandler("/webjars/**")
				.addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
	
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any())
				.build()
				.apiInfo(apiInfo());
	}
	
	private ApiInfo apiInfo() {
		return new ApiInfo(
				env.getProperty("apiInfo.title"),
				env.getProperty("apiInfo.description"),
				env.getProperty("apiInfo.version"),
				env.getProperty("apiInfo.termsUrl"),
				new Contact(env.getProperty("apiInfo.contact.name"),
							env.getProperty("apiInfo.contact.url"),
							env.getProperty("apiInfo.contact.email")),
				env.getProperty("apiInfo.license"),
				env.getProperty("apiInfo.licenseUrl"));
	}
}

