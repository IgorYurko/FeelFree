package com.spduniversity.feelfree.config.social;

import com.spduniversity.feelfree.service.common.CookieService;
import com.spduniversity.feelfree.service.common.SocialAuthorizeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurerAdapter;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.connect.web.*;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.connect.GoogleConnectionFactory;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.ArrayList;

@Slf4j
@Configuration
@EnableSocial
public class SocialConfig extends SocialConfigurerAdapter {
	
	@Resource
	private DataSource dataSource;
	
	@Resource
	private CookieService cookieService;
	
	@Resource
	private SocialAuthorizeService socialAuthorizeService;
	
	@Override
	public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
		TextEncryptor textEncryptor = Encryptors.noOpText();
		return new JdbcUsersConnectionRepository(dataSource, connectionFactoryLocator, textEncryptor);
	}
	
	@Override
	public void addConnectionFactories(ConnectionFactoryConfigurer connectionFactoryConfigurer, Environment environment) {
		GoogleConnectionFactory googleConnectionFactory = new GoogleConnectionFactory(environment.getProperty("social.google.appId"),
																					  environment.getProperty("social.google.appSecret"));
		googleConnectionFactory.setScope("email");
		connectionFactoryConfigurer.addConnectionFactory(googleConnectionFactory);
		
		FacebookConnectionFactory facebookConnectionFactory = new FacebookConnectionFactory(environment.getProperty("social.facebook.appId"),
																							environment.getProperty("social.facebook.appSecret"));
		facebookConnectionFactory.setScope("email,public_profile");
		connectionFactoryConfigurer.addConnectionFactory(facebookConnectionFactory);
	}
	
	@Bean
    public ConnectController connectController(ConnectionFactoryLocator connectionFactoryLocator,
											   ConnectionRepository connectionRepository) {
		
		return new ConnectController(connectionFactoryLocator, connectionRepository);
    }
	
	@Bean
	public ProviderSignInController providerSignInController(ConnectionFactoryLocator connectionFactoryLocator,
															 UsersConnectionRepository usersConnectionRepository,
															 SignInAdapter signInAdapter) {
		ProviderSignInController providerSignInController = new ProviderSignInController(connectionFactoryLocator,
																						 usersConnectionRepository,
																						 signInAdapter);
		providerSignInController.setSignInInterceptors(new ArrayList<ProviderSignInInterceptor<?>>(){{
			add(facebookProviderSignInInterceptor());
			add(googleProviderSignInInterceptor());
		}});
		providerSignInController.setPostSignInUrl("/api/v1/registration/social/sign-in");
		providerSignInController.setSignUpUrl("/api/v1/registration/social/sign-up");
		return providerSignInController;
	}
	
	@Bean
	public ProviderSignInUtils getProviderSignInUtils(ConnectionFactoryLocator connectionFactoryLocator,
													  UsersConnectionRepository usersConnectionRepository) {
		return new ProviderSignInUtils(connectionFactoryLocator, usersConnectionRepository);
	}
	
	@Bean
	public SignInAdapter signInAdapter() {
		return new SignInAdapterDefault();
	}
	
	@Bean
	ProviderSignInInterceptor<Facebook> facebookProviderSignInInterceptor() {
		return new FacebookProviderSignInInterceptor(socialAuthorizeService, cookieService);
	}
	
	@Bean
	ProviderSignInInterceptor<Google> googleProviderSignInInterceptor() {
		return new GoogleProviderSignInInterceptor(socialAuthorizeService, cookieService);
	}
}
