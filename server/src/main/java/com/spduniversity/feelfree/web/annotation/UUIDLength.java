package com.spduniversity.feelfree.web.annotation;

import com.spduniversity.feelfree.web.annotation.impl.UUIDLengthValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = UUIDLengthValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface UUIDLength {
	String message() default "{annotation.uuidLength.default}";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	int equal() default 0;
}
