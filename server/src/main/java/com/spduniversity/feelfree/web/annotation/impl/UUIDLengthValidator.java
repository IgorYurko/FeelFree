package com.spduniversity.feelfree.web.annotation.impl;

import com.spduniversity.feelfree.web.annotation.UUIDLength;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UUIDLengthValidator implements ConstraintValidator<UUIDLength, String> {
	
	private int equal;
	private String message;
	
	@Override
	public void initialize(UUIDLength constraintAnnotation) {
		this.equal = constraintAnnotation.equal();
		this.message = constraintAnnotation.message();
	}
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (StringUtils.stripToNull(value) != null) {
			if (value.trim().length() != equal) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
				return false;
			}
			return true;
		}
		return false;
	}
}
