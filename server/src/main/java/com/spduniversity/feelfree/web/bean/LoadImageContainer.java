package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.web.annotation.UUIDLength;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Data
public class LoadImageContainer {
	
	@NotEmpty
	@UUIDLength(equal = 32)
	private String name;
	private Integer height;
	private Integer width;
}
