package com.spduniversity.feelfree.web.v1;


import com.spduniversity.feelfree.domain.dto.LoadImageDTO;
import com.spduniversity.feelfree.service.web.LoadService;
import com.spduniversity.feelfree.web.bean.LoadImageContainer;
import com.spduniversity.feelfree.web.common.ResponseCreator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.sql.SQLException;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/load")
public class LoadController {
	
	@Resource
	private LoadService service;
	
	@PreAuthorize("permitAll()")
	@GetMapping(value = "/image", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> image(LoadImageContainer loadImageContainer) throws Exception {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(loadImageContainer);
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		LoadImageDTO image = service.getImage(loadImageContainer);
		if (image == null) {
			return ResponseCreator.notFound("image.load.notFound").build();
		}
		try {
			return ResponseEntity.ok()
								 .contentLength(image.getValue().length())
								 .contentType(MediaType.valueOf(image.getType()))
								 .body(new InputStreamResource(image.getValue().getBinaryStream()));
		} catch (SQLException ex) {
			log.error(ex.getMessage(), ex);
		}
		return ResponseCreator.error("image.load.serverError").build();
	}
}
