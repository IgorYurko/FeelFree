package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.web.common.Length;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

@Data
public class SearchGeolocationContainer {
	
	@NotNull
	@Range(min = -90, max = 90)
	private Double latitude;
	
	@NotNull
	@Range(min = -180, max = 180)
	private Double longitude;
	private Integer radius;
	private Length length;
	private Integer page;
	private Integer size;
}
