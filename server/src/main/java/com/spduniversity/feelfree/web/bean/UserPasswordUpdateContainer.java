package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.web.annotation.FieldEquals;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldEquals(field = "password", equalsTo = "passwordConfirm")
public class UserPasswordUpdateContainer {
    @NotNull
    @Length(min = 6, max = 20)
    private String oldPassword;
    @NotNull
    @Length(min = 6, max = 20)
    private String password;
    @NotNull
    @Length(min = 6, max = 20)
    private String passwordConfirm;

}
