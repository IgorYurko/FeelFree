package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.service.web.bean.UserContainer;
import com.spduniversity.feelfree.web.annotation.FieldEquals;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;

import static com.spduniversity.feelfree.service.common.extra.VariablesContainer.EMAIL_PATTERN;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldEquals(field = "password", equalsTo = "passwordConfirm")
public class UserRegisterContainer implements UserContainer {
        
        @Length(max = 50)
        private String firstName;
        @Length(max = 50)
        private String lastName;

        @NotBlank
        @Length(max = 320)
        @Pattern(regexp = EMAIL_PATTERN, message = "{annotation.parameters.email.invalid}")
        private String profileEmail;
        
        @NotBlank
        @Length(min = 7, max = 15)
        private String password;
        
        @NotBlank
        @Length(min = 7, max = 15)
        private String passwordConfirm;
}
