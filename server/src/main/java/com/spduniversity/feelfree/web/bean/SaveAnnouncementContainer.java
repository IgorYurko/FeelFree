package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.domain.model.Price;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaveAnnouncementContainer {
	
	private String uuid;
	
	@NotBlank
	private String country;
	
	@NotBlank
	private String city;
	
	private String region;

	private String placeId;
	
	@NotBlank
	private String street;
	
	private Integer appNumber;
	
	@NotNull
	private Price.Period period;
	
	@NotNull
	@Min(1)
	@Max(1000000)
	private Integer price;
	
	@NotNull
	@Min(1)
	private Integer roomCount;
	
	@NotNull
	@Min(1)
	private Integer livingPlacesCount;
	
	@NotBlank
	private String title;
	
	@NotNull
	@Range(min = -90, max = 90)
	private Double latitude;
	
	@NotNull
	@Range(min = -180, max = 180)
	private Double longitude;
	
	private String shortDescription;
	private Boolean wiFi;
	private Boolean washingMachine;
	private Boolean refrigerator;
	private Boolean hairDryer;
	private Boolean iron;
	private Boolean smoking;
	private Boolean animals;
	private Boolean kitchenStuff;
	private Boolean conditioner;
	private Boolean balcony;
	private Boolean tv;
	private Boolean essentials;
	private Boolean shampoo;
}
