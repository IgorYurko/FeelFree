package com.spduniversity.feelfree.web.v1;


import com.spduniversity.feelfree.domain.dto.*;
import com.spduniversity.feelfree.service.web.SearchService;
import com.spduniversity.feelfree.web.bean.AccountPageableCommentContainer;
import com.spduniversity.feelfree.web.bean.SearchAnnouncementsContainer;
import com.spduniversity.feelfree.web.bean.SearchGeolocationContainer;
import com.spduniversity.feelfree.web.bean.UUIDContainer;
import com.spduniversity.feelfree.web.common.ResponseCreator;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/search")
public class SearchController {
	
	@Resource
	private SearchService service;
	
	@GetMapping(value = "/geolocation", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> geolocation(SearchGeolocationContainer container) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		Page<GeolocationSearchDTO> geolocations = service.getGeolocations(container);
		if (CollectionUtils.isEmpty(geolocations.getContent())) {
			return ResponseCreator.noContent().build();
		}
		return ResponseCreator.ok(geolocations).build();
	}

	@GetMapping(value = "/announcement", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> announcements(SearchAnnouncementsContainer searchAnnouncementsContainer) {
		Page<AnnouncementSearchDTO> announcements = service.getAnnouncements(searchAnnouncementsContainer);
		if (CollectionUtils.isEmpty(announcements.getContent())) {
			return ResponseCreator.noContent().build();
		}
		return ResponseCreator.ok(announcements).build();
	}
	
	@GetMapping(value = "/announcement/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> announcementInfo(@PathVariable(required = false) String uuid) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(UUIDContainer.of(uuid));
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		AnnouncementSearchSingleDTO announcement = service.getAnnouncement(uuid);
		if (announcement == null) {
			return ResponseCreator.noContent().build();
		}
		return ResponseCreator.ok(announcement).build();
	}
	
	@GetMapping(value = "/announcement/{uuid}/comment", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> commentInfo(@PathVariable(required = false) String uuid, AccountPageableCommentContainer container) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(UUIDContainer.of(uuid));
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		Page<CommentDTO> comments = service.getComments(container, uuid);
		if (CollectionUtils.isEmpty(comments.getContent())) {
			return ResponseCreator.noContent().build();
		}
		return ResponseCreator.ok(comments).build();
	}
	
	@GetMapping(value = "/announcement/price-range", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> priceRangeInfo() {
		MinMaxPriceDTO priceRange = service.getPriceRange();
		if (priceRange == null) {
			return ResponseCreator.error("").build();
		}
		return ResponseCreator.ok(priceRange).build();
	}
}
