package com.spduniversity.feelfree.web.v1;

import com.spduniversity.feelfree.domain.dto.*;
import com.spduniversity.feelfree.service.common.util.UserSecurityUtil;
import com.spduniversity.feelfree.service.web.AccountService;
import com.spduniversity.feelfree.web.bean.*;
import com.spduniversity.feelfree.web.common.ResponseCreator;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/account")
public class AccountAnnouncementController {
	
	@Resource
	private AccountService accountService;
	
	@GetMapping(value = "/announcement", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> announcementsInfo(AccountAnnouncementContainer container) {
		Page<AnnouncementAccountPreviewDTO> announcements = accountService.getAnnouncements(container, UserSecurityUtil.getName());
		if (CollectionUtils.isEmpty(announcements.getContent())) {
			return ResponseCreator.notFound("item.notFound").build();
		}
		return ResponseCreator.ok(announcements).build();
	}
	
	@GetMapping(value = "/announcement/{uuid}/rent", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> announcementRentInfo(@PathVariable(required = false) String uuid) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(UUIDContainer.of(uuid));
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		List<RentedDTO> rents = accountService.getRents(uuid, UserSecurityUtil.getName());
		if (CollectionUtils.isEmpty(rents)) {
			return ResponseCreator.notFound("rents.notFound", uuid).build();
			
		}
		return ResponseCreator.ok(rents).build();
	}
	
	@GetMapping(value = "/announcement/{uuid}/rented-dates", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> announcementRentedDatesInfo(@PathVariable("uuid") String uuid, AccountAnnouncementRentedDateContainer container) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		List<RentedDatesDTO> rentedDates = accountService.getRentedDates(container, uuid, UserSecurityUtil.getName());
		return ResponseCreator.ok(rentedDates).build();
	}
	
	@PutMapping(value = "/announcement/rented-dates", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> announcementRentedDatesSwitch(@RequestBody AccountAnnouncementRentedSwitchContainer container) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		if (accountService.updateRent(container, UserSecurityUtil.getName())) {
			return ResponseCreator.ok(container).build();
		}
		return ResponseCreator.error("rentedDate.error").build();
	}
	
	@PutMapping(value = "/announcement/hide", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> announcementUpdate(@RequestBody HideAnnouncementContainer container) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		if (accountService.updateAnnouncement(container, UserSecurityUtil.getName())) {
			return ResponseCreator.ok(container).build();
		}
		return ResponseCreator.error("hide.error").build();
	}
	
	@PostMapping(value = "/announcement", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> announcementSave(@RequestBody SaveAnnouncementContainer container) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		UUIDDTO uuid = accountService.saveAnnouncement(container, UserSecurityUtil.getName());
		if (uuid == null) {
			return ResponseCreator.error("announcement.save.error").build();
		}
		return ResponseCreator.ok(uuid).build();
	}
	
	@GetMapping(value = "/announcement/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> announcementInfo(@PathVariable(required = false) String uuid) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(UUIDContainer.of(uuid));
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		AnnouncementAccountDTO announcement = accountService.getAnnouncement(uuid, UserSecurityUtil.getName());
		if (announcement == null) {
			return ResponseCreator.notFound("announcement.notFound", uuid).build();
		}
		return ResponseCreator.ok(announcement).build();
	}
	
	@DeleteMapping(value = "/announcement/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> announcementDelete(@PathVariable(required = false) String uuid, Principal principal) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(UUIDContainer.of(uuid));
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		if (accountService.deleteAnnouncement(uuid, principal.getName())) {
			return ResponseCreator.ok(new UUIDDTO(uuid)).build();
		}
		return ResponseCreator.error("announcement.delete.error", uuid).build();
	}
	
	@PostMapping(value = "/announcement/image", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> imagesSave(UploadImageContainer container) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		List<ImageDTO> result = accountService.saveImage(container, UserSecurityUtil.getName());
		if (CollectionUtils.isEmpty(result)) {
			return ResponseCreator.error("image.save.error").build();
		}
		return ResponseCreator.ok(result).build();
	}
	
	@DeleteMapping(value = "/announcement/{announcementUuid}/image/{imageUuid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> imageDelete(@PathVariable(required = false) String announcementUuid,
										 @PathVariable(required = false) String imageUuid) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(new AccountImageDeleteContainer(announcementUuid, imageUuid));
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		if (accountService.deleteImage(announcementUuid, imageUuid, UserSecurityUtil.getName())) {
			return ResponseCreator.ok(new AccountImageDeleteDTO(announcementUuid, imageUuid)).build();
		}
		return ResponseCreator.error("image.delete.error", imageUuid).build();
	}
	
	@PutMapping(value = "/announcement/image", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> announcementUpdate(@RequestBody AccountPrimeImageContainer container) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		Set<ImageUuidPrimeDTO> images = accountService.updateImage(container, UserSecurityUtil.getName());
		if (CollectionUtils.isEmpty(images)) {
			return ResponseCreator.error("account.image.save.error").build();
		}
		return ResponseCreator.ok(images).build();
	}
}
