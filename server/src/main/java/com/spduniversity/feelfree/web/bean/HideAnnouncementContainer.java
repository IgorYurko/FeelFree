package com.spduniversity.feelfree.web.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class HideAnnouncementContainer extends UUIDContainer {
	
	@NotNull
	private Boolean hidden;
}
