package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.domain.model.Rent;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class AccountAnnouncementRentedSwitchContainer extends UUIDContainer {
	@Min(0)
	private Long id;
	
	@NotNull
	private Rent.Status status;
}
