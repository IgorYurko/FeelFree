package com.spduniversity.feelfree.web.v1;

import com.spduniversity.feelfree.domain.dto.ConversationDTO;
import com.spduniversity.feelfree.domain.dto.MessageDTO;
import com.spduniversity.feelfree.service.web.MessageService;
import com.spduniversity.feelfree.web.bean.AccountMessageAnnouncementContainer;
import com.spduniversity.feelfree.web.bean.AccountMessageContainer;
import com.spduniversity.feelfree.web.common.ResponseCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Set;

@RestController
@RequestMapping(value = "/api/v1/account/message")
public class AccountMessageController {

   @Autowired
    MessageService messageService;

    @PostMapping(value = "/announcement/new", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> announcementMessageSave(@RequestBody AccountMessageAnnouncementContainer container, Principal principal) {
        ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
        if (target.isNotValid()) {
            return target.badRequest().build();
        }
        if(messageService.saveAnnouncementMessage(container, principal.getName())) {
            return ResponseCreator.ok("message.ok").build();
        }

        return ResponseCreator.ok("message.error").build();
    }
    @PostMapping(value = "/new", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> messageSave(@RequestBody AccountMessageContainer container, Principal principal) {
        ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
        if (target.isNotValid()) {
            return target.badRequest().build();
        }
        MessageDTO message =  messageService.saveMessage(container, principal.getName());
        if(message == null) {
            return ResponseCreator.ok("message.error").build();
        }
        return ResponseCreator.ok(message).build();
    }
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> allMessages(Principal principal) {
        Set<ConversationDTO> conversationMessages = messageService.allMessages(principal.getName());
        if (conversationMessages==null){
           return ResponseCreator.badRequest("conversation.error").build();
        }
        return ResponseCreator.ok(conversationMessages).build();
    }

}
