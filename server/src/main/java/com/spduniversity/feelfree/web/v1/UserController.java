package com.spduniversity.feelfree.web.v1;

import com.spduniversity.feelfree.domain.dto.ImageDTO;
import com.spduniversity.feelfree.domain.dto.UserDTO;
import com.spduniversity.feelfree.service.common.CookieService;
import com.spduniversity.feelfree.service.common.util.UserSecurityUtil;
import com.spduniversity.feelfree.service.domain.UserService;
import com.spduniversity.feelfree.service.web.AccountService;
import com.spduniversity.feelfree.service.web.RegistrationService;
import com.spduniversity.feelfree.service.web.UserValidationService;
import com.spduniversity.feelfree.web.bean.EmailContainer;
import com.spduniversity.feelfree.web.bean.UploadUserImageContainer;
import com.spduniversity.feelfree.web.bean.UserInfoContainer;
import com.spduniversity.feelfree.web.bean.UserPasswordUpdateContainer;
import com.spduniversity.feelfree.web.common.ResponseCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserValidationService userValidationService;

    @Autowired
    private CookieService cookieService;

    @Resource
    private AccountService accountService;

    @Resource
    private RegistrationService registrationService;
    
    @GetMapping(value = "",produces = MediaType.APPLICATION_JSON_VALUE )
    public UserDTO userInfo(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDTO userDTO = userService.getUserData(auth.getName());
        if (userDTO == null) {
            throw new UnapprovedClientAuthenticationException("user not found");
        }
        return userDTO;
    }
    
    @PutMapping(value = "/edit/email", produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<?> emailChange(@RequestBody EmailContainer emailContainer, HttpServletResponse response){
        ResponseCreator.ObjectValidator target = ResponseCreator.validate(emailContainer);
        if (target.isNotValid()) {
            return target.badRequest().build();
        }
        if (!registrationService.changeEmail(UserSecurityUtil.getName(), emailContainer.getEmail())) {
            return ResponseCreator.badRequest("parameters.email.reserved").build();
        }
        return ResponseCreator.ok("parameters.email.ok").build();
    }

    @PutMapping(value = "/edit/password",produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<Map<String, String>> pswChange(@RequestBody UserPasswordUpdateContainer updateContainer){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(auth.getCredentials().toString());
        Map<String, String> errors = userValidationService.getPasswordErrors(updateContainer,auth.getName());
        if (errors.isEmpty()){
            userService.changePassword(updateContainer.getPassword(), auth.getName());
            return ResponseEntity.ok(new HashMap<>());
        }
        return ResponseEntity.badRequest().body(errors);
    }
    
    @PutMapping(value = "edit/info",produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<?> userInfoChange(@RequestBody UserInfoContainer userInfoContainer){
        ResponseCreator.ObjectValidator target = ResponseCreator.validate(userInfoContainer);
        if (target.isNotValid()) {
            return target.badRequest().build();
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        userService.changeInfo(userInfoContainer,auth.getName());
        return ResponseCreator.ok("parameters.info.ok").build();
    }

    @PostMapping(value = "/edit/image", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> imagesSave(UploadUserImageContainer container) {
        ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
        if (target.isNotValid()) {
            return target.badRequest().build();
        }
        try{
            ImageDTO result = userService.saveUserImage(container.getFile().get(0), UserSecurityUtil.getName());
            return ResponseCreator.ok(result).build();
        }
        catch (NullPointerException e){
            return ResponseCreator.error("image.save.error").build();
        }

    }
}
