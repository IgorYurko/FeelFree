package com.spduniversity.feelfree.web.annotation;

import com.spduniversity.feelfree.web.annotation.impl.ValidMultipartFileValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = ValidMultipartFileValidator.class)
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidMultipartFile {
	String message() default "{annotation.multipartFile.default}";
	String[] formats() default {};
	long minSize() default 0L;
	long maxSize() default Long.MAX_VALUE;
	long commonMaxSize() default Long.MAX_VALUE;
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
