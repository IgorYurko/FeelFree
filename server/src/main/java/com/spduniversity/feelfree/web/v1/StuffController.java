package com.spduniversity.feelfree.web.v1;

import com.spduniversity.feelfree.service.common.MailService;
import com.spduniversity.feelfree.service.domain.UserService;
import com.spduniversity.feelfree.service.web.UserValidationService;
import com.spduniversity.feelfree.web.bean.NeedHelpContainer;
import com.spduniversity.feelfree.web.bean.EmailContainer;
import com.spduniversity.feelfree.web.common.ResponseCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/stuff")
public class StuffController {

    @Autowired
    private UserService userService;

    @Autowired
    private MailService mailService;

    @Autowired
    UserValidationService userValidationService;

    @GetMapping(value = "/email",  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> usedEmail(@RequestParam(value = "email")String email){
        String msg;
        if(!userValidationService.isReservedEmail(email)){
            msg = "parameters.email.available";
        }
        else {msg = "parameters.email.reserved";}
        return ResponseCreator.ok(msg).build();
    }

    @PutMapping(value = "/passwordreset", produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<?> pswReset(@RequestBody EmailContainer emailContainer){
        ResponseCreator.ObjectValidator target = ResponseCreator.validate(emailContainer);
        if (target.isNotValid()) {
            return target.badRequest().build();
        }
        if (!userService.resetPassword(emailContainer.getEmail())) {
            return ResponseCreator.badRequest("parameters.email.invalid").build();
        }
        else return ResponseCreator.ok("parameters.password.reset").build();
    }

    @PostMapping(value = "/help",  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> needHelp(@RequestBody NeedHelpContainer container){
        ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
        if (target.isNotValid()) {
            return target.badRequest().build();
        }
        else {
            mailService.sendNeedHelpEmail(container);
            return ResponseCreator.ok("parameters.help.ok").build();
        }
    }
}
