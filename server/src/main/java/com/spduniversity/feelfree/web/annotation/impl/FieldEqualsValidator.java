package com.spduniversity.feelfree.web.annotation.impl;


import com.spduniversity.feelfree.web.annotation.FieldEquals;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FieldEqualsValidator implements ConstraintValidator<FieldEquals, Object> {
	
	private String message;
	private String field;
	private String equalsTo;
	private String exclude;
	
	@Override
	public void initialize(FieldEquals constraintAnnotation) {
		this.message = constraintAnnotation.message();
		this.field = constraintAnnotation.field();
		this.equalsTo = constraintAnnotation.equalsTo();
		this.exclude = constraintAnnotation.exclude();
	}
	
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		if (StringUtils.isBlank(field) && StringUtils.isBlank(equalsTo)) {
			return true;
		}
		try {
			Object fieldResult = FieldUtils.readDeclaredField(value, field, true);
			Object equalsToResult = FieldUtils.readDeclaredField(value, equalsTo, true);
			if (fieldResult == null && equalsToResult == null) {
				return true;
			}
			if (fieldResult != null && fieldResult.equals(equalsToResult)) {
				return true;
			}
			
			context.disableDefaultConstraintViolation();
			ConstraintValidatorContext.ConstraintViolationBuilder constraintViolationBuilder = context.buildConstraintViolationWithTemplate(message);
			if (StringUtils.isNotBlank(exclude)) {
				if (!exclude.equals(field)) {
					constraintViolationBuilder.addPropertyNode(field).addConstraintViolation();
					return false;
				}
				if (!exclude.equals(equalsTo)) {
					constraintViolationBuilder.addPropertyNode(equalsTo).addConstraintViolation();
					return false;
				}
			}
			constraintViolationBuilder.addPropertyNode(field)
									  .addPropertyNode(equalsTo)
									  .addConstraintViolation();
			return false;
		} catch (IllegalAccessException | IllegalArgumentException ex) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("Server error, please back later.")
				   .addPropertyNode(field)
				   .addPropertyNode(equalsTo)
				   .addConstraintViolation();
			return false;
		}
	}
}
