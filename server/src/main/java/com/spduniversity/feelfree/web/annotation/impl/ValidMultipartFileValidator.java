package com.spduniversity.feelfree.web.annotation.impl;

import com.spduniversity.feelfree.web.annotation.ValidMultipartFile;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Collection;

public class ValidMultipartFileValidator implements ConstraintValidator<ValidMultipartFile, Object> {
	
	private long minSize;
	private long maxSize;
	private long commonMaxSize;
	private String message;
	private String[] formats;
	
	@Override
	public void initialize(ValidMultipartFile constraintAnnotation) {
		this.minSize = constraintAnnotation.minSize();
		this.maxSize = constraintAnnotation.maxSize();
		this.formats = Arrays.stream(constraintAnnotation.formats()).map(String::toLowerCase).toArray(String[]::new);
		this.message = constraintAnnotation.message();
		this.commonMaxSize = constraintAnnotation.commonMaxSize();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		if (value instanceof MultipartFile) {
			return isValidMultipartFile((MultipartFile) value, context);
		}
		
		if (value instanceof Collection) {
			boolean isMultipartFiles = ((Collection) value).stream().allMatch(e -> e instanceof MultipartFile);
			if (!isMultipartFiles) {
				buildConstraintViolation(context);
				return false;
			}
			Collection<MultipartFile> instValue = (Collection<MultipartFile>) value;
			if (instValue.stream().mapToLong(MultipartFile::getSize).sum() > commonMaxSize) {
				message = "{annotation.multipartFile.default.commonSize}";
				buildConstraintViolation(context);
			}
			return instValue.stream().allMatch(e -> isValidMultipartFile(e, context));
		}
		return true;
	}
	
	private boolean isValidMultipartFile(MultipartFile value, ConstraintValidatorContext context) {
		if (minSize > value.getSize() || maxSize < value.getSize()) {
			message = "{annotation.multipartFile.default.size}";
			buildConstraintViolation(context);
			return false;
			
		}
		if (ArrayUtils.isNotEmpty(formats) && !ArrayUtils.contains(formats, FilenameUtils.getExtension(value.getOriginalFilename()).toLowerCase())) {
			message = "{annotation.multipartFile.default.formats}";
			buildConstraintViolation(context);
			return false;
			
		}
		return true;
	}
	
	private void buildConstraintViolation(ConstraintValidatorContext context) {
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
	}
}
