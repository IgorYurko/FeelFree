package com.spduniversity.feelfree.web.annotation;


import com.spduniversity.feelfree.web.annotation.impl.FieldEqualsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = FieldEqualsValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldEquals {
	String message() default "{annotation.fieldEquals.default}";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	String field() default "";
	String equalsTo() default "";
	String exclude() default "";
}
