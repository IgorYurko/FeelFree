package com.spduniversity.feelfree.web.v1;

import com.spduniversity.feelfree.domain.dto.AnnouncementAccountFavoriteDTO;
import com.spduniversity.feelfree.service.domain.UserService;
import com.spduniversity.feelfree.web.bean.AccountFavoriteResponseContainer;
import com.spduniversity.feelfree.web.common.ResponseCreator;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Set;
@RestController
@RequestMapping("/api/v1/account")
public class AccountFavoriteController {

    @Resource
    UserService userService;

    @GetMapping(value = "/favorite",produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<?> userFavorite(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Set<AnnouncementAccountFavoriteDTO> favorites = userService.getFavorites(auth.getName());
        return ResponseCreator.ok(favorites).build();
    }

    @GetMapping(value = "/favorite/add/{uuid}",produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<?>userFavoriteAdd(@PathVariable String uuid){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(userService.addFavorite(auth.getName(),uuid)){
            AccountFavoriteResponseContainer container = new AccountFavoriteResponseContainer();
            container.setUuid(uuid);
            container.setFavorite("TRUE");
            return ResponseCreator.ok(container).build();
        }
        return ResponseCreator.badRequest("user.favourite.add.error").build();
    }
    @DeleteMapping(value = "/favorite/remove/{uuid}",produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<?>userFavoriteRemove(@PathVariable String uuid){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(userService.removeFavorite(auth.getName(),uuid)){
            AccountFavoriteResponseContainer container = new AccountFavoriteResponseContainer();
            container.setUuid(uuid);
            container.setFavorite("FALSE");
            return ResponseCreator.ok(container).build();
        }
        return ResponseCreator.badRequest("user.favourite.add.error").build();
    }

}
