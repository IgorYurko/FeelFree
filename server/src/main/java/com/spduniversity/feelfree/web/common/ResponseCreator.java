package com.spduniversity.feelfree.web.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.spduniversity.feelfree.service.common.ValidatorService;
import com.spduniversity.feelfree.service.common.extra.MessageSourceSingleton;
import com.spduniversity.feelfree.service.common.extra.ValidatorSingleton;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;
import java.util.Optional;

public class ResponseCreator {

	public static ResponseBuilder ok(Object body, Object... args) {
		return new ResponseBuilder(HttpStatus.OK, body);
	}

	public static ResponseBuilder ok(String message, Object... args) {
		return new ResponseBuilder(HttpStatus.OK, message, args);
	}
	
	public static ResponseBuilder badRequest(String message, Object... args) {
		return new ResponseBuilder(HttpStatus.BAD_REQUEST, message, args);
	}
	
	public static ResponseBuilder error(String message, Object... args) {
		return new ResponseBuilder(HttpStatus.INTERNAL_SERVER_ERROR, message, args);
	}
	
	public static ResponseBuilder notFound(String message, Object... args) {
		return new ResponseBuilder(HttpStatus.NOT_FOUND, message, args);
	}
	
	public static ResponseBuilder noContent() {
		return new ResponseBuilder(HttpStatus.NO_CONTENT, "", "");
	}
	
	public static <T> ObjectValidator validate(T target) {
		return new ObjectValidator<>(target);
	}
	
	public static class ResponseBuilder {
		private HttpStatus httpStatus;
		private Object body;
		private MessageSource messageSource = MessageSourceSingleton.get();
		
		private ResponseBuilder(HttpStatus httpStatus) {
			this.httpStatus = httpStatus;
		}
		
		private ResponseBuilder(HttpStatus httpStatus, Object body) {
			this.httpStatus = httpStatus;
			this.body = body;
		}
		
		private ResponseBuilder(HttpStatus httpStatus, Map<String, String> body) {
			this.httpStatus = httpStatus;
			this.body = new ResponseMessenger(body);
		}
		
		private ResponseBuilder(HttpStatus httpStatus, String body, Object... args) {
			this.httpStatus = httpStatus;
			if (StringUtils.isNotBlank(body)) {
				this.body = new ResponseMessenger(messageSource.getMessage(body, args, LocaleContextHolder.getLocale()));
			}
		}
		
		public ResponseBuilder body(Object body) {
			this.body = body;
			return this;
		}
		
		public ResponseEntity build() {
			return Optional.ofNullable(body)
						   .map(e -> ResponseEntity.status(httpStatus).body(body))
						   .orElse(ResponseEntity.status(httpStatus).build());
		}
	}
	
	public static class ObjectValidator<T> {
		private final ValidatorService validatorService = ValidatorSingleton.get();
		private final T target;
		
		private ObjectValidator(T target) {
			this.target = target;
		}
		
		public boolean isValid() {
			return validatorService.isValid(target);
		}
		
		public boolean isNotValid() {
			return !validatorService.isValid(target);
		}

		public Map<String, String> getErrors() {
			return validatorService.getErrors(target);
		}
		
		public ResponseBuilder ok(String message, Object... args) {
			return new ResponseBuilder(HttpStatus.OK, message, args);
		}

		public ResponseBuilder ok() {
			return new ResponseBuilder(HttpStatus.OK);
		}
		
		public ResponseBuilder error(String message, Object... args) {
			return new ResponseBuilder(HttpStatus.INTERNAL_SERVER_ERROR, message, args);
		}
		
		public ResponseBuilder error() {
			return new ResponseBuilder(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		public ResponseBuilder notFound(String message, Object... args) {
			return new ResponseBuilder(HttpStatus.NOT_FOUND, message, args);
		}
		
		public ResponseBuilder notFound() {
			return new ResponseBuilder(HttpStatus.NOT_FOUND);
		}
		
		public ResponseBuilder badRequest(Map<String, String> putErrors) {
			Map<String, String> errors = validatorService.getErrors(target);
			errors.putAll(putErrors);
			return new ResponseBuilder(HttpStatus.BAD_REQUEST, errors);
		}
		
		public ResponseBuilder badRequest() {
			return new ResponseBuilder(HttpStatus.BAD_REQUEST, validatorService.getErrors(target));
		}
	}
	
	@Getter
	@Builder
	@AllArgsConstructor
	@NoArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public static class ResponseMessenger {
		private String message;
		private Map<String, String> errors;
		
		private ResponseMessenger(String message) {
			this.message = message;
		}
		
		private ResponseMessenger(Map<String, String> errors) {
			this.errors = errors;
		}
	}
}
