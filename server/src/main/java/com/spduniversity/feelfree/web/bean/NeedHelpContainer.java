package com.spduniversity.feelfree.web.bean;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.spduniversity.feelfree.service.common.extra.VariablesContainer.EMAIL_PATTERN;

@Data
public class NeedHelpContainer {
    
    @NotBlank
    @Length(max = 320)
    @Pattern(regexp = EMAIL_PATTERN, message = "{annotation.parameters.email.invalid}")
    private String email;

    @Size(max = 100)
    private String title;

    @Size(max = 600)
    private String messageBody;
}
