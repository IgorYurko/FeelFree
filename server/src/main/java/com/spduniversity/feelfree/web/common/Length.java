package com.spduniversity.feelfree.web.common;

public enum Length {
	MILE(3959),
	KM(6371);
	
	private final int value;
	
	Length(int value) {
		this.value = value;
	}
	
	public int get() {
		return value;
	}
}
