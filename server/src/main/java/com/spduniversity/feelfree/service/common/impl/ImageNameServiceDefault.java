package com.spduniversity.feelfree.service.common.impl;

import com.spduniversity.feelfree.domain.dto.CommentDTO;
import com.spduniversity.feelfree.domain.dto.ImageDTO;
import com.spduniversity.feelfree.domain.dto.ImageUUIDDTO;
import com.spduniversity.feelfree.service.common.ImageNameService;
import com.spduniversity.feelfree.service.common.extra.VariablesContainer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;

@Service
public class ImageNameServiceDefault implements ImageNameService {


	@Resource
	private VariablesContainer variablesContainer;
	
	@Override
	public void setFullNameIntoImage(Collection<ImageDTO> images) {
		images.forEach(this::setFullNameIntoImage);
	}
	
	@Override
	public void setFullNameIntoImage(ImageDTO image) {
		if (image != null && StringUtils.isNotBlank(image.getValue())) {
			image.setName(String.format("%s/%s%s", variablesContainer.getServerUrl(), variablesContainer.getImageLoadUrl(), image.getValue()));
		}
	}
	
	@Override
	public void setNameUuidIntoImage(ImageUUIDDTO image) {
		if (image != null) {
			String value = image.getValue();
			if (StringUtils.isNotBlank(value)) {
				image.setName(String.format("%s/%s%s", variablesContainer.getServerUrl(), variablesContainer.getImageLoadUrl(), value));
				image.setUuid(value);
			}
		}
	}
	
	@Override
	public void setNameUuidIntoImage(Collection<ImageUUIDDTO> images) {
		images.forEach(this::setNameUuidIntoImage);
	}
	
	@Override
	public String getFullImageName(String imageName) {
		if (StringUtils.isNotBlank(imageName)) {
			return String.format("%s/%s%s", variablesContainer.getServerUrl(), variablesContainer.getImageLoadUrl(), imageName);
		}
		return null;
	}
	
	@Override
	public void setFullNameIntoMessage(CommentDTO image) {
		setFullNameIntoImage(image.getUser().getImage());
	}

	
	@Override
	public void setFullNameIntoMessage(Collection<CommentDTO> messages) {
		messages.forEach(e -> setFullNameIntoImage(e.getUser().getImage()));
	}
}
