package com.spduniversity.feelfree.service.common.bean;

public interface UserPasswordContainer<T> {
	void setUser(T user);
	void setPassword(String password);
	T getUser();
	String getPassword();
	
}
