package com.spduniversity.feelfree.service.web.impl;

import com.spduniversity.feelfree.domain.mapper.TemporaryEmailMapperHelper;
import com.spduniversity.feelfree.domain.mapper.TemporaryUserMapperHelper;
import com.spduniversity.feelfree.domain.mapper.UserMapperHelper;
import com.spduniversity.feelfree.domain.model.Role;
import com.spduniversity.feelfree.domain.model.TemporaryEmail;
import com.spduniversity.feelfree.domain.model.TemporaryUser;
import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.domain.repository.RoleRepository;
import com.spduniversity.feelfree.domain.repository.UserRepository;
import com.spduniversity.feelfree.service.common.*;
import com.spduniversity.feelfree.service.common.bean.UserPasswordContainer;
import com.spduniversity.feelfree.service.common.extra.VariablesContainer;
import com.spduniversity.feelfree.service.common.impl.CookieServiceDefault.Encoder;
import com.spduniversity.feelfree.service.domain.TemporaryEmailService;
import com.spduniversity.feelfree.service.domain.TemporaryUserService;
import com.spduniversity.feelfree.service.domain.UserService;
import com.spduniversity.feelfree.service.web.RegistrationService;
import com.spduniversity.feelfree.service.web.UserValidationService;
import com.spduniversity.feelfree.service.web.bean.UserContainer;
import com.spduniversity.feelfree.web.bean.UserRegisterContainer;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.WebRequest;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.spduniversity.feelfree.service.common.extra.VariablesContainer.CLIENT_ID_FEEL_FREE;

@Slf4j
@Service
public class RegistrationServiceDefault implements RegistrationService {
    
    @Resource
    private ValidatorService validator;

    @Resource
    private MessageSource messageSource;
    
    @Autowired
    public UserRepository repository;

    @Autowired
    private MailService mailService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserValidationService userValidationService;

    @Autowired
    private UserService userService;

    @Autowired
    private TemporaryUserService temporaryUserService;
    
    @Resource
    private SocialAuthorizeService socialAuthorizeService;
    
    @Resource
    private TemporaryEmailService temporaryEmailService;
    
    @Resource
    private ProviderSignInUtils signInUtils;
    
    @Resource
    private CookieService cookieService;
    
    @Resource
    private SocialUserCreatorService socialConnectionService;
    
    @Resource
    private TemporaryUserMapperHelper temporaryUserMapper;
    
    @Resource
    private TemporaryEmailMapperHelper temporaryEmailMapper;
    
    @Resource
    private UserMapperHelper userMapper;
    
    @Resource
    private VariablesContainer variables;
    
    @Override
    public Map<String, String> getErrors(UserRegisterContainer userRegisterContainer) {
        Map<String, String> result = new HashMap<>();
        if (!validator.isValid(userRegisterContainer)) {
            result.putAll(validator.getErrors(userRegisterContainer));
        }
        if(userValidationService.isReservedEmail(userRegisterContainer.getProfileEmail())){
            result.put("profileEmail", messageSource.getMessage("parameters.email.reserved", null, LocaleContextHolder.getLocale()));
        }
        return result;
    }

    @Override
    public <T extends UserContainer> void registerTemporaryUser(T userRegisterContainer){
        if (userRegisterContainer != null) {
            TemporaryUser temporaryUser = temporaryUserMapper.toTemporaryUser().apply(userRegisterContainer);
            String token = temporaryUserService.saveTemporaryUser(temporaryUser);
            if (StringUtils.isNotBlank(token)) {
                mailService.sendActivationEmail(userRegisterContainer, token);
            }
        }
    }
    
    private void registerNewUser(@Nonnull TemporaryUser temporaryUser) {
        User user = temporaryUserMapper.toUser().apply(temporaryUser);
        Role role = roleRepository.findByValue("ROLE_USER");
        user.getRoles().add(role);
        userService.save(user);
    }
    
    private void registerExistUser(@Nonnull TemporaryUser temporaryUser) {
        User user = userService.getUserByEmail(temporaryUser.getProfileEmail());
        temporaryUserMapper.margeUser(user).apply(temporaryUser);
        userService.save(user);
    }
    
    @Override
    public String reviewToken(String token){
        if (!token.isEmpty()) {
            TemporaryUser temporaryUser = temporaryUserService.findByToken(token);
            if (temporaryUser != null) {
                if (temporaryUser.getExpiryDate().isAfter(LocalDateTime.now())) {
                    registerNewUser(temporaryUser);
                    String email = temporaryUser.getProfileEmail();
                    temporaryUserService.delete(temporaryUser);
                    return email;
                } else {
                    temporaryUserService.delete(temporaryUser);
                    return null;
                }
            }
        }
        return null;
    }
    
    @Override
    public String reviewSocialToken(String token) {
        if (!token.isEmpty()) {
            TemporaryUser temporaryUser = temporaryUserService.findByToken(token);
            if (temporaryUser != null) {
                if (temporaryUser.getExpiryDate().isAfter(LocalDateTime.now())) {
                    String email = temporaryUser.getProfileEmail();
                    if (userService.isReservedEmail(email)) {
                        registerExistUser(temporaryUser);
                    } else {
                        registerNewUser(temporaryUser);
                    }
                    temporaryUserService.delete(temporaryUser);
                    return email;
                } else {
                    temporaryUserService.delete(temporaryUser);
                    return null;
                }
            }
        }
        return null;
    }
    
    @Override
    public String registerEmail(String token) {
        if (!token.isEmpty()) {
            TemporaryEmail temporaryEmail = temporaryEmailService.findByToken(token);
            if (temporaryEmail != null) {
                if (temporaryEmail.getExpiryDate().isAfter(LocalDateTime.now())) {
                    userService.updateProfileEmail(temporaryEmail.getUser().getId(), temporaryEmail.getValue());
                    temporaryEmailService.delete(temporaryEmail);
                    return temporaryEmail.getValue();
                } else {
                    temporaryEmailService.delete(temporaryEmail);
                    return null;
                }
            }
        }
        return null;
    }
    
    @Override
    public boolean registerSocialTemporaryUser(WebRequest request, String email) {
        Connection<?> connection = signInUtils.getConnectionFromSession(request);
        if (connection != null) {
            UserPasswordContainer<TemporaryUser> userPassword = socialConnectionService.createTemporaryUser(connection);
            TemporaryUser temporaryUser = userPassword.getUser();
            temporaryUser.setProfileEmail(email);
            if (temporaryUserService.save(temporaryUser)) {
                UserContainer container = temporaryUserMapper.toUserContainer().apply(temporaryUser);
                if (userService.isReservedEmail(container.getProfileEmail())) {
                    mailService.sendSocialMargeEmail(container, temporaryUser.getToken());
                } else {
                    mailService.sendActivationEmail(container, temporaryUser.getToken(), userPassword.getPassword());
                }
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean registerSocialUser(WebRequest request) {
        Connection<?> connection = signInUtils.getConnectionFromSession(request);
        if (connection != null) {
            String email = connection.fetchUserProfile().getEmail();
            if (!userService.isReservedEmail(email)) {
                UserPasswordContainer<User> userPassword = socialConnectionService.createUser(connection);
                User user = userPassword.getUser();
                userService.save(user);
                UserContainer container = userMapper.toUserContainer().apply(user);
                mailService.sendSocialPasswordEmail(container, userPassword.getPassword());
                return true;
            }
            User user = userService.getUserByEmail(email);
            socialConnectionService.editUser(connection, user);
            userService.save(user);
            return true;
        }
        return false;
    }
    
    @Override
    public Boolean changeEmail(String oldEmail, String newEmail){
        if (StringUtils.isNotBlank(oldEmail) && StringUtils.isNotBlank(newEmail)) {
            if (oldEmail.trim().equals(newEmail.trim())) {
                return true;
            }
            if(!userService.isReservedEmail(newEmail)){
                TemporaryEmail temporaryEmail = Optional.ofNullable(userService.findByUniqueValue(oldEmail))
                                                        .map(user -> temporaryEmailMapper.toTemporaryEmail(user).apply(newEmail))
                                                        .orElse(null);
                if (temporaryEmail != null && temporaryEmailService.save(temporaryEmail)) {
                    UserContainer container = temporaryEmailMapper.toUserContainer().apply(temporaryEmail);
                    mailService.sendEmailResetEmail(container, temporaryEmail.getToken());
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public boolean makeAuthorizeRedirectResponse(HttpServletResponse response, String uniqueValue, String clientId, String redirectUrl) {
        OAuth2AccessToken token = socialAuthorizeService.authorizeUser(uniqueValue, clientId);
        if (token != null) {
            cookieService.add(response, variables.getTokenCookieName(), token.getValue(), variables.getTokenCookieAge(), Encoder.NON);
            try {
                response.sendRedirect(redirectUrl);
                return true;
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return false;
    }
    
    @Override
    public boolean makeAuthorizeRedirectResponse(HttpServletResponse response, String uniqueValue, String redirectUrl) {
        return makeAuthorizeRedirectResponse(response, uniqueValue, CLIENT_ID_FEEL_FREE, redirectUrl);
    }
    
    @Override
    @SneakyThrows
    public void makeRedirectResponse(HttpServletResponse response, String redirectUrl) {
        response.sendRedirect(redirectUrl);
    }
}
