package com.spduniversity.feelfree.service.common.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spduniversity.feelfree.service.common.PageableService;
import com.spduniversity.feelfree.service.common.bean.PageableContainer;
import com.spduniversity.feelfree.service.common.bean.PageableDefaultContainer;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.EnumUtils.getEnum;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBefore;

@Service
public class PageableServiceDefault implements PageableService {
	
	@Resource
	private ObjectMapper objectMapper;
	
	private static final String DEFAULT_DELIMITER_ORDER = ",";
	private static final Sort.Direction DEFAULT_DIRECTION_SORT = Sort.Direction.ASC;
	
	@Override
	public <T extends PageableDefaultContainer> PageRequest createPageable(T container) {
		if (CollectionUtils.isEmpty(container.getSort()) && CollectionUtils.isEmpty(container.getDefaultSort())) {
			return new PageRequest(defaultIfNull(container.getPage(), container.getDefaultPage()),
								   defaultIfNull(container.getSize(), container.getDefaultSize()));
		}
		List<Sort.Order> orders = defaultIfNull(container.getSort(), container.getDefaultSort())
				.stream()
				.collect(Collectors.toMap(
						k -> substringBefore(k, DEFAULT_DELIMITER_ORDER),
						v -> substringAfter(v, DEFAULT_DELIMITER_ORDER)))
				.entrySet()
				.stream()
				.map(this::createOrder)
				.collect(Collectors.toList());
		return new PageRequest(defaultIfNull(container.getPage(), container.getDefaultPage()),
							   defaultIfNull(container.getSize(), container.getDefaultSize()),
							   new Sort(orders));
	}
	
	@Override
	public <T extends PageableContainer> PageRequest createPageable(T source, Integer defaultPage, Integer defaultSize, List<String> defaultSort) {
		PageableDefaultContainer pageableContainer = objectMapper.convertValue(source, PageableDefaultContainer.class);
		pageableContainer.setDefaultPage(defaultPage);
		pageableContainer.setDefaultSize(defaultSize);
		pageableContainer.setDefaultSort(defaultSort);
		return createPageable(pageableContainer);
	}
	
	private Sort.Order createOrder(Map.Entry<String, String> entry) {
		Sort.Direction direction = getEnum(Sort.Direction.class, entry.getValue().toUpperCase(Locale.ENGLISH));
		if (direction == null) {
			return new Sort.Order(DEFAULT_DIRECTION_SORT, entry.getKey());
		}
		return new Sort.Order(direction, entry.getKey());
	}
}
