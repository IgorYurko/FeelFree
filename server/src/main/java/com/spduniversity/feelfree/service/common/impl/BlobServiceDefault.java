package com.spduniversity.feelfree.service.common.impl;

import com.spduniversity.feelfree.service.common.BlobService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceUnit;
import java.io.InputStream;
import java.sql.Blob;

@Slf4j
@Service
public class BlobServiceDefault implements BlobService {
	
	@PersistenceUnit(unitName = "sessionFactory")
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession(){
		try {
			return sessionFactory.getCurrentSession();
		} catch (HibernateException ex) {
			log.debug(ex.getMessage());
			return sessionFactory.openSession();
		}
	}
	
	@Override
	public Blob createBlob(final byte[] bytes) {
		return Hibernate.getLobCreator(getCurrentSession()).createBlob(bytes);
	}
	
	@Override
	public Blob createBlob(InputStream stream, long length) {
		return Hibernate.getLobCreator(getCurrentSession()).createBlob(stream, length);
	}
}
