package com.spduniversity.feelfree.service.web;

import com.spduniversity.feelfree.domain.dto.*;
import com.spduniversity.feelfree.web.bean.AccountPageableCommentContainer;
import com.spduniversity.feelfree.web.bean.SearchAnnouncementsContainer;
import com.spduniversity.feelfree.web.bean.SearchGeolocationContainer;
import org.springframework.data.domain.Page;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface SearchService {
	
	@Nonnull
	Page<GeolocationSearchDTO> getGeolocations(@Nullable SearchGeolocationContainer searchGeolocationContainer);
	
	@Nullable
	AnnouncementSearchSingleDTO getAnnouncement(@Nullable String uuid);
	
	@Nonnull
	Page<AnnouncementSearchDTO> getAnnouncements(@Nullable SearchAnnouncementsContainer searchAnnouncementsContainer);
	
	MinMaxPriceDTO getPriceRange();
	
	@SuppressWarnings("unchecked")
	@Nonnull
	Page<CommentDTO> getComments(AccountPageableCommentContainer container, String uuid);
}
