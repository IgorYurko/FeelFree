package com.spduniversity.feelfree.service.common;

import com.spduniversity.feelfree.service.common.bean.PageableContainer;
import com.spduniversity.feelfree.service.common.bean.PageableDefaultContainer;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface PageableService {
	<T extends PageableDefaultContainer> PageRequest createPageable(T container);
	<T extends PageableContainer> PageRequest createPageable(T source, Integer defaultPage, Integer defaultSize, List<String> defaultSort);
}
