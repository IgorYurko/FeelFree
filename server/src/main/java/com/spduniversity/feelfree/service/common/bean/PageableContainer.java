package com.spduniversity.feelfree.service.common.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageableContainer {
	private Integer page;
	private Integer size;
	private List<String> sort;
}
