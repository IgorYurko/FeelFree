package com.spduniversity.feelfree.service.domain;

import com.spduniversity.feelfree.domain.model.TemporaryEmail;

import javax.annotation.Nullable;

public interface TemporaryEmailService {
	boolean save(@Nullable TemporaryEmail temporaryEmail);
	
	@Nullable
	TemporaryEmail findByToken(@Nullable String token);
	
	void delete(TemporaryEmail temporaryEmail);
}
