package com.spduniversity.feelfree.service.domain.impl;

import com.spduniversity.feelfree.domain.model.Comment;
import com.spduniversity.feelfree.domain.repository.CommentRepository;
import com.spduniversity.feelfree.service.domain.CommentService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.function.Function;

@Service
public class CommentServiceDefault implements CommentService {
	
	@Resource
	private CommentRepository repository;
	
	@Nonnull
	@Override
	public Comment save(@Nonnull Comment comment) {
		return repository.save(comment);
	}
	
	@SafeVarargs
	@Nonnull
	@Override
	public final <T> Page<T> getCommentData(@Nonnull Function<Comment, T> mapperFunction,
											@Nonnull Pageable pageable,
											@Nullable Specification<Comment>... specifications) {
		return repository.resultBuilder(mapperFunction)
						 .pageable(pageable)
						 .whereAnd(specifications)
						 .pageResult();
	}
}
