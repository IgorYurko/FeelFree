package com.spduniversity.feelfree.service.web.bean;

import static org.apache.commons.lang3.StringUtils.*;

public interface UserContainer {
	String getFirstName();
	String getLastName();
	String getProfileEmail();
	String getPassword();
	
	default String getFullName() {
		return (defaultIfBlank(getFirstName(), EMPTY) + SPACE + defaultIfBlank(getLastName(), EMPTY)).trim();
	}
	
	default String getProfileEmailIfBlankFullName() {
		return isBlank(getFullName()) ? getProfileEmail() : getFullName();
	}
}
