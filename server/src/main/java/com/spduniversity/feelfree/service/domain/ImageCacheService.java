package com.spduniversity.feelfree.service.domain;

import com.spduniversity.feelfree.domain.dto.LoadImageDTO;
import com.spduniversity.feelfree.domain.model.ImageCache;

import javax.annotation.Nullable;

public interface ImageCacheService {
	boolean isExistCacheImageData(@Nullable String name, int width, int height);
	
	@Nullable
	LoadImageDTO getImageDataToDTO(@Nullable String name, int width, int height);
	
	@Nullable
	ImageCache getImageData(@Nullable String name, int width, int height);
	
	@Nullable
	ImageCache saveImage(@Nullable ImageCache imageCache);
	
	void delete(@Nullable String name, int width, int height);
}
