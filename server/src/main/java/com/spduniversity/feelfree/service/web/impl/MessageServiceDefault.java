package com.spduniversity.feelfree.service.web.impl;

import com.spduniversity.feelfree.domain.dto.ConversationDTO;
import com.spduniversity.feelfree.domain.dto.EmailNotificationDTO;
import com.spduniversity.feelfree.domain.dto.MessageDTO;
import com.spduniversity.feelfree.domain.mapper.ConversationMapperHelper;
import com.spduniversity.feelfree.domain.model.*;
import com.spduniversity.feelfree.domain.repository.UserRepository;
import com.spduniversity.feelfree.service.common.MailService;
import com.spduniversity.feelfree.service.domain.AnnouncementService;
import com.spduniversity.feelfree.service.domain.ConversationService;
import com.spduniversity.feelfree.service.domain.UserService;
import com.spduniversity.feelfree.service.web.MessageService;
import com.spduniversity.feelfree.web.bean.AccountMessageAnnouncementContainer;
import com.spduniversity.feelfree.web.bean.AccountMessageContainer;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;

@Service
public class MessageServiceDefault implements MessageService {

    @Resource
    private UserService userService;

    @Resource
    private UserRepository userRepository;

    @Resource
    private ConversationService conversationService;

    @Resource
    private AnnouncementService announcementService;

    @Resource
    private MessageSource messageSource;

    @Resource
    private MailService mailService;

    @Resource
    ConversationMapperHelper conversationMapperHelper;



    @Override
    public boolean saveAnnouncementMessage(AccountMessageAnnouncementContainer container, String userEmail){
        User userFrom = userRepository.findByUniqueValue(userEmail);
        Announcement announcement = announcementService.getAnnouncement(container.getAnnouncementUuid());
        User userTo = announcement.getUser();

        if (userTo == null || userFrom == null){return false;}

        Conversation conversation = conversationService.getConversation(userFrom,userTo);
        Message message = new Message();
        message.setConversation(conversation);
        message.setTitle(findTitle(container.getTitle(),conversation));
        message.setUser(userFrom);
        message.setText(container.getText());
        message.setCreateDate(LocalDateTime.now());
        conversationService.saveMessage(message);
        conversationService.saveConversation(conversation);
        mailService.sendMessageNotification(createMessageNotification(userTo, userFrom, message));

        return true;
    }

    @Nullable
    @Override
    public MessageDTO saveMessage(AccountMessageContainer container, String email){
        Conversation conversation = conversationService.getConversationById(container.getConversationId());
        User userFrom = userRepository.findByUniqueValue(email);

        if (conversation == null || userFrom == null) {return null;}

        Message message = new Message();
        message.setCreateDate(LocalDateTime.now());
        message.setUser(userFrom);
        message.setConversation(conversation);
        message.setText(container.getText());
        message.setTitle(conversationService.getTitleFromLastMessage(conversation));
        conversationService.saveMessage(message);
        conversationService.saveConversation(conversation);
        User userTo = getUserToByConversation(conversation,userFrom);
        mailService.sendMessageNotification(createMessageNotification(userTo, userFrom, message));
        return conversationMapperHelper.convertToMessageDTO(message);
    }


    @Override
    public Set<ConversationDTO> allMessages(String userEmail){
        User user = userRepository.findByUniqueValue(userEmail);
        if (user==null){return null;}
        return conversationService.getUserConversations(user);

    }

    @Override
    public void saveMessageAtRent(User userFrom, Announcement announcement, Rent rent){
        Conversation conversation = conversationService.getConversation(userFrom,announcement.getUser());
        Message message = new Message();
        message.setConversation(conversation);
        message.setTitle(findTitle(announcement.getTitle(),conversation));
        message.setUser(userFrom);
        message.setText(messageSource.getMessage("message.rent" ,null, LocaleContextHolder.getLocale()));
        message.setCreateDate(LocalDateTime.now());
        conversationService.saveMessage(message);
        User userTo = announcement.getUser();
        EmailNotificationDTO emailNotificationDTO = createMessageNotification(userTo, userFrom, message);
        emailNotificationDTO.setRent(rent);
        conversationService.saveConversation(conversation);
        mailService.sendEmailRequestBook(emailNotificationDTO);
    }
	
	@Override
	public void updateMessageAtRent(Rent rent, String profileEmail) {
        String responseMessage;
        User userFrom = userRepository.findByUniqueValue(profileEmail);
        User userTo = rent.getUser();
        Conversation conversation = conversationService.getConversation(userFrom,userTo);
        Message message = new Message();
        message.setTitle(conversationService.getTitleFromLastMessage(conversation));
        message.setUser(userFrom);
        if (rent.getStatus().equals(Rent.Status.CONFIRMED))
        {responseMessage = "message.rent.accept";}
        else responseMessage = "message.rent.reject";
        String dates = rent.getCheckInDate().format(dateFormat()) +" - " + rent.getCheckOutDate().format(dateFormat());
        message.setText(messageSource.getMessage(responseMessage ,new Object[]{dates}, LocaleContextHolder.getLocale()));
        message.setCreateDate(LocalDateTime.now());
        message.setConversation(conversation);
        conversationService.saveMessage(message);
        conversationService.saveConversation(conversation);
        EmailNotificationDTO messageNotification = createMessageNotification(userTo, userFrom, message);
        messageNotification.setRent(rent);
        mailService.sendStatusOfRequestBook(messageNotification);


	
	}
    private DateTimeFormatter dateFormat() {
        return DateTimeFormatter.ofPattern("dd-MMM-yyyy");
    }
	
	private MessageTitle findTitle(String newTitle, Conversation conversation) {
        MessageTitle lastMsgTittle = null;
        try {
            lastMsgTittle = conversationService.getTitleFromLastMessage(conversation);
        }catch (NullPointerException e){}
        if(lastMsgTittle == null || !lastMsgTittle.getText().equals(newTitle)) {
            MessageTitle newMsgTitle = new MessageTitle();
            newMsgTitle.setText(newTitle);
            return newMsgTitle;
        }
        else return lastMsgTittle;

    }

    public EmailNotificationDTO createMessageNotification(User userTo, User userFrom, Message message){
        EmailNotificationDTO messageNotificationDTO = new EmailNotificationDTO();
        messageNotificationDTO.setText(message.getText());
        messageNotificationDTO.setTitle(message.getTitle().getText());
        messageNotificationDTO.setUserFrom(userFrom);
        messageNotificationDTO.setUserTo(userTo);
        return messageNotificationDTO;

    }
    private User getUserToByConversation(Conversation conversation, User userFrom){
        Set<User> conversationUsers = conversation.getConversationUsers();
        User userTo = null;
        for (User user : conversationUsers) {
            if (!user.equals(userFrom))
            {userTo = user;}
        }
        return userTo;
    }

}
