package com.spduniversity.feelfree.service.web.bean;

import com.spduniversity.feelfree.domain.model.Price;
import com.spduniversity.feelfree.domain.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaveAnnouncementContainer {
	private String country;
	private String city;
	private String region;
	private String street;
	private Integer appNumber;
	private Price.Period period;
	private Double price;
	private Integer roomCount;
	private Integer livingPlacesCount;
	private String title;
	private Double latitude;
	private Double longitude;
	private String shortDescription;
	private boolean wiFi;
	private boolean washingMachine;
	private boolean refrigerator;
	private boolean hairDryer;
	private boolean iron;
	private boolean smoking;
	private boolean animals;
	private boolean kitchenStuff;
	private boolean conditioner;
	private boolean balcony;
	private boolean tv;
	private boolean essentials;
	private boolean shampoo;
	private User user;
}
