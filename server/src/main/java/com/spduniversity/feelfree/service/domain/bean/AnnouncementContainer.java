package com.spduniversity.feelfree.service.domain.bean;

import com.spduniversity.feelfree.domain.model.Price;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnnouncementContainer {
	private String country;
	private String city;
	private String region;
	private String placeId;
	private Integer minPrice;
	private Integer maxPrice;
	private Integer rooms;
	private Integer livingPlaces;
	private Price.Period period;
	private Boolean nonNullImage;
	private Boolean wiFi;
	private Boolean washingMachine;
	private Boolean refrigerator;
	private Boolean hairDryer;
	private Boolean iron;
	private Boolean smoking;
	private Boolean animals;
	private Boolean kitchenStuff;
	private Boolean conditioner;
	private Boolean balcony;
	private Boolean tv;
	private Boolean essentials;
	private Boolean shampoo;
}
