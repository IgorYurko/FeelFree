package com.spduniversity.feelfree.service.web;

import com.spduniversity.feelfree.web.bean.UserPasswordUpdateContainer;

import java.util.Map;

public interface UserValidationService {

    Map<String, String> getPasswordErrors(UserPasswordUpdateContainer container, String userEmail);

    Boolean isReservedEmail(String email);
}
