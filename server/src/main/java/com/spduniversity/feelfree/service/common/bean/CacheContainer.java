package com.spduniversity.feelfree.service.common.bean;

import lombok.Data;

import java.sql.Blob;

@Data
public class CacheContainer {
	private String name;
	private String type;
	private Blob value;
	private Integer width;
	private Integer height;
}
