package com.spduniversity.feelfree.service.web;


import com.spduniversity.feelfree.domain.dto.*;
import com.spduniversity.feelfree.web.bean.*;
import org.springframework.data.domain.Page;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;

public interface AccountService {
	@Nullable
	UUIDDTO saveAnnouncement(@Nullable SaveAnnouncementContainer container, @Nullable String profileEmail);
	<T extends UUIDContainer> boolean updateAnnouncement(@Nullable T container, @Nullable String profileEmail);
	@Nullable
	<T extends UUIDContainer> CommentDTO saveComment(@Nullable T container, @Nullable String profileEmail);
	
	Set<ImageUuidPrimeDTO> updateImage(@Nullable AccountPrimeImageContainer container, String profileEmail);
	
	@Nonnull
	List<ImageDTO> saveImage(@Nullable UploadImageContainer container, @Nullable String profileEmail);
	@Nullable
	AnnouncementAccountDTO getAnnouncement(@Nullable String uuid, String profileEmail);
	boolean deleteAnnouncement(@Nullable String uuid, @Nullable String profileEmail);
	boolean deleteImage(@Nullable String announcementUuid, @Nullable String imageUuid, String profileEmail);
	@Nonnull
	Page<AnnouncementAccountPreviewDTO> getAnnouncements(@Nullable AccountAnnouncementContainer container, @Nullable String profileEmail);
	
	@Nullable
	List<RentedDTO> getRents(@Nullable String uuid, @Nullable String profileEmail);
	boolean saveRent(@Nullable AccountRentContainer container, @Nullable String profileEmail);
	boolean updateRent(@Nullable AccountAnnouncementRentedSwitchContainer container, @Nullable String profileEmail);
	
	List<RentedDatesDTO> getRentedDates(AccountAnnouncementRentedDateContainer container, String uuid, String profileEmail);
}
