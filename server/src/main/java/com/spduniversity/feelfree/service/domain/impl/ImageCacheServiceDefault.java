package com.spduniversity.feelfree.service.domain.impl;

import com.spduniversity.feelfree.domain.dto.LoadImageDTO;
import com.spduniversity.feelfree.domain.model.ImageCache;
import com.spduniversity.feelfree.domain.repository.ImageCacheRepository;
import com.spduniversity.feelfree.service.domain.ImageCacheService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.annotation.Resource;

@Service
public class ImageCacheServiceDefault implements ImageCacheService {
	
	@Resource
	private ImageCacheRepository imageCacheRepository;
	
	@Override
	public boolean isExistCacheImageData(@Nullable String name, int width, int height) {
		if (StringUtils.isBlank(name)) {
			return false;
		}
		return imageCacheRepository.isExistImage(name.trim(), width, height);
	}
	
	@Override
	@Nullable
	public ImageCache getImageData(@Nullable String name, int width, int height) {
		if (StringUtils.isBlank(name)) {
			return null;
		}
		return imageCacheRepository.findTopByName(name.trim(), width, height);
	}
	
	@Override
	@Nullable
	public LoadImageDTO getImageDataToDTO(@Nullable String name, int width, int height) {
		if (StringUtils.isBlank(name)) {
			return null;
		}
		return imageCacheRepository.getCacheImage(name.trim(), width, height);
	}
	
	@Override
	@Nullable
	public ImageCache saveImage(@Nullable ImageCache imageCache) {
		if (imageCache == null) {
			return null;
		}
		return imageCacheRepository.save(imageCache);
	}
	
	@Override
	public void delete(@Nullable String name, int width, int height) {
		if (StringUtils.isNotBlank(name)) {
			imageCacheRepository.delete(name.trim(), width, height);
		}
	}
}
