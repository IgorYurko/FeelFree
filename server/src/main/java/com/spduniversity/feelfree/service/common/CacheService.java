package com.spduniversity.feelfree.service.common;

import com.spduniversity.feelfree.service.common.bean.CacheContainer;

import javax.annotation.Nullable;
import java.util.concurrent.Future;

public interface CacheService {
	Future<Boolean> createCache(@Nullable CacheContainer container);
	void deleteCache(String name, int with, int height);
}
