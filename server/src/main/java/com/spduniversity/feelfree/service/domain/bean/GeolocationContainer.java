package com.spduniversity.feelfree.service.domain.bean;

import com.spduniversity.feelfree.web.common.Length;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeolocationContainer {
	private Double latitude;
	private Double longitude;
	private Integer radius;
	private Length length;
}
