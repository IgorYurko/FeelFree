package com.spduniversity.feelfree.service.web;

import com.spduniversity.feelfree.service.web.bean.UserContainer;
import com.spduniversity.feelfree.web.bean.UserRegisterContainer;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface RegistrationService {
    Map<String, String> getErrors(UserRegisterContainer userRegisterContainer);
    
    Boolean changeEmail(String oldEmail, String newEmail);
    
    boolean makeAuthorizeRedirectResponse(HttpServletResponse response, String uniqueValue, String clientId, String redirectUrl);
    
    boolean makeAuthorizeRedirectResponse(HttpServletResponse response, String uniqueValue, String redirectUrl);
    
    void makeRedirectResponse(HttpServletResponse response, String redirectUrl);
    
    <T extends UserContainer> void registerTemporaryUser(T userContainer);
    
    String reviewToken(String token);
    
    String reviewSocialToken(String token);
    
    String registerEmail(String token);
    
    boolean registerSocialTemporaryUser(WebRequest request, String email);
    
    boolean registerSocialUser(WebRequest request);
}
