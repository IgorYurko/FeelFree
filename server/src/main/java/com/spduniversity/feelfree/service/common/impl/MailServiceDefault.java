package com.spduniversity.feelfree.service.common.impl;

import com.spduniversity.feelfree.domain.dto.EmailNotificationDTO;
import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.service.common.MailService;
import com.spduniversity.feelfree.service.common.extra.VariablesContainer;
import com.spduniversity.feelfree.service.web.bean.UserContainer;
import com.spduniversity.feelfree.web.bean.NeedHelpContainer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.CharEncoding;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import java.time.format.DateTimeFormatter;

@Slf4j
@Service
public class MailServiceDefault implements MailService {

    @Resource
    private JavaMailSender mailSender;
    
    @Resource
    private TemplateEngine templateEngine;
    
    @Resource
    private MessageSource messageSource;
    
    @Resource
    private VariablesContainer variablesContainer;
    
    @Async
    private void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        MimeMessagePreparator preparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
            messageHelper.setFrom(variablesContainer.getFrom());
            messageHelper.setTo(to);
            messageHelper.setSubject(subject);
            messageHelper.setText(content, isHtml);
        };
        try {
            mailSender.send(preparator);
        } catch (MailException ex) {
            log.error(ex.getMessage(), ex);
        }
    }
    
    @Async
    @Override
    public <T extends UserContainer> void sendActivationEmail(@Nonnull T user, @Nonnull String activationKey) {
        String profileEmail = user.getProfileEmail();
        if (StringUtils.isNotBlank(profileEmail)) {
            String subject = messageSource.getMessage("email.activation.title", null, LocaleContextHolder.getLocale());
            Context context = new Context();
            context.setVariable("user", user);
            context.setVariable("activateUrl",
                                String.format("%s/%s%s", variablesContainer.getServerUrl(), variablesContainer.getEmailActivateUrl(), activationKey));
            String content = templateEngine.process("activation", context);
            sendEmail(profileEmail, subject, content, false, true);
        }
    }
    
    @Async
    @Override
    public <T extends UserContainer> void sendSocialMargeEmail(@Nonnull T user, @Nonnull String activationKey) {
        String profileEmail = user.getProfileEmail();
        if (StringUtils.isNotBlank(profileEmail)) {
            String subject = messageSource.getMessage("email.social.marge.text", null, LocaleContextHolder.getLocale());
            Context context = new Context();
            context.setVariable("user", user);
            context.setVariable("activateUrl",
                                String.format("%s/%s%s", variablesContainer.getServerUrl(), variablesContainer.getSocialEmailActivateUrl(), activationKey));
            String content = templateEngine.process("socialMargeActivation", context);
            sendEmail(profileEmail, subject, content, false, true);
        }
    }
    
    @Async
    @Override
    public <T extends UserContainer> void sendActivationEmail(@Nonnull T user, @Nonnull String activationKey, @Nonnull String password) {
        String profileEmail = user.getProfileEmail();
        if (StringUtils.isNotBlank(profileEmail)) {
            String subject = messageSource.getMessage("email.activation.title", null, LocaleContextHolder.getLocale());
            Context context = new Context();
            context.setVariable("user", user);
            context.setVariable("password", password);
            context.setVariable("activateUrl",
                                String.format("%s/%s%s", variablesContainer.getServerUrl(), variablesContainer.getEmailActivateUrl(), activationKey));
            String content = templateEngine.process("socialActivation", context);
            sendEmail(profileEmail, subject, content, false, true);
        }
    }
    
    @Async
    @Override
    public <T extends UserContainer> void sendEmailResetEmail(@Nonnull T user, @Nonnull String activationKey) {
        String profileEmail = user.getProfileEmail();
        if (StringUtils.isNotBlank(profileEmail)) {
            String subject = messageSource.getMessage("email.reset.title", null, LocaleContextHolder.getLocale());
            Context context = new Context();
            context.setVariable("user", user);
            context.setVariable("activateUrl",
                                String.format("%s/%s%s", variablesContainer.getServerUrl(), variablesContainer.getEmailResetUrl(), activationKey));
            String content = templateEngine.process("resetEmail", context);
            sendEmail(profileEmail, subject, content, false, true);
        }
    }
	
	@Async
    @Override
    public void sendPasswordResetEmail(@Nonnull User user, @Nonnull String password) {
        String profileEmail = user.getProfileEmail();
        if (StringUtils.isNotBlank(profileEmail)) {
            String subject = messageSource.getMessage("email.pswreset.title", null, LocaleContextHolder.getLocale());
            Context context = new Context();
            context.setVariable("user", user);
            context.setVariable("password", password);
            String content = templateEngine.process("pswreset", context);
            sendEmail(profileEmail, subject, content, false, true);
        }
    }

    @Async
    @Override
    public void sendMessageNotification(@Nonnull EmailNotificationDTO dto) {
        String profileEmail = dto.getUserTo().getProfileEmail();
        if (StringUtils.isNotBlank(profileEmail)) {
            String subject = messageSource.getMessage("email.message.notification.title", new Object[]{dto.getTitle()}, LocaleContextHolder.getLocale());
            Context context = new Context();
            context.setVariable("dto", dto);
            String content = templateEngine.process("messageNotification", context);
            sendEmail(profileEmail, subject, content, false, true);
        }
    }

    @Async
    @Override
    public void sendEmailRequestBook(@Nonnull EmailNotificationDTO dto) {
        String profileEmail = dto.getUserTo().getProfileEmail();
        if (StringUtils.isNotBlank(profileEmail)) {
            String subject = messageSource.getMessage("email.message.request.title", new Object[]{dto.getTitle()}, LocaleContextHolder.getLocale());
            Context context = new Context();
            context.setVariable("dto", dto);
            context.setVariable("checkInDate", dto.getRent().getCheckInDate().format(dateFormat()));
            context.setVariable("checkOutDate", dto.getRent().getCheckOutDate().format(dateFormat()));
            String content = templateEngine.process("requestNotification", context);
            sendEmail(profileEmail, subject, content, false, true);
        }
    }
    @Async
    @Override
    public void sendStatusOfRequestBook(@Nonnull EmailNotificationDTO dto) {
        String profileEmail = dto.getUserTo().getProfileEmail();
        if (StringUtils.isNotBlank(profileEmail)) {
            String subject = messageSource.getMessage("email.message.request.title.status", new Object[]{dto.getTitle(),dto.getRent().getStatus()}, LocaleContextHolder.getLocale());
            Context context = new Context();
            context.setVariable("dto", dto);
            String content = templateEngine.process("statusRequestNotification", context);
            sendEmail(profileEmail, subject, content, false, true);
        }
    }

    private DateTimeFormatter dateFormat() {
       return DateTimeFormatter.ofPattern("dd-MMM-yyyy");
    }


    @Async
	@Override
	public <T extends UserContainer> void sendSocialPasswordEmail(@Nonnull T user, @Nonnull String password) {
        String profileEmail = user.getProfileEmail();
        if (StringUtils.isNotBlank(profileEmail)) {
            String subject = messageSource.getMessage("email.social.password.title", null, LocaleContextHolder.getLocale());
            Context context = new Context();
            context.setVariable("user", user);
            context.setVariable("password", password);
            String content = templateEngine.process("socialPassword", context);
            sendEmail(profileEmail, subject, content, false, true);
        }
	}
    
    @Async
    @Override
    public void sendNeedHelpEmail(@Nonnull NeedHelpContainer container) {
        String subject =container.getTitle();
        String content = container.getMessageBody() +" ---send answer to this email: " + container.getEmail();
        sendEmail(variablesContainer.getSupportEmail(), subject, content, false, true);
    }
}
