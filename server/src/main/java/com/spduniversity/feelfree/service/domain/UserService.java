package com.spduniversity.feelfree.service.domain;

import com.spduniversity.feelfree.domain.dto.AnnouncementAccountFavoriteDTO;
import com.spduniversity.feelfree.domain.dto.ImageDTO;
import com.spduniversity.feelfree.domain.dto.UserDTO;
import com.spduniversity.feelfree.domain.dto.UserDetailsDTO;
import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.web.bean.UserInfoContainer;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;

public interface UserService {
	UserDetailsDTO getUserDetailsData(String email);

	UserDTO getUserData(String email);

	@Nullable
	User getUser(@Nullable String profileEmail);
	
	@Nullable
	User getUserByEmail(@Nullable String profileEmail);

	@Nullable
	ImageDTO saveUserImage(@Nullable MultipartFile file, String profileEmail);
	
	void changePassword(String password , String email);

	void changeInfo(UserInfoContainer container, String email);

	boolean save(User user);

	Boolean resetPassword(String email);
	
	@Transactional
	void enableUser(User user);
	
	Set<AnnouncementAccountFavoriteDTO> getFavorites(String email);
	
	List<String> getFavoritesData(String email);
	
	@Transactional
	boolean addFavorite(String uniqueValue, String uuid);
	
	@Transactional
	boolean removeFavorite(String uniqueValue, String uuid);
	
	boolean isAuthorize(@Nullable String uniqueValue);
	
	boolean isReservedEmail(@Nullable String email);
	
	@Nullable
	User findByUniqueValue(String email);
	
	void updateProfileEmail(Long userId, String profileEmail);
}
