package com.spduniversity.feelfree.service.domain;

import com.spduniversity.feelfree.domain.model.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.function.Function;

public interface CommentService {
	@Nonnull
	Comment save(@Nonnull Comment comment);
	
	@SuppressWarnings("unchecked")
	@Nonnull
	<T> Page<T> getCommentData(@Nonnull Function<Comment, T> mapperFunction,
							   @Nonnull Pageable pageable,
							   @Nullable Specification<Comment>... specifications);
	
}
