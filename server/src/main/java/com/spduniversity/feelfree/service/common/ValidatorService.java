package com.spduniversity.feelfree.service.common;

import java.util.Map;

public interface ValidatorService {
	boolean isValid(Object obj);
	boolean isValid(Object obj, String field);
	
	boolean isValid(Object... obj);
	
	Map<String, String> getErrors(Object obj);
}
