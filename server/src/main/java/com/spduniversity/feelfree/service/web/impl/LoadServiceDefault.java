package com.spduniversity.feelfree.service.web.impl;

import com.spduniversity.feelfree.domain.dto.LoadImageDTO;
import com.spduniversity.feelfree.service.common.BlobService;
import com.spduniversity.feelfree.service.common.CacheService;
import com.spduniversity.feelfree.service.common.ResizeImageService;
import com.spduniversity.feelfree.service.common.bean.CacheContainer;
import com.spduniversity.feelfree.service.domain.ImageCacheService;
import com.spduniversity.feelfree.service.domain.ImageService;
import com.spduniversity.feelfree.service.web.LoadService;
import com.spduniversity.feelfree.web.bean.LoadImageContainer;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.sql.Blob;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Service
public class LoadServiceDefault implements LoadService {
	
	private final ExecutorService executorService = Executors.newSingleThreadExecutor();
	
	@Resource
	private ImageService imageService;
	
	@Resource
	private ImageCacheService imageCacheService;
	
	@Resource
	private ResizeImageService resizeImageService;
	
	@Resource
	private CacheService cacheService;
	
	@Resource
	private BlobService blobService;
	
	@Nullable
	public LoadImageDTO getImage(@Nullable LoadImageContainer loadImageContainer) {
		if (loadImageContainer == null) {
			return null;
		}
		Integer width = loadImageContainer.getWidth();
		Integer height = loadImageContainer.getHeight();
		String name = loadImageContainer.getName();
		
		if (width == null || height == null) {
			return imageService.getImageData(loadImageContainer.getName());
		}
		
		if (imageCacheService.isExistCacheImageData(name, width, height)) {
			return imageCacheService.getImageDataToDTO(name, width, height);
		}
		
		LoadImageDTO imageData = getResizeImage(name, width, height);
		if (imageData != null) {
			executorService.submit(() -> createCache(imageData, width, height));
		}
		return imageData;
	}
	
	@Nullable
	private LoadImageDTO getResizeImage(String name, Integer width, Integer height) {
		LoadImageDTO imageData = imageService.getImageData(name);
		if (imageData != null) {
			try {
				byte[] scaleImage = resizeImageService.scaleImage(imageData.getValue().getBinaryStream(), width, height);
				return toLoadImageDto(scaleImage, imageData.getName(), imageData.getType());
			} catch (Exception ex) {
				log.error(ex.getMessage(), ex);
			}
		}
		return null;
	}
	
	private LoadImageDTO toLoadImageDto(byte[] scaleImage, String nameDto, String typeDto) {
		return new LoadImageDTO() {
			@Getter private final String name = nameDto;
			@Getter private final Blob value  = blobService.createBlob(scaleImage);
			@Getter private final String type = typeDto;
		};
	}
	
	private void createCache(LoadImageDTO imageData, Integer width, Integer height) {
		CacheContainer cacheContainer = new CacheContainer();
		BeanUtils.copyProperties(imageData, cacheContainer);
		cacheContainer.setHeight(height);
		cacheContainer.setWidth(width);
		cacheService.createCache(cacheContainer);
	}
}
