package com.spduniversity.feelfree.service.web;

import com.spduniversity.feelfree.domain.dto.ConversationDTO;
import com.spduniversity.feelfree.domain.dto.MessageDTO;
import com.spduniversity.feelfree.domain.model.Announcement;
import com.spduniversity.feelfree.domain.model.Rent;
import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.web.bean.AccountMessageAnnouncementContainer;
import com.spduniversity.feelfree.web.bean.AccountMessageContainer;

import javax.annotation.Nullable;
import java.util.Set;

public interface MessageService {
   boolean saveAnnouncementMessage(AccountMessageAnnouncementContainer container, String userEmail);

   @Nullable
   MessageDTO saveMessage(AccountMessageContainer container, String email);

   Set<ConversationDTO> allMessages(String uniqueData);

   void saveMessageAtRent(User userFrom, Announcement announcement, Rent rent);

   void updateMessageAtRent(Rent rent, String profileEmail);

}
