package com.spduniversity.feelfree.service.domain;

import com.spduniversity.feelfree.domain.dto.MinMaxPriceDTO;

public interface PriceService {
	MinMaxPriceDTO getPriceRange();
}
