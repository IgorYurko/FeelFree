package com.spduniversity.feelfree.service.domain;

import com.spduniversity.feelfree.domain.dto.GeolocationSearchDTO;
import com.spduniversity.feelfree.domain.model.Announcement;
import com.spduniversity.feelfree.service.domain.bean.AnnouncementContainer;
import com.spduniversity.feelfree.service.domain.bean.GeolocationContainer;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.function.Function;

public interface AnnouncementService {
	
	@Nullable
	<T> T getAnnouncementData(@Nonnull String uuid,
							  @NotNull Function<Announcement, T> mapperFunction);
	@Nullable
	@SuppressWarnings("unchecked")
	<T> T getAnnouncementData(@NonNull Function<Announcement, T> mapperFunction,
							  @Nullable Specification<Announcement>... specifications);
	
	@Nonnull
	<T> Page<T> getAnnouncementData(@Nonnull AnnouncementContainer announcementContainer,
									@Nonnull Pageable pageable,
									@Nonnull Function<Announcement, T> mapperFunction);
	
	@Nonnull
	@SuppressWarnings("unchecked")
	<T> Page<T> getAnnouncementData(@Nonnull Function<Announcement, T> mapperFunction,
									@Nonnull Pageable pageable,
									@Nullable Specification<Announcement>... specifications);
	
	@Nonnull
	Page<GeolocationSearchDTO> getGeolocationData(@Nonnull GeolocationContainer searchGeolocationBean,
												  @Nullable Integer offSet,
												  @Nullable Integer rows);
	
	@Nullable
	Announcement save(@Nonnull Announcement announcement);
	
	@Nullable
	Announcement getAnnouncement(@Nonnull Specification<Announcement> specification);
	
	@Nullable
	Announcement getAnnouncement(@Nullable String uuid);
	
	boolean delete(@Nonnull Specification<Announcement> specification);
}
