package com.spduniversity.feelfree.service.common.impl;

import com.spduniversity.feelfree.service.common.CookieService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.request.NativeWebRequest;

import javax.annotation.Nullable;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

@Service
public class CookieServiceDefault implements CookieService {
	
	private static final int MAX_AGE = 60;
	
	@Override
	@Nullable
	public Cookie create(String name, String value, Integer maxAge, Encoder encoder) {
		if (StringUtils.isBlank(name) || value == null) {
			return null;
		}
		Cookie cookie = encoder != null ? new Cookie(name, encoder.encode(value)) : new Cookie(name, value);
		cookie.setPath("/");
		cookie.setMaxAge(maxAge == null ? MAX_AGE : maxAge);
		cookie.setSecure(false);
		return cookie;
	}
	
	@Override
	public void add(NativeWebRequest request, String name, String value, Integer maxAge, Encoder encoder) {
		if (request == null) {
			return;
		}
		Cookie cookie = create(name, value, maxAge, encoder);
		if (cookie != null) {
			((HttpServletResponse)request.getNativeResponse()).addCookie(cookie);
		}
	}
	
	@Override
	public void add(HttpServletResponse response, String name, String value, Integer maxAge, Encoder encoder) {
		if (response == null) {
			return;
		}
		Cookie cookie = create(name, value, maxAge, encoder);
		if (cookie != null) {
			response.addCookie(cookie);
		}
	}
	
	@Override
	public void add(HttpServletResponse response, Cookie... cookies) {
		if (response != null) {
			Arrays.stream(cookies)
				  .forEach(response::addCookie);
		}
	}
	
	@Override
	public void add(NativeWebRequest request, Cookie... cookies) {
		if (request != null) {
			HttpServletResponse response = (HttpServletResponse) request.getNativeResponse();
			add(response, cookies);
		}
	}
	
	public enum Encoder {
		BASE64 {
			@Override
			String encode(String value) {
				return Base64Utils.encodeToString(value.getBytes());
			}
		},
		NON {
			@Override
			String encode(String value) {
				return value;
			}
		};
		
		abstract String encode(String value);
	}
}
