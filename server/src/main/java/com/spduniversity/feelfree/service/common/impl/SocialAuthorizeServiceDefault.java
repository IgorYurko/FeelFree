package com.spduniversity.feelfree.service.common.impl;

import com.spduniversity.feelfree.domain.repository.SocialConnectionRepository;
import com.spduniversity.feelfree.service.common.SocialAuthorizeService;
import com.spduniversity.feelfree.service.common.util.UserSecurityUtil;
import com.spduniversity.feelfree.service.domain.TemporaryUserService;
import com.spduniversity.feelfree.service.domain.UserService;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.social.connect.Connection;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.Optional;

@Log4j
@Service
public class SocialAuthorizeServiceDefault implements SocialAuthorizeService {
	
	@Resource
	private UserService userService;
	
	@Resource
	private UserDetailsService userDetailsService;
	
	@Resource
	private TemporaryUserService temporaryUserService;
	
	@Resource
	private AuthorizationServerTokenServices jdbcTokenServices;
	
	@Resource
	private JdbcTokenStore tokenStore;

	@Override
	public boolean isAuthorize(String uniqueValue) {
		return userService.isAuthorize(uniqueValue);
	}
	
	@Override
	public boolean isTemporary(String uniqueValue) {
		return temporaryUserService.isNonExpire(uniqueValue);
	}
	
	@Resource(name = "socialConnectionRepositoryDefault")
	private SocialConnectionRepository connectionRepository;
	
	@Override
	@Nullable
	public UserDetails getDetailsData(String uniqueValue) {
		try {
			return userDetailsService.loadUserByUsername(uniqueValue);
		} catch (UsernameNotFoundException ex) {
			log.error(ex.getMessage());
			return null;
		}
	}
	
	@Override
	public OAuth2AccessToken authorizeUser(String uniqueValue, String clientId) {
		return Optional.ofNullable(StringUtils.stripToNull(uniqueValue))
					   .map(this::getDetailsData)
					   .map(e -> createAccessToken(e, clientId.trim()))
					   .orElse(null);
	}
	
	private OAuth2AccessToken createAccessToken(UserDetails userData, String clientId) {
		UsernamePasswordAuthenticationToken authentication =
				new UsernamePasswordAuthenticationToken(userData, userData.getPassword(), userData.getAuthorities());
		OAuth2Request oAuth2Request =
				new OAuth2Request(null,
								  clientId,
								  userData.getAuthorities(),
								  true,
								  null,
								  null,
								  null,
								  null,
								  null);
		
		OAuth2Authentication auth = new OAuth2Authentication(oAuth2Request, authentication);
		return jdbcTokenServices.createAccessToken(auth);
	}
	
	@Override
	public OAuth2AccessToken reauthorizeUser(String uniqueValue, String clientId) {
		UserDetails userData = Optional.ofNullable(StringUtils.stripToNull(uniqueValue))
										  .map(this::getDetailsData)
										  .orElse(null);
		if (userData != null) {
			if (removeAccessToken()) {
				return createAccessToken(userData, clientId.trim());
			}
		}
		return null;
	}
	
	@Override
	public boolean removeAccessToken() {
		Authentication authentication = UserSecurityUtil.getAuthentication();
		if (authentication != null) {
			try {
				String tokenValue = ((OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue();
				tokenStore.removeAccessToken(tokenValue);
				return true;
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
		}
		return false;
	}
	
	@Override
	public boolean deleteConnection(Connection<?> connection) {
		if (connection != null) {
			return connectionRepository
						   .deleteConnection(connection.getKey().getProviderId(), connection.getKey().getProviderUserId()) > 0;
		}
		return false;
	}
}
