package com.spduniversity.feelfree.service.common.extra;

import org.springframework.context.i18n.LocaleContextHolder;

import javax.validation.MessageInterpolator;
import java.util.Locale;

public class ServerLocaleMessageInterpolator implements MessageInterpolator {
	private final MessageInterpolator delegate;
	
	ServerLocaleMessageInterpolator(MessageInterpolator delegate) {
		this.delegate = delegate;
	}
	
	@Override
	public String interpolate(String message, Context context) {
		return this.interpolate(message, context, LocaleContextHolder.getLocale());
	}
	
	@Override
	public String interpolate(String message, Context context, Locale locale) {
		return delegate.interpolate(message, context, locale);
	}
}
