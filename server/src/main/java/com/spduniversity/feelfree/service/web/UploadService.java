package com.spduniversity.feelfree.service.web;

import com.spduniversity.feelfree.web.bean.UploadImageContainer;

import javax.annotation.Nullable;

public interface UploadService {
	boolean uploadImage(@Nullable UploadImageContainer container);
}
