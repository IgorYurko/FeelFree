package com.spduniversity.feelfree.service.domain;


import com.spduniversity.feelfree.domain.dto.LoadImageDTO;
import com.spduniversity.feelfree.domain.model.Image;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.function.Function;

public interface ImageService {
	@Nullable
	LoadImageDTO getImageData(@Nullable String name);
	
	boolean isSave(Image image);
	
	@Nonnull
	<T> List<T> save(@Nullable List<Image> images, @Nonnull Function<Image, T> mapperFunction);
	
	@Nonnull
	List<Image> save(@Nullable List<Image> images);
	
	boolean delete(@Nonnull String announcementUuid, @Nonnull String imageUuid, @Nonnull String profileEmail);

	Image save(@NotNull Image image);
	
	Image getImage(String announcementUuid, String uuid, String profileEmail);
}
