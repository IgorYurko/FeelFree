package com.spduniversity.feelfree.service.common.extra;

import com.spduniversity.feelfree.domain.model.Announcement;
import com.spduniversity.feelfree.domain.repository.core.ResultBuilder;
import com.spduniversity.feelfree.domain.specification.AnnouncementSpecificationHelper;
import com.spduniversity.feelfree.service.domain.bean.AnnouncementContainer;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

import static java.lang.Boolean.TRUE;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@SuppressWarnings("unchecked")
public class AnnouncementSearchSpecificationCreator {
	
	private static final AnnouncementSpecificationHelper specification = new AnnouncementSpecificationHelper();
	private static final Map<Predicate<AnnouncementContainer>, BiConsumer<ResultBuilder, AnnouncementContainer>> filter = new HashMap<>();
	
	static {
		filter.put(a -> TRUE, (rb, a) -> rb.whereAnd(specification.nonHidden()));
		filter.put(a -> isNotBlank(a.getPlaceId()), (rb, a) -> rb.whereAnd(specification.havePlaceId(a.getPlaceId())));
		filter.put(a -> isNotBlank(a.getCountry()) && isBlank(a.getPlaceId()), (rb, a) -> rb.whereAnd(specification.likeCountry(a.getCountry().trim())));
		filter.put(a -> isNotBlank(a.getCity()) && isBlank(a.getPlaceId()), (rb, a) -> rb.whereAnd(specification.likeCity(a.getCity().trim())));
		filter.put(a -> isNotBlank(a.getRegion())  && isBlank(a.getPlaceId()), (rb, a) -> rb.whereAnd(specification.likeRegion(a.getRegion().trim())));
		filter.put(a -> nonNull(a.getMinPrice()), (rb, a) -> rb.whereAnd(specification.greaterThanOrEqualPrice(a.getMinPrice())));
		filter.put(a -> nonNull(a.getMaxPrice()), (rb, a) -> rb.whereAnd(specification.lessThanOrEqualPrice(a.getMaxPrice())));
		filter.put(a -> nonNull(a.getPeriod()), (rb, a) -> rb.whereAnd(specification.equalPeriod(a.getPeriod())));
		filter.put(a -> TRUE.equals(a.getNonNullImage()), (rb, a) -> rb.whereAnd(specification.notNullImage()));
		filter.put(a -> nonNull(a.getRooms()), (rb, a) -> rb.whereAnd(specification.equalRooms(a.getRooms())));
		filter.put(a -> nonNull(a.getLivingPlaces()), (rb, a) -> rb.whereAnd(specification.equalLivingPlaces(a.getLivingPlaces())));
		filter.put(a -> TRUE.equals(a.getWiFi()), (rb, a) -> rb.whereAnd(specification.trueWifi()));
		filter.put(a -> TRUE.equals(a.getWashingMachine()), (rb, a) -> rb.whereAnd(specification.trueWashingMachine()));
		filter.put(a -> TRUE.equals(a.getRefrigerator()), (rb, a) -> rb.whereAnd(specification.trueRefrigerator()));
		filter.put(a -> TRUE.equals(a.getHairDryer()), (rb, a) -> rb.whereAnd(specification.trueHairDryer()));
		filter.put(a -> TRUE.equals(a.getIron()), (rb, a) -> rb.whereAnd(specification.trueIron()));
		filter.put(a -> TRUE.equals(a.getSmoking()), (rb, a) -> rb.whereAnd(specification.trueSmoking()));
		filter.put(a -> TRUE.equals(a.getAnimals()), (rb, a) -> rb.whereAnd(specification.trueAnimals()));
		filter.put(a -> TRUE.equals(a.getKitchenStuff()), (rb, a) -> rb.whereAnd(specification.trueKitchenStuff()));
		filter.put(a -> TRUE.equals(a.getConditioner()), (rb, a) -> rb.whereAnd(specification.trueConditioner()));
		filter.put(a -> TRUE.equals(a.getBalcony()), (rb, a) -> rb.whereAnd(specification.trueBalcony()));
		filter.put(a -> TRUE.equals(a.getTv()), (rb, a) -> rb.whereAnd(specification.trueTv()));
		filter.put(a -> TRUE.equals(a.getEssentials()), (rb, a) -> rb.whereAnd(specification.trueEssentials()));
		filter.put(a -> TRUE.equals(a.getShampoo()), (rb, a) -> rb.whereAnd(specification.trueShampoo()));
	}
	
	public static <T> void execute(@Nullable ResultBuilder<Announcement, T> rb, @Nullable AnnouncementContainer container) {
		if (rb == null || container == null) {
			return;
		}
		filter.forEach((k, v) -> {
			if (k.test(container)) {
				v.accept(rb, container);
			}
		});
	}
}
