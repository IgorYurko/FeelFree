package com.spduniversity.feelfree.service.common.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;

public class UserSecurityUtil {
	
	private static final String ANONYMOUS = "ROLE_ANONYMOUS";
	private static final String USER = "ROLE_USER";
	private static final String ADMIN = "ROLE_ADMIN";
	
	public static boolean isAnonymous() {
		return SecurityContextHolder.getContext().getAuthentication().getAuthorities()
									.stream()
									.anyMatch(e -> ANONYMOUS.equals(e.getAuthority()));
	}
	
	public static boolean isUser() {
		return SecurityContextHolder.getContext().getAuthentication().getAuthorities()
									.stream()
									.anyMatch(e -> USER.equals(e.getAuthority()));
	}
	
	public static boolean isAdmin() {
		return SecurityContextHolder.getContext().getAuthentication().getAuthorities()
									.stream()
									.anyMatch(e -> ADMIN.equals(e.getAuthority()));
	}
	
	public static boolean checkRole(String authority) {
		if (StringUtils.isBlank(authority)) {
			return false;
		}
		return SecurityContextHolder.getContext().getAuthentication().getAuthorities()
									.stream()
									.anyMatch(e -> authority.equals(e.getAuthority()));
	}
	
	public static String getName() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}
	
	public static Collection<? extends GrantedAuthority> getRoles() {
		return SecurityContextHolder.getContext().getAuthentication().getAuthorities();
	}
	
	public static Authentication getAuthentication () {
		return SecurityContextHolder.getContext().getAuthentication();
	}
}
