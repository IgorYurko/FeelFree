package com.spduniversity.feelfree.service.common.extra;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.regex.Pattern;

@Slf4j
public class EntityMapper extends ObjectMapper {
	private static final Pattern FIRST_WORD = Pattern.compile("(?=[A-Z])");
	private final String prefixFilter;
	private final String suffixBean;
	
	public EntityMapper(String prefixFilter, String suffixBean) {
		this.prefixFilter = StringUtils.appendIfMissing(prefixFilter, ".");
		this.suffixBean = suffixBean;
	}
	
	public <T> T convertEntityValue(Object fromValue, Class<T> toValueType) throws IllegalArgumentException {
		SimpleFilterProvider provider = getProvider(getFiltersAndField(toValueType));
		setFilterProvider(provider);
		return super.convertValue(fromValue, toValueType);
	}
	
	@Override
	public <T> T convertValue(Object fromValue, Class<T> toValueType) throws IllegalArgumentException {
		setFilterProvider(createProvider());
		return super.convertValue(fromValue, toValueType);
	}
	
	private SimpleFilterProvider createProvider() {
		SimpleFilterProvider filterProvider = new SimpleFilterProvider();
		filterProvider.setFailOnUnknownId(false);
		return filterProvider;
	}
	
	private SimpleFilterProvider getProvider(Map<String, List<String>> filters) {
		SimpleFilterProvider provider = createProvider();
		filters.forEach((k, v) -> provider.addFilter(k, SimpleBeanPropertyFilter.filterOutAllExcept(v.toArray(new String[0]))));
		return provider;
	}
	
	private <T> Map<String, List<String>> getFiltersAndField(Class<T> clazz) {
		if (clazz == null) {
			return Collections.emptyMap();
		}
		Map<String, List<String>> filterFields = new HashMap<>();
		JsonFilter annotation = getJsonFilterAnnotation(clazz);
		try {
			if (annotation != null) {
				String filter = annotation.value();
				Field[] fields = FieldUtils.getAllFields(clazz);
				for (Field field : fields) {
					String typeName = getTypeName(field);
					if (StringUtils.endsWith(typeName, suffixBean)) {
						putFilterToMap(filterFields, filter, field);
						filterFields.putAll(getFiltersAndField(getClass(field)));
					} else {
						putFilterToMap(filterFields, filter, field);
					}
				}
			} else {
				return Collections.emptyMap();
			}
		} catch (Exception ex) {
			return Collections.emptyMap();
		}
		return filterFields;
	}
	
	private JsonFilter getJsonFilterAnnotation(Class<?> clazz) {
		if (clazz != null) {
			JsonFilterTarget target = clazz.getAnnotation(JsonFilterTarget.class);
			if (target != null) {
				Class<?> classTarget = target.filterClass();
				return classTarget.getAnnotation(JsonFilter.class);
			}
			try {
				String classTargetName = FIRST_WORD.splitAsStream(clazz.getSimpleName()).findFirst().orElse(null);
				Class<?> classTarget = Class.forName(prefixFilter + classTargetName);
				return classTarget.getAnnotation(JsonFilter.class);
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
		}
		return null;
	}
	
	private String getTypeName(Field field) {
		if (Collection.class.isAssignableFrom(field.getType())) {
			return getGenericTypeName(field);
		}
		return field.getType().getName();
	}
	
	private Class<?> getClass(Field field) {
		if (Collection.class.isAssignableFrom(field.getType())) {
			try {
				return Class.forName(getGenericTypeName(field));
			} catch (ClassNotFoundException e) {
				return null;
			}
		}
		return field.getType();
	}
	
	private void putFilterToMap(Map<String, List<String>> map, String filter, Field field) {
		map.computeIfAbsent(filter, k -> new ArrayList<>());
		List<String> strings = map.get(filter);
		strings.add(field.getName());
		map.put(filter, strings);
	}
	
	private String getGenericTypeName(Field field) {
		return getGenericType(field).getTypeName();
	}
	
	private Type getGenericType(Field field) {
		return ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
	}
}
