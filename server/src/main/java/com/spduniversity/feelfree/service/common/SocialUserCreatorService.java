package com.spduniversity.feelfree.service.common;

import com.spduniversity.feelfree.domain.model.TemporaryUser;
import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.service.common.bean.UserPasswordContainer;
import org.springframework.social.connect.Connection;

public interface SocialUserCreatorService {
	boolean isExistEmail(Connection<?> connection);
	
	UserPasswordContainer<User> createUser(Connection<?> connection);
	
	UserPasswordContainer<TemporaryUser> createTemporaryUser(Connection<?> connection);
	
	void editUser(Connection<?> connection, User user);
}
