package com.spduniversity.feelfree.service.web;

import com.spduniversity.feelfree.domain.dto.LoadImageDTO;
import com.spduniversity.feelfree.web.bean.LoadImageContainer;

import javax.annotation.Nullable;

public interface LoadService {
	@Nullable
	LoadImageDTO getImage(@Nullable LoadImageContainer loadImageContainer);
}
