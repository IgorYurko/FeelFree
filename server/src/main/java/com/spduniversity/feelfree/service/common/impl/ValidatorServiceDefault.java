package com.spduniversity.feelfree.service.common.impl;

import com.spduniversity.feelfree.service.common.ValidatorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ValidatorServiceDefault implements ValidatorService {
	
	private final static Pattern DEFAULT_DELIMITER_FIELD = Pattern.compile("\\.");
	private final static String DEFAULT_DELIMITER_FIELD_VALUE = ".";
	private Validator validator;
	
	@Autowired
	public ValidatorServiceDefault(Validator validator) {
		this.validator = validator;
	}
	
	@Override
	public boolean isValid(Object obj) {
		return obj != null && validate(obj).size() == 0;
	}
	
	@Override
	public boolean isValid(Object obj, String field) {
		return obj != null && validate(obj, field).size() == 0;
	}
	
	@Override
	public boolean isValid(Object... obj) {
		return Arrays.stream(obj).allMatch(e -> isValid(obj));
	}
	
	
	@Override
	public Map<String, String> getErrors(Object obj) {
		if (Objects.isNull(obj) || isValid(obj)) {
			return Collections.emptyMap();
		}
		Set<ConstraintViolation<Object>> violations = validate(obj);
		HashMap<String, String> result = new HashMap<>();
		violations.forEach(e -> {
			String field = e.getPropertyPath().toString();
			if (field.contains(DEFAULT_DELIMITER_FIELD_VALUE)) {
				result.putAll(splitFields(field, e.getMessage()));
				return;
			}
			result.put(field, e.getMessage());
			
		});
		return result;
	}
	
	private Map<String, String> splitFields(String fields, String message) {
		return DEFAULT_DELIMITER_FIELD.splitAsStream(fields)
									  .collect(Collectors.toMap(k -> k, v -> message));
	}
	
	private Set<ConstraintViolation<Object>> validate(Object obj) {
		return validator.validate(obj);
	}
	
	private Set<ConstraintViolation<Object>> validate(Object obj, String field) {
		return validator.validateProperty(obj, field);
	}
}
