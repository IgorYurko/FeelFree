package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(exclude = {"user", "announcement"})
@ToString(exclude = {"user", "announcement"})
@Entity
@Table(name = "rent")
@JsonFilter("rentFilter")
public class Rent implements Serializable {
	
	private static final long serialVersionUID = -3991257604018113283L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "check_in_date")
	private LocalDateTime checkInDate;
	
	@Column(name = "check_out_date")
	private LocalDateTime checkOutDate;
	
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	@JsonBackReference
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "announcement_rent", joinColumns = @JoinColumn(name = "rent_id"),
			   inverseJoinColumns = @JoinColumn(name = "announcement_id")
	)
	@JsonBackReference
	private Announcement announcement;
	
	public enum Status {
		CONFIRMED,
		REJECTED,
		WAITING
	}
}
