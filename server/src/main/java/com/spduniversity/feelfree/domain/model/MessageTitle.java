package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "message_title")
@EqualsAndHashCode(exclude = {"text"})
@ToString(exclude = {"text"})
public class MessageTitle {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "text")
    @Size(max = 170)
    @JsonBackReference
    private String text;


}
