package com.spduniversity.feelfree.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashSet;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnnouncementSearchDTO {
	private String uuid;
	private AddressDTO address;
	private String title;
	private String shortDescription;
	private LinkedHashSet<ImageDTO> images;
	private PriceDTO price;
	private GeolocationDTO geolocation;
	private Favorite favorite;
}
