package com.spduniversity.feelfree.domain.specification;

import com.spduniversity.feelfree.domain.model.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Path;
import java.time.LocalDateTime;

@Component
public class RentSpecificationHelper {
	
	public Specification<Rent> equalAnnouncementUuid(String uuid) {
		return (root, query, cb) -> {
			Path<Announcement> announcement = root.get(Rent_.announcement);
			return cb.equal(announcement.get(Announcement_.uuid), uuid);
		};
	}
	
	public Specification<Rent> equalUserProfileEmail(String profileEmail) {
		return (root, query, cb) -> {
			Path<Announcement> announcement = root.get(Rent_.announcement);
			Path<User> user = announcement.get(Announcement_.user);
			return cb.equal(user.get(User_.profileEmail), profileEmail);
		};
	}
	
	public Specification<Rent> userEnabled() {
		return (root, query, cb) -> {
			Path<Announcement> announcement = root.get(Rent_.announcement);
			Path<User> user = announcement.get(Announcement_.user);
			return cb.equal(user.get(User_.enabled), true);
		};
	}
	
	public Specification<Rent> equalsAnnouncementAndUserActive(String announcementUuid, String userProfile) {
		return Specifications.where(equalAnnouncementUuid(announcementUuid))
							 .and(equalUserProfileEmail(userProfile))
							 .and(userEnabled());
	}
	
	public Specification<Rent> greaterThanOrEqualToDate(LocalDateTime from) {
		return (root, query, cb) -> cb.greaterThanOrEqualTo(root.get(Rent_.checkInDate), from);
	}

	public Specification<Rent> lessThanOrEqualToDate(LocalDateTime to) {
		return (root, query, cb) -> cb.lessThanOrEqualTo(root.get(Rent_.checkInDate), to);
	}
	
	public Specification<Rent> equalStatus(Rent.Status status) {
		return (root, query, cb) -> cb.equal(root.get(Rent_.status), status);
	}
}
