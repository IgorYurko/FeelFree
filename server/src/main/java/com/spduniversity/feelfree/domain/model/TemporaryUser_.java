package com.spduniversity.feelfree.domain.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.time.LocalDateTime;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TemporaryUser.class)
public abstract class TemporaryUser_ {

	public static volatile SingularAttribute<TemporaryUser, String> googleId;
	public static volatile SingularAttribute<TemporaryUser, LocalDateTime> expiryDate;
	public static volatile SingularAttribute<TemporaryUser, String> firstName;
	public static volatile SingularAttribute<TemporaryUser, String> lastName;
	public static volatile SingularAttribute<TemporaryUser, Image> image;
	public static volatile SingularAttribute<TemporaryUser, String> profileEmail;
	public static volatile SingularAttribute<TemporaryUser, String> password;
	public static volatile SingularAttribute<TemporaryUser, String> facebookId;
	public static volatile SingularAttribute<TemporaryUser, Long> id;
	public static volatile SingularAttribute<TemporaryUser, String> token;

}

