package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "temporary_email")
@JsonFilter("temporaryEmailFilter")
public class TemporaryEmail implements Serializable {
	
	private static final long serialVersionUID = -1401633777636406068L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	
	@Column(name = "value")
	private String value;
	
	@Column(name = "token")
	private String token;
	
	@Column(name = "expiry")
	private LocalDateTime expiryDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;
	
	@PrePersist
	private void executePersist() {
		if (expiryDate == null) {
			expiryDate = LocalDateTime.now().plusDays(1);
		}
	}
}
