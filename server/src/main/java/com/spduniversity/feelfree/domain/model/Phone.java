package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.Data;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Entity
@Table(name = "phone")
@JsonFilter("phoneFilter")
public class Phone implements Serializable {
	
	private static final long serialVersionUID = 7174652158146241429L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "value", length = 15)
	@Size(max = 15)
	@NumberFormat
	private String value;
}
