package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Conversation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConversationRepository extends JpaRepository<Conversation, Long>, ConversationRepositoryCustom {
    Conversation findById(Long id);





}
