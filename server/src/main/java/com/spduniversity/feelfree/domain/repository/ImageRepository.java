package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Transactional(readOnly = true)
public interface ImageRepository extends JpaRepository<Image, Long> {
	
	@SuppressWarnings("SpringDataJpaMethodInconsistencyInspection")
	@Nullable
	<T> T findTopByName(String name, Class<T> beanClass);
	
	@Modifying
	@Transactional
	@Query("delete from Image i where i.id = " +
		   "(select im.id from Announcement a " +
		   "join a.images im " +
		   "join a.user u " +
		   "where a.uuid = :aUuid and im.name = :iUuid and u.profileEmail = :profileEmail and u.enabled = true)")
	void delete(@Param("aUuid") String announcementUuid, @Param("iUuid") String imageUuid, @Param("profileEmail") String profileEmail);
	
	@Query("select new java.lang.Boolean(count(i.id) = 0) from Image i where i.name = :name")
	Boolean isDeleted(@Param("name") String uuid);
	
	
	@Query("select i from Image i where i.id = " +
		   "(select distinct im.id from Announcement a " +
		   "join a.images im " +
		   "join a.user u " +
		   "where a.uuid = :aUuid and im.name = :iUuid and u.profileEmail = :profileEmail and u.enabled = true) group by i.id")
	Image findOne(@Param("aUuid") String announcementUuid, @Param("iUuid") String imageUuid, @Param("profileEmail") String profileEmail);
}
