package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.domain.repository.core.JpaInnerRepository;

public interface UserRepositoryInner extends JpaInnerRepository<User, Long> {}
