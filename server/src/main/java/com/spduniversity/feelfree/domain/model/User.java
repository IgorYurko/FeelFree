package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;


@Data
@EqualsAndHashCode(exclude = {"image", "phones", "announcements", "emails", "roles", "rents", "comments", "conversations"})
@ToString(exclude = {"image", "phones", "announcements", "emails", "roles", "rents", "comments", "conversations"})
@DynamicInsert
@DynamicUpdate
@Entity
@Cacheable(false)
@Table(name = "user")
@JsonFilter("userFilter")
public class User implements Serializable {
	
	private static final long serialVersionUID = -6479765365736849688L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "profile_email")
	private String profileEmail;
	
	@Column(name = "password")
	private String password;

	@Column(name = "facebook_id")
	private String facebookId;
	
	@Column(name = "google_id")
	private String googleId;
	
	@Column(name = "create_date")
	private LocalDateTime createDate;
	
	@Column(name = "update_date")
	private LocalDateTime updateDate;
	
	@Column(name = "enabled", columnDefinition = "TINYINT", length = 1)
	private Boolean enabled = Boolean.TRUE;
	
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "image_id", referencedColumnName = "id")
	@JsonManagedReference
	private Image image;

	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "user_phone", joinColumns = @JoinColumn(name = "user_id"),
			   inverseJoinColumns = @JoinColumn(name = "phone_id"))
	@JsonManagedReference
	private Set<Phone> phones = new HashSet<>();
	
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE })
	@JsonBackReference
	private Set<Announcement> announcements = new LinkedHashSet<>();
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "user_email", joinColumns = @JoinColumn(name = "user_id"),
			   inverseJoinColumns = @JoinColumn(name = "email_id"))
	@JsonManagedReference
	private Set<Email> emails = new HashSet<>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"),
			   inverseJoinColumns = @JoinColumn(name = "role_id"))
	@JsonManagedReference
	private Set<Role> roles = new HashSet<>();
	
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@OrderBy("checkInDate ASC")
	@JsonManagedReference
	private Set<Rent> rents = new LinkedHashSet<>();

	@ManyToMany(mappedBy = "favourites", fetch = FetchType.LAZY, cascade = { CascadeType.MERGE,CascadeType.PERSIST, CascadeType.REMOVE})
	@JsonBackReference
	private Set<Announcement> favorites = new LinkedHashSet<>();
	
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@OrderBy("updateDate DESC")
	@JsonBackReference
	private Set<Comment> comments = new LinkedHashSet<>();

	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,  CascadeType.MERGE})
	@OrderBy("messageTime DESC")
	@JsonManagedReference
	@JoinTable(name = "user_conversation",
			joinColumns = {@JoinColumn(name = "user_id")},
			inverseJoinColumns = {@JoinColumn(name = "conversation_id")})
	private Set<Conversation> conversations = new LinkedHashSet<>();
	
	@Transient
	public String getFullName() {
		return (StringUtils.defaultIfBlank(firstName, StringUtils.EMPTY) + StringUtils.SPACE +
				StringUtils.defaultIfBlank(lastName, StringUtils.EMPTY)).trim();
	}
	
	@Transient
	public String getProfileEmailIfBlankFullName() {
		return StringUtils.isBlank(getFullName()) ? profileEmail : getFullName();
	}
	
	@Transient
	public String getProfileEmailIfBlankFirstName() {
		return StringUtils.isBlank(firstName) ? profileEmail : firstName;
	}

	@PrePersist
	private void executePersist() {
		if (createDate == null) {
			createDate = LocalDateTime.now();
		}

		if (updateDate == null) {
			updateDate = LocalDateTime.now();
		}
	}
}
