package com.spduniversity.feelfree.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressAccountDTO {
	private String country;
	private String city;
	private String region;
	private String street;
	private Integer appNumber;
	private String placeId;
}
