package com.spduniversity.feelfree.domain.repository;

public interface ImageCacheRepositoryCustom {
	boolean isExistImage(String name, int width, int height);
}
