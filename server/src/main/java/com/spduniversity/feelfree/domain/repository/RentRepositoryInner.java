package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Rent;
import com.spduniversity.feelfree.domain.repository.core.JpaInnerRepository;

public interface RentRepositoryInner extends JpaInnerRepository<Rent, Long> {}
