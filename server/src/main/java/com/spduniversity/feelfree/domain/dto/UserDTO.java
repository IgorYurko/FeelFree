package com.spduniversity.feelfree.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
	private String firstName;
	private String lastName;
	private String profileEmail;
	private ImageDTO image;
	private Set<EmailDTO> emails;
	private Set<PhoneDTO> phones;
	private boolean google;
	private boolean facebook;
}
