package com.spduniversity.feelfree.domain.dto;

public enum Favorite {
	UNDEFINED,
	TRUE,
	FALSE
}
