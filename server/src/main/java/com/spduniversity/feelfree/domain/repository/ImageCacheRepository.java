package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.dto.LoadImageDTO;
import com.spduniversity.feelfree.domain.model.ImageCache;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface ImageCacheRepository extends JpaRepository<ImageCache, Long>, ImageCacheRepositoryCustom {
	
	@Query("select ic.name as name, ic.value as value, ic.type as type from ImageCache ic " +
		   "where ic.name = :name and ic.width = :width and ic.height = :height")
	LoadImageDTO getCacheImage(@Param("name") String name, @Param("width") int width, @Param("height") int height);
	
	@Query("select ic from ImageCache ic where ic.name = :name and ic.width = :width and ic.height = :height")
	ImageCache findTopByName(@Param("name") String name, @Param("width") int width, @Param("height") int height);
	
	@Modifying
	@Transactional
	@Query("delete from ImageCache ic where ic.name = :name and ic.width = :width and ic.height = :height")
	void delete(@Param("name") String name, @Param("width") int width, @Param("height") int height);
}
