package com.spduniversity.feelfree.domain.mapper;

import com.spduniversity.feelfree.domain.model.TemporaryUser;
import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.service.common.util.HashGeneratorUtil;
import com.spduniversity.feelfree.service.web.bean.UserContainer;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class TemporaryUserMapperHelper {
	
	public <T extends UserContainer> Function<T, TemporaryUser> toTemporaryUser() {
		return container -> {
			TemporaryUser temporaryUser = new TemporaryUser();
			BeanUtils.copyProperties(container, temporaryUser);
			String token = HashGeneratorUtil.generateToken();
			temporaryUser.setToken(token);
			temporaryUser.setPassword(HashGeneratorUtil.generateBCrypt(temporaryUser.getPassword()));
			return temporaryUser;
		};
	}
	
	public Function<TemporaryUser, User> toUser() {
		return temporaryUser -> {
			User user = new User();
			BeanUtils.copyProperties(temporaryUser, user, "id");
			return user;
		};
	}
	
	public Function<TemporaryUser, UserContainer> toUserContainer() {
		return temporaryUser -> new UserContainer() {
			@Getter private final String firstName = temporaryUser.getFirstName();
			@Getter private final String lastName = temporaryUser.getLastName();
			@Getter private final String profileEmail = temporaryUser.getProfileEmail();
			@Getter private final String password = temporaryUser.getPassword();
		};
	}
	
	public Function<TemporaryUser, User> margeUser(User user) {
		return temporaryUser -> {
			if (StringUtils.isBlank(user.getFirstName())) {
				user.setFirstName(temporaryUser.getFirstName());
			}
			if (StringUtils.isBlank(user.getLastName())) {
				user.setLastName(temporaryUser.getLastName());
			}
			if (StringUtils.isBlank(user.getGoogleId())) {
				user.setGoogleId(temporaryUser.getGoogleId());
			}
			if (StringUtils.isBlank(user.getFacebookId())) {
				user.setFacebookId(temporaryUser.getFacebookId());
			}
			if (user.getImage() == null) {
				user.setImage(temporaryUser.getImage());
			}
			return user;
		};
	}
}
