package com.spduniversity.feelfree.domain.mapper;

import com.spduniversity.feelfree.domain.dto.UserDTO;
import com.spduniversity.feelfree.domain.dto.UserDetailsDTO;
import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.service.common.ImageNameService;
import com.spduniversity.feelfree.service.common.extra.EntityMapper;
import com.spduniversity.feelfree.service.web.bean.UserContainer;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.function.Function;

@Component
public class UserMapperHelper {
	
	@Resource
	private EntityMapper objectMapper;
	
	@Resource
	private ImageNameService imageNameService;
	
	public Function<User, UserDetailsDTO> toUserDetailsDTO() {
		return user -> objectMapper.convertEntityValue(user, UserDetailsDTO.class);
	}
	
	public Function<User, UserDTO> toUserDTO() {
		return user -> {
			UserDTO result = objectMapper.convertEntityValue(user, UserDTO.class);
			if (StringUtils.isNotBlank(user.getGoogleId())) {
				result.setGoogle(true);
			}
			if (StringUtils.isNotBlank(user.getFacebookId())) {
				result.setFacebook(true);
			}
			imageNameService.setFullNameIntoImage(result.getImage());
			return result;
		};
	}
	
	public <T extends UserContainer> Function<T, User> toUser() {
		return container -> {
			User user = new User();
			BeanUtils.copyProperties(container, user);
			user.setEnabled(true);
			return user;
		};
	}
	
	public Function<User, UserContainer> toUserContainer() {
		return user -> new UserContainer() {
			@Getter private final String firstName = user.getFirstName();
			@Getter private final String lastName = user.getLastName();
			@Getter private final String profileEmail = user.getProfileEmail();
			@Getter private final String password = user.getPassword();
		};
	}
}
