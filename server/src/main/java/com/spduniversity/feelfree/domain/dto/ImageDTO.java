package com.spduniversity.feelfree.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class ImageDTO {
	
	@Setter
	private String name;
	
	@JsonProperty("value")
	public String getValue() {
		return name;
	}
}
