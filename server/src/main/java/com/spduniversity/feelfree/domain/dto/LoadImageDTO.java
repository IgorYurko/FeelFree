package com.spduniversity.feelfree.domain.dto;

import java.sql.Blob;

public interface LoadImageDTO {
	String getName();
	Blob getValue();
	String getType();
}
