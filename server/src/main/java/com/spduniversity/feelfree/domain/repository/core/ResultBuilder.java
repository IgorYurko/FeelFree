package com.spduniversity.feelfree.domain.repository.core;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public interface ResultBuilder<T, R> {
	ResultBuilder<T, R> whereAnd(@Nullable Specification<T>... where);
	ResultBuilder<T, R> whereAnd(@Nullable List<Specification<T>> where);
	ResultBuilder<T, R> whereOr(@Nullable Specification<T>... where);
	ResultBuilder<T, R> whereOr(@Nullable List<Specification<T>> where);
	ResultBuilder<T, R> sort(@Nullable Sort sort);
	ResultBuilder<T, R> pageable(@Nullable Pageable pageable);
	@Nonnull Page<R> pageResult();
	@Nullable R singleResult();
	@Nonnull List<R> resultList();
}
