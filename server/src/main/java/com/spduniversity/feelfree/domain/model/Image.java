package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.spduniversity.feelfree.service.common.util.HashGeneratorUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(exclude = {"value", "announcement"})
@ToString(exclude = {"value", "announcement"})
@Entity
@Table(name = "image")
@JsonFilter("imageFilter")
public class Image implements Serializable {
	
	private static final long serialVersionUID = -1159141568973859309L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "name", length = 36)
	private String name;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@JsonIgnore
	@Column(name = "value")
	private Blob value;
	
	@Column(name = "type", length = 50)
	private String type;
	
	@Column(name = "upload_date")
	private LocalDateTime uploadDate;
	
	@Column(name = "prime", columnDefinition = "TINYINT", length = 1)
	private Boolean prime = Boolean.FALSE;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinTable(name = "announcement_image", joinColumns = @JoinColumn(name = "image_id"),
			   inverseJoinColumns = @JoinColumn(name = "announcement_id")
	)
	@JsonBackReference
	private Announcement announcement;
	
	@PrePersist
	private void executePersist() {
		if (name == null) {
			name = HashGeneratorUtil.generateUUID();
		}
		
		if (uploadDate == null) {
			uploadDate = LocalDateTime.now();
		}
	}
}
