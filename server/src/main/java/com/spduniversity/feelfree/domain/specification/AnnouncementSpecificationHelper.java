package com.spduniversity.feelfree.domain.specification;

import com.spduniversity.feelfree.domain.model.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;
import java.time.LocalDateTime;
import java.util.Set;

@Component
public class AnnouncementSpecificationHelper {
	
	public Specification<Announcement> equalsUuidAndUserActive(String announcementUuid, String profileEmail) {
		return Specifications.where(equalUuid(announcementUuid))
							 .and(equalProfileEmail(profileEmail))
							 .and(userEnabled());
	}
	
	public Specification<Announcement> equalsUserActive(String profileEmail) {
		return Specifications.where(equalProfileEmail(profileEmail))
							 .and(userEnabled());
	}
	
	public Specification<Announcement> equalId(Long id) {
		return (root, query, cb) -> cb.equal(root.get(Announcement_.id), id);
	}
	
	public Specification<Announcement> equalProfileEmail(String profileEmail) {
		return (root, query, cb) -> {
			Path<User> user = root.get(Announcement_.user);
			return cb.equal(user.get(User_.profileEmail), profileEmail);
		};
	}
	
	public Specification<Announcement> userEnabled() {
		return (root, query, cb) -> {
			Path<User> user = root.get(Announcement_.user);
			return cb.equal(user.get(User_.enabled), true);
		};
	}
	
	public Specification<Announcement> equalUuid(String uuid) {
		return (root, query, cb) -> cb.equal(root.get(Announcement_.uuid), uuid);
	}
	
	public Specification<Announcement> nonHidden() {
		return (root, query, cb) -> cb.equal(root.get(Announcement_.hidden), false);
	}
	
	public Specification<Announcement> likeCountry(String country) {
		return (root, query, cb) -> {
			Path<Address> address = root.get(Announcement_.address);
			return cb.like(cb.lower(address.get(Address_.country)), country.toLowerCase() + "%");
		};
	}

	public Specification<Announcement> havePlaceId(String placeId) {
		return (root, query, cb) -> {
			Path<Address> address = root.get(Announcement_.address);
			return cb.equal(address.get(Address_.placeId), placeId);
		};
	}
	
	public Specification<Announcement> likeCity(String city) {
		return (root, query, cb) -> {
			Path<Address> address = root.get(Announcement_.address);
			return cb.like(cb.lower(address.get(Address_.city)), city.toLowerCase() + "%");
		};
	}
	
	public Specification<Announcement> likeRegion(String region) {
		return (root, query, cb) -> {
			Path<Address> address = root.get(Announcement_.address);
			return cb.like(cb.lower(address.get(Address_.region)), region.toLowerCase() + "%");
		};
	}

	public Specification<Announcement> greaterThanOrEqualPrice(int minPrice) {
		return (root, query, cb) -> {
			Path<Price> price = root.get(Announcement_.price);
			return cb.greaterThanOrEqualTo(price.get(Price_.value), minPrice);
		};
	}
	
	public Specification<Announcement> lessThanOrEqualPrice(int maxPrice) {
		return (root, query, cb) -> {
			Path<Price> price = root.get(Announcement_.price);
			return cb.lessThanOrEqualTo(price.get(Price_.value), maxPrice);
		};
	}
	
	public Specification<Announcement> notReserved(LocalDateTime start, LocalDateTime end) {
		return (root, query, cb) -> {
			Subquery<Long> subquery = query.subquery(Long.class);
			Root<Announcement> announcement = subquery.from(Announcement.class);
			SetJoin<Announcement, Rent> rent = announcement.join(Announcement_.rents, JoinType.INNER);
			Predicate reservedRents = cb.and(
					cb.greaterThanOrEqualTo(rent.get(Rent_.checkOutDate), start),
					cb.lessThanOrEqualTo(rent.get(Rent_.checkInDate), end)
			);
			subquery.select(announcement.get(Announcement_.id)).where(reservedRents);
			return cb.not(root.get(Announcement_.id).in(subquery));
		};
	}
	
	public Specification<Announcement> equalRooms(int rooms) {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.equal(description.get(Description_.roomCount), rooms);
		};
	}
	
	public Specification<Announcement> equalLivingPlaces(int livingPlaces) {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.equal(description.get(Description_.livingPlacesCount), livingPlaces);
		};
	}
	
	public Specification<Announcement> equalPeriod(Price.Period period) {
		return (root, query, cb) -> {
			Path<Price> price = root.get(Announcement_.price);
			return cb.equal(price.get(Price_.period), period);
		};
	}
	
	public Specification<Announcement> notNullImage() {
		return (root, query, cb) -> {
			Expression<Set<Image>> images = root.get(Announcement_.images);
			return cb.isNotEmpty(images);
		};
	}
	
	public Specification<Announcement> trueWifi() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.wiFi));
		};
	}
	
	public Specification<Announcement> trueWashingMachine() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.washingMachine));
		};
	}
	
	public Specification<Announcement> trueRefrigerator() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.refrigerator));
		};
	}
	
	public Specification<Announcement> trueHairDryer() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.hairDryer));
		};
	}
	
	public Specification<Announcement> trueIron() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.iron));
		};
	}
	
	public Specification<Announcement> trueSmoking() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.smoking));
		};
	}
	
	public Specification<Announcement> trueAnimals() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.animals));
		};
	}
	
	public Specification<Announcement> trueKitchenStuff() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.kitchenStuff));
		};
	}
	
	public Specification<Announcement> trueConditioner() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.conditioner));
		};
	}
	
	public Specification<Announcement> trueBalcony() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.balcony));
		};
	}
	
	public Specification<Announcement> trueTv() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.tv));
		};
	}

	public Specification<Announcement> trueEssentials() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.essentials));
		};
	}
	
	public Specification<Announcement> trueShampoo() {
		return (root, query, cb) -> {
			Path<Description> description = root.get(Announcement_.description);
			return cb.isTrue(description.get(Description_.shampoo));
		};
	}
}
