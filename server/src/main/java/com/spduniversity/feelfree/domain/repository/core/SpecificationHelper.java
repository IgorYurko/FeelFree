package com.spduniversity.feelfree.domain.repository.core;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class SpecificationHelper {
	
	@SafeVarargs
	public final <T> Specification<T> whereAnd(Specification<T>... specifications) {
		Specification<T>[] notNullSpecifications = getNotNullArray(specifications);
		return getSpecifications(notNullSpecifications, CompositionType.AND);
	}
	
	public <T> Specification<T> whereAnd(List<Specification<T>> specifications) {
		List<Specification<T>> notNullSpecifications = getNotNullList(specifications);
		return getSpecifications(notNullSpecifications, CompositionType.AND);
	}
	
	private <T> List<Specification<T>> getNotNullList(List<Specification<T>> specifications) {
		return specifications.stream()
							 .filter(Objects::nonNull)
							 .collect(Collectors.toList());
	}
	
	@SuppressWarnings("unchecked")
	private <T> Specification<T>[] getNotNullArray(Specification<T>[] specifications) {
		return Arrays.stream(specifications)
					 .filter(Objects::nonNull)
					 .toArray(Specification[]::new);
	}
	
	@SafeVarargs
	public final <T> Specification<T> whereOr(Specification<T>... specifications) {
		Specification<T>[] notNullSpecifications = getNotNullArray(specifications);
		return getSpecifications(notNullSpecifications, CompositionType.OR);
	}
	
	public <T> Specification<T> whereOr(List<Specification<T>> specifications) {
		List<Specification<T>> notNullSpecifications = getNotNullList(specifications);
		return getSpecifications(notNullSpecifications, CompositionType.OR);
	}
	
	private <T> Specifications<T> getSpecifications(List<Specification<T>> notNullSpecifications, CompositionType type) {
		Specifications<T> where = Specifications.where(null);
		for (Specification<T> specification : notNullSpecifications) {
			where = type.execute(where, specification);
		}
		return where;
	}
	
	private <T> Specifications<T> getSpecifications(Specification<T>[] notNullSpecifications, CompositionType type) {
		Specifications<T> where = Specifications.where(null);
		for (Specification<T> specification : notNullSpecifications) {
			where = type.execute(where, specification);
		}
		return where;
	}
	
	enum CompositionType {
		AND {
			@Override
			<T> Specifications<T> execute(Specifications<T> where, Specification<T> specifications) {
				return where.and(specifications);
			}
		}, OR {
			@Override
			<T> Specifications<T> execute(Specifications<T> where, Specification<T> specifications) {
				return where.or(specifications);
			}
		};
		
		abstract <T> Specifications<T> execute(Specifications<T> where, Specification<T> specifications);
	}
}
