package com.spduniversity.feelfree.domain.mapper;

import com.spduniversity.feelfree.domain.dto.ImageDTO;
import com.spduniversity.feelfree.domain.dto.ImageUuidPrimeDTO;
import com.spduniversity.feelfree.domain.model.Image;
import com.spduniversity.feelfree.service.common.BlobService;
import com.spduniversity.feelfree.service.common.ImageNameService;
import com.spduniversity.feelfree.service.common.extra.EntityMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ImageMapperHelper {
	
	@Resource
	private BlobService blobService;
	
	@Resource
	private EntityMapper objectMapper;
	
	@Resource
	private ImageNameService imageNameService;
	
	public Function<MultipartFile, Image> toImage() {
		return this::createImageData;
	}
	
	public Function<Image, ImageDTO> toImageDTO() {
		return image -> objectMapper.convertEntityValue(image, ImageDTO.class);
	}
	
	public Image createImageData(MultipartFile multipartFile) {
		Image image = new Image();
		setImageData(multipartFile, image);
		return image;
	}
	
	private void setImageData(MultipartFile multipartFile, Image image) {
		image.setType(multipartFile.getContentType());
		try {
			image.setValue(blobService.createBlob(multipartFile.getBytes()));
		} catch (IOException ex) {
			log.error(ex.getMessage(), ex);
		}
	}
	
	public Function<Set<Image>, Set<ImageUuidPrimeDTO>>  toImageUuidDTO() {
		return image -> image.stream()
							 .map(e -> new ImageUuidPrimeDTO(e.getName(), imageNameService.getFullImageName(e.getName()), e.getPrime()))
							 .collect(Collectors.toCollection(LinkedHashSet::new));
	}
}
