package com.spduniversity.feelfree.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
public class ImageUUIDDTO {
	
	@Getter
	@Setter
	private String uuid;
	
	@Setter
	private String name;
	
	@JsonProperty("value")
	public String getValue() {
		return name;
	}
}
