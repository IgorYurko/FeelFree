package com.spduniversity.feelfree.domain.dto;

import com.spduniversity.feelfree.domain.model.Rent;
import com.spduniversity.feelfree.domain.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(exclude = {"userFrom", "userTo"})
@ToString(exclude = {"userFrom", "userTo"})
public class EmailNotificationDTO {

    private User userFrom;

    private User userTo;

    private String title;

    private String text;

    private Rent rent;

}
