package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(exclude = {"value"})
@ToString(exclude = {"value"})
@Entity
@Table(name = "image_cache")
@JsonFilter("imageCacheFilter")
public class ImageCache implements Serializable {
	
	private static final long serialVersionUID = 856870898233597968L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@JsonIgnore
	@Column(name = "value")
	private Blob value;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "height")
	private Integer height;
	
	@Column(name = "width")
	private Integer width;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "upload_date")
	private LocalDateTime uploadDate;
	
	@PrePersist
	private void executePersist() {
		if (uploadDate == null) {
			uploadDate = LocalDateTime.now();
		}
	}
}
