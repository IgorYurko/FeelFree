package com.spduniversity.feelfree.domain.repository.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public abstract class AbstractInnerRepository<T extends Serializable, ID extends Serializable> implements JpaSpecificationExecutorMapper<T> {
	
	private DTOMapper dtoMapper;
	private SpecificationHelper specificationHelper;
	private JpaInnerRepository<T, ID> innerRepository;
	
	@Autowired
	private void setInnerRepository(JpaInnerRepository<T, ID> innerRepository) {
		this.innerRepository = innerRepository;
	}
	
	@Autowired
	private void setDtoMapper(DTOMapper dtoMapper) {
		this.dtoMapper = dtoMapper;
	}
	
	@Autowired
	private void setSpecificationHelper(SpecificationHelper specificationHelper) {
		this.specificationHelper = specificationHelper;
	}
	
	public <R> ResultBuilderImpl<R> resultBuilder(Function<T, R> mapper) {
		return new ResultBuilderImpl<>(mapper);
	}

	@Override
	@Nullable
	public T findOne(Specification<T> where){
		return innerRepository.findOne(where);
	}
	
	@Override
	public List<T> findAll(Specification<T> where) {
		return innerRepository.findAll(where);
	}
	
	@Override
	public Page<T> findAll(Specification<T> where, Pageable pageable) {
		return innerRepository.findAll(where, pageable);
	}
	
	@Override
	public List<T> findAll(Specification<T> where, Sort sort) {
		return innerRepository.findAll(where, sort);
	}
	
	@Override
	public long count(Specification<T> where) {
		return innerRepository.count(where);
	}
	
	public class ResultBuilderImpl<R> implements ResultBuilder<T, R> {
		
		private Specifications<T> where = Specifications.where(null);
		private final Function<T, R> mapper;
		private Pageable pageable;
		private Sort sort;
		
		private ResultBuilderImpl(Function<T, R> mapper) {
			this.mapper = mapper;
		}
		
		@SafeVarargs
		public final ResultBuilderImpl<R> whereAnd(@Nullable Specification<T>... where) {
			if (where != null) {
				this.where = this.where.and(specificationHelper.whereAnd(where));
			}
			return this;
		}
		
		public ResultBuilderImpl<R> whereAnd(@Nullable List<Specification<T>> where) {
			if (where != null) {
				this.where = this.where.and(specificationHelper.whereAnd(where));
			}
			return this;
		}
		
		@SafeVarargs
		public final ResultBuilderImpl<R> whereOr(@Nullable Specification<T>... where) {
			if (where != null) {
				this.where = this.where.or(specificationHelper.whereOr(where));
			}
			return this;
		}
		
		@Override
		public ResultBuilder<T, R> whereOr(@Nullable List<Specification<T>> where) {
			if (where != null) {
				this.where = this.where.or(specificationHelper.whereOr(where));
			}
			return this;
		}
		
		@Override
		public ResultBuilderImpl<R> sort(@Nullable Sort sort) {
			Optional.ofNullable(sort)
					.ifPresent(e -> this.sort = e);
			return this;
		}
		
		@Override
		public ResultBuilderImpl<R> pageable(@Nullable Pageable pageable) {
			Optional.ofNullable(pageable)
					.ifPresent(e -> this.pageable = e);
			return this;
		}
		
		@Nonnull
		@Override
		public Page<R> pageResult() {
			return dtoMapper.mapToPage(innerRepository.findAll(where, pageable), mapper);
		}
		
		@Nullable
		@Override
		public R singleResult() {
			return dtoMapper.mapToObject(innerRepository.findOne(where), mapper);
		}
		
		@Nonnull
		@Override
		public List<R> resultList() {
			return dtoMapper.mapToObjectList(innerRepository.findAll(where, sort), mapper);
		}
	}
}
