package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Announcement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface AnnouncementRepository extends JpaRepository<Announcement, Long>, AnnouncementRepositoryCustom {
	
	@Modifying
	@Transactional
	@Query("delete from Announcement a where a.uuid = :uuid")
	void delete(@Param("uuid") String uuid);
	
	@Query("select new java.lang.Boolean(count(a.id) = 0) from Announcement a where a.uuid = :uuid")
	Boolean isDeleted(@Param("uuid") String uuid);
}
