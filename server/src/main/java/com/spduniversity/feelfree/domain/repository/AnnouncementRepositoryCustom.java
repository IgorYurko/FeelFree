package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.dto.GeolocationSearchDTO;
import com.spduniversity.feelfree.domain.model.Announcement;
import com.spduniversity.feelfree.domain.repository.core.JpaSpecificationExecutorMapper;
import com.spduniversity.feelfree.service.domain.bean.GeolocationContainer;
import org.springframework.data.domain.Page;

import javax.annotation.Nonnull;

public interface AnnouncementRepositoryCustom extends JpaSpecificationExecutorMapper<Announcement> {
	
	Page<GeolocationSearchDTO> findGeolocationInfo(@Nonnull GeolocationContainer searchGeolocationBean,
												   @Nonnull Integer offSet,
												   @Nonnull Integer rows);
}
