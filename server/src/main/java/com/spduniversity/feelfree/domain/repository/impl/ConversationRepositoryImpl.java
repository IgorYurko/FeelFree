package com.spduniversity.feelfree.domain.repository.impl;

import com.spduniversity.feelfree.domain.model.Conversation;
import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.domain.repository.ConversationRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.List;

public class ConversationRepositoryImpl implements ConversationRepositoryCustom {

    @PersistenceContext(unitName = "entityManagerFactory")
    private EntityManager entityManager;

    @Override
    @SuppressWarnings("unchecked")
    public Conversation findConversationByUsers(User user1, User user2) {

        String convQuery = "select conversation_id from user_conversation where user_id = :fid " +
                "and conversation_id IN (select conversation_id from user_conversation where user_id = :sid)";
        List<BigInteger> ids = (List<BigInteger>) entityManager.createNativeQuery(convQuery)
                .setParameter("fid", user1.getId())
                .setParameter("sid", user2.getId())
                .getResultList();

        if (ids.size() == 0) return null;

        return (Conversation) entityManager
                .createQuery("select u from Conversation u where u.id = :id")
                .setParameter("id", ids.iterator().next().longValue())
                .getSingleResult();
    }
}
