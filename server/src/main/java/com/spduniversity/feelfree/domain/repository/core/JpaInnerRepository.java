package com.spduniversity.feelfree.domain.repository.core;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface JpaInnerRepository<T, ID extends Serializable> extends CrudRepository<T, ID>, JpaSpecificationExecutor<T> {}
