package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

import static com.spduniversity.feelfree.service.common.extra.VariablesContainer.EMAIL_PATTERN;

@Data
@NoArgsConstructor
@Entity
@Table(name = "email")
@JsonFilter("emailFilter")
public class Email implements Serializable {
	
	private static final long serialVersionUID = 7068739640501899499L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Pattern(regexp = EMAIL_PATTERN, message = "{annotation.parameters.email.invalid}")
	@Column(name = "value", length = 150)
	private String value;
}
