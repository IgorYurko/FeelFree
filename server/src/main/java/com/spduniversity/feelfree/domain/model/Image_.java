package com.spduniversity.feelfree.domain.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Blob;
import java.time.LocalDateTime;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Image.class)
public abstract class Image_ {

	public static volatile SingularAttribute<Image, LocalDateTime> uploadDate;
	public static volatile SingularAttribute<Image, String> name;
	public static volatile SingularAttribute<Image, Long> id;
	public static volatile SingularAttribute<Image, String> type;
	public static volatile SingularAttribute<Image, Blob> value;
	public static volatile SingularAttribute<Image, Announcement> announcement;

}

