package com.spduniversity.feelfree.domain.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.time.LocalDateTime;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Announcement.class)
public abstract class Announcement_ {

	public static volatile SingularAttribute<Announcement, LocalDateTime> updateDate;
	public static volatile SetAttribute<Announcement, Image> images;
	public static volatile SingularAttribute<Announcement, Address> address;
	public static volatile SetAttribute<Announcement, Comment> comments;
	public static volatile SingularAttribute<Announcement, Boolean> hidden;
	public static volatile SingularAttribute<Announcement, Description> description;
	public static volatile SingularAttribute<Announcement, String> shortDescription;
	public static volatile SingularAttribute<Announcement, String> title;
	public static volatile SetAttribute<Announcement, Rent> rents;
	public static volatile SingularAttribute<Announcement, String> uuid;
	public static volatile SingularAttribute<Announcement, Price> price;
	public static volatile SingularAttribute<Announcement, Long> id;
	public static volatile SingularAttribute<Announcement, User> user;
	public static volatile SingularAttribute<Announcement, LocalDateTime> createDate;
	public static volatile SingularAttribute<Announcement, Geolocation> geolocation;

}

