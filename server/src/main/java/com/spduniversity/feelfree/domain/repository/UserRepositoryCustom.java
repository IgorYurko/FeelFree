package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.domain.repository.core.JpaSpecificationExecutorMapper;

public interface UserRepositoryCustom extends JpaSpecificationExecutorMapper<User> {
	
}
