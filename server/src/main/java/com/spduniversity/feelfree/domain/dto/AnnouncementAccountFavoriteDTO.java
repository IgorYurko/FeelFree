package com.spduniversity.feelfree.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashSet;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnnouncementAccountFavoriteDTO {
	private String uuid;
	private String title;
	private String favorite;
	private String shortDescription;
	private AddressDTO address;
	private PriceDTO price;
	private LinkedHashSet<ImageDTO> images;

	public String getFavorite(){
		return "TRUE";
	}
}
