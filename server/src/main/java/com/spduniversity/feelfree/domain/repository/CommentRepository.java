package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long>, CommentRepositoryCustom {}
