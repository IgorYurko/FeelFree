package com.spduniversity.feelfree.domain.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.time.LocalDateTime;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Conversation.class)
public abstract class Conversation_ {

	public static volatile SingularAttribute<Conversation, LocalDateTime> messageTime;
	public static volatile SetAttribute<Conversation, Message> messages;
	public static volatile SingularAttribute<Conversation, Long> id;
	public static volatile SetAttribute<Conversation, User> conversationUsers;

}

