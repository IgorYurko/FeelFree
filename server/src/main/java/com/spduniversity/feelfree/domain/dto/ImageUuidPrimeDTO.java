package com.spduniversity.feelfree.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class ImageUuidPrimeDTO {
	
	@Getter
	@Setter
	private String uuid;
	
	@Setter
	private String name;
	
	@Getter
	@Setter
	private Boolean prime;
	
	@JsonProperty("value")
	public String getValue() {
		return name;
	}
}
