package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@EqualsAndHashCode(exclude = "announcements")
@ToString(exclude = "announcements")
@Entity
@Table(name = "address")
@JsonFilter("addressFilter")
public class Address implements Serializable {
	
	private static final long serialVersionUID = 3204531670716284161L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "country", nullable = false, length = 200)
	private String country;
	
	@Column(name = "city", nullable = false, length = 200)
	private String city;
	
	@Column(name = "region", nullable = false, length = 200)
	private String region;
	
	@Column(name = "street", nullable = false, length = 200)
	private String street;
	
	@Column(name = "app_number", nullable = false)
	private Integer appNumber;
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "address")
	@JsonBackReference
	private Announcement announcements;

	@Column(name = "place_id", nullable = false, length = 200)
	private String placeId;
}
