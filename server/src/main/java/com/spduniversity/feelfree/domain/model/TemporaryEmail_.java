package com.spduniversity.feelfree.domain.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.time.LocalDateTime;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TemporaryEmail.class)
public abstract class TemporaryEmail_ {

	public static volatile SingularAttribute<TemporaryEmail, LocalDateTime> expiryDate;
	public static volatile SingularAttribute<TemporaryEmail, String> id;
	public static volatile SingularAttribute<TemporaryEmail, String> value;
	public static volatile SingularAttribute<TemporaryEmail, User> user;
	public static volatile SingularAttribute<TemporaryEmail, String> token;

}

