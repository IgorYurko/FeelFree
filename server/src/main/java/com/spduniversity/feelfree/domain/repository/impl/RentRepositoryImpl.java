package com.spduniversity.feelfree.domain.repository.impl;

import com.spduniversity.feelfree.domain.model.Rent;
import com.spduniversity.feelfree.domain.repository.RentRepositoryCustom;
import com.spduniversity.feelfree.domain.repository.core.AbstractInnerRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class RentRepositoryImpl extends AbstractInnerRepository<Rent, Long> implements RentRepositoryCustom {}
