package com.spduniversity.feelfree.domain.repository.core;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class DTOMapper {
	
	public <T, R> R mapToObject(T source, Function<T, R> target) {
		return Optional.ofNullable(source)
					   .map(target)
					   .orElse(null);
	}
	
	public <T, R> List<R> mapToObjectList(List<T> source, Function<T, R> target) {
		return source.stream()
					 .map(target)
					 .collect(Collectors.toList());
	}
	
	public <T, R> Page<R> mapToPage(Page<T> source, Function<T, R> target) {
		return source.map(target::apply);
	}
}
