package com.spduniversity.feelfree.domain.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Geolocation.class)
public abstract class Geolocation_ {

	public static volatile SingularAttribute<Geolocation, Double> latitude;
	public static volatile SingularAttribute<Geolocation, Long> id;
	public static volatile SingularAttribute<Geolocation, Double> longitude;
	public static volatile SingularAttribute<Geolocation, Announcement> announcement;

}

