package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Announcement;
import com.spduniversity.feelfree.domain.repository.core.JpaInnerRepository;

public interface AnnouncementRepositoryInner extends JpaInnerRepository<Announcement, Long> {}

