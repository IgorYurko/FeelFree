package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@EqualsAndHashCode(exclude = {"user", "conversation","title"})
@ToString(exclude = {"user", "conversation","title"})
@Table(name = "message")
@JsonFilter("messageFilter")
public class Message implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "conversation_id")
    private Conversation conversation;

    @Column(name = "created_at")
    private LocalDateTime createDate;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    @JsonBackReference
    private User user;


    @OneToOne(targetEntity = MessageTitle.class, fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "title_id")
    @JsonManagedReference
    private MessageTitle title;

    @Column(name = "line_text")
    private String text;

    @PrePersist
    private void executePersist() {
        if (createDate == null) {
            createDate = LocalDateTime.now();
        }


    }

}
