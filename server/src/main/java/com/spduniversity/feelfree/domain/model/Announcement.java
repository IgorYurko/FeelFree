package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.spduniversity.feelfree.domain.dto.GeolocationSearchDTO;
import com.spduniversity.feelfree.service.common.util.HashGeneratorUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
@EqualsAndHashCode(exclude = {"price", "geolocation", "address", "user", "description", "images", "comments", "rents", "favourites"})
@ToString(exclude = {"price", "geolocation", "address", "user", "description", "images", "comments", "rents", "favourites"})
@Entity
@DynamicInsert
@DynamicUpdate
@Cacheable(false)
@Table(name = "announcement")
@SqlResultSetMappings({
		@SqlResultSetMapping(
				name = "SearchGeolocationDTOMapper",
				classes = @ConstructorResult(
						targetClass = GeolocationSearchDTO.class,
						columns = {
								@ColumnResult(name = "uuid", type = String.class),
								@ColumnResult(name = "price", type = Double.class),
								@ColumnResult(name = "latitude", type = Double.class),
								@ColumnResult(name = "longitude", type = Double.class)
						}
				)
		)
})
@JsonFilter("announcementFilter")
public class Announcement implements Serializable {
	
	private static final long serialVersionUID = 4067018820618520201L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "uuid")
	private String uuid;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "short_description")
	private String shortDescription;
	
	@Column(name = "hidden", columnDefinition = "TINYINT", length = 1)
	private Boolean hidden = Boolean.FALSE;
	
	@Column(name = "create_date")
	private LocalDateTime createDate;
	
	@Column(name = "update_date")
	private LocalDateTime updateDate;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name = "geolocation_id", referencedColumnName = "id")
	@JsonManagedReference
	private Geolocation geolocation;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name = "description_id", referencedColumnName = "id")
	@JsonManagedReference
	private Description description;

	@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name = "address_id", referencedColumnName = "id")
	@JsonManagedReference
	private Address address;

	@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name = "price_id", referencedColumnName = "id")
	@JsonManagedReference
	private Price price;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "announcement", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@OrderBy("prime DESC")
	@JsonManagedReference
	private Set<Image> images = new LinkedHashSet<>();

	@OneToMany(mappedBy =  "announcement",fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@OrderBy("updateDate DESC")
	@JsonManagedReference
	private Set<Comment> comments = new LinkedHashSet<>();

	@OneToMany(mappedBy = "announcement", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@OrderBy("checkInDate ASC")
	@JsonManagedReference
	private Set<Rent> rents = new LinkedHashSet<>();
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "user_announcement", joinColumns = @JoinColumn(name = "announcement_id"),
			   inverseJoinColumns = @JoinColumn(name = "user_id")
	)
	@JsonManagedReference
	private User user;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "favorite", joinColumns = @JoinColumn(name = "announcement_id"),
			   inverseJoinColumns = @JoinColumn(name = "user_id"))
	@JsonManagedReference
	private Set<User> favourites = new LinkedHashSet<>();
	
	@PrePersist
	private void executePersist() {
		uuid = HashGeneratorUtil.generateUUID();
		
		if (createDate == null) {
			createDate = LocalDateTime.now();
		}
		
		if (updateDate == null) {
			updateDate = LocalDateTime.now();
		}
	}
	
	@PreUpdate
	private void executeUpdate() {
		updateDate = LocalDateTime.now();
	}
}
