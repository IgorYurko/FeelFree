package com.spduniversity.feelfree.domain.model;

import com.spduniversity.feelfree.domain.model.Price.Period;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Price.class)
public abstract class Price_ {

	public static volatile SingularAttribute<Price, Period> period;
	public static volatile SingularAttribute<Price, Long> id;
	public static volatile SingularAttribute<Price, Integer> value;
	public static volatile SingularAttribute<Price, Announcement> announcement;

}

