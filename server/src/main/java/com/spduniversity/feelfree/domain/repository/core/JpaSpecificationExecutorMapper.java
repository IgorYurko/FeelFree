package com.spduniversity.feelfree.domain.repository.core;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.NoRepositoryBean;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.List;
import java.util.function.Function;

@NoRepositoryBean
public interface JpaSpecificationExecutorMapper<T extends Serializable> {
	<R> ResultBuilder<T, R> resultBuilder(Function<T, R> mapper);
	@Nullable T findOne(Specification<T> where);
	@Nullable List<T> findAll(Specification<T> where);
	@Nullable Page<T> findAll(Specification<T> where, Pageable pageable);
	@Nullable List<T> findAll(Specification<T> where, Sort sort);
	long count(Specification<T> where);
}
