SET foreign_key_checks = 0;
CREATE TABLE `user_conversation` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`conversation_id` BIGINT(20) UNSIGNED NOT NULL,
	`user_id` BIGINT(20) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_user_conversation_conversation` (`conversation_id`),
	INDEX `FK_user_conversation_user` (`user_id`),
	CONSTRAINT `FK_user_conversation_conversation` FOREIGN KEY (`conversation_id`) REFERENCES `conversation` (`id`),
	CONSTRAINT `FK_user_conversation_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=2
;


CREATE TABLE `message` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`conversation_id` BIGINT(20) UNSIGNED NOT NULL,
	`user_id` BIGINT(20) UNSIGNED NOT NULL,
	`title` VARCHAR(170) NOT NULL,
	`line_text` TEXT NOT NULL,
	`created_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `fk_message_line_chat` (`conversation_id`),
	INDEX `fk_message_line_chat_user1` (`user_id`),
	CONSTRAINT `fk_chat_line_chat` FOREIGN KEY (`conversation_id`) REFERENCES `conversation` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `fk_chat_line_chat_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `conversation` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`new_msg_time` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
INSERT INTO `message` (`id`, `conversation_id`, `user_id`, `title`, `line_text`, `created_at`) VALUES (1, 1, 6, 'Hello from Demian', 'Hello its Demian', '2017-04-10 18:24:29');
INSERT INTO `message` (`id`, `conversation_id`, `user_id`, `title`, `line_text`, `created_at`) VALUES (2, 1, 8, 'Respond From admin', 'Hello Demian, its Admin answering to you!', '2017-04-10 18:25:03');
INSERT INTO `conversation` (`id`, `new_msg_time`) VALUES (1,'2017-04-10 18:18:18');
INSERT INTO `conversation` (`id`, `new_msg_time`) VALUES (2, '2017-04-10 18:19:12');
INSERT INTO `conversation` (`id`, `new_msg_time`) VALUES (3, '2017-04-10 18:19:21');
INSERT INTO `user_conversation` (`id`, `conversation_id`, `user_id`) VALUES (1, 1, 8);
INSERT INTO `user_conversation` (`id`, `conversation_id`, `user_id`) VALUES (2, 1, 6);

SET foreign_key_checks = 1;