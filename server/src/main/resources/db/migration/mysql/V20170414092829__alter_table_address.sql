ALTER TABLE `address` CHANGE COLUMN `app_number` `app_number` INT(5) NULL AFTER `street`;
ALTER TABLE `address`
  ADD INDEX `address_all` (`country`, `city`, `region`, `street`, `app_number`);