CREATE TABLE `temporary_email` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`value` VARCHAR(254) NOT NULL,
	`user_id` BIGINT(20) UNSIGNED NOT NULL,
	`token` VARCHAR(50) NOT NULL,
	`expiry` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_temporary_email_user` (`user_id`),
	CONSTRAINT `FK_temporary_email_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;