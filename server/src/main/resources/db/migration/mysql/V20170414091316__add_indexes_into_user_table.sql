ALTER TABLE `user` ADD UNIQUE INDEX `profile_email` (`profile_email`);
ALTER TABLE `user` ADD UNIQUE INDEX `facebook_id` (`facebook_id`);
ALTER TABLE `user` ADD UNIQUE INDEX `google_id` (`google_id`);

ALTER TABLE `user`
  ADD INDEX `profile_email_facebook_google_enabled` (`profile_email`, `facebook_id`, `google_id`, `enabled`);