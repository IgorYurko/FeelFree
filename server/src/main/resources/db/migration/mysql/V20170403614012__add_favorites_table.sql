CREATE TABLE `favorite` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` BIGINT(20) UNSIGNED NOT NULL,
	`announcement_id` BIGINT(20) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_favorites_user` (`user_id`),
	INDEX `FK_favorites_announcement` (`announcement_id`),
	CONSTRAINT `FK_favorites_announcement` FOREIGN KEY (`announcement_id`) REFERENCES `announcement` (`id`),
	CONSTRAINT `FK_favorites_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;