ALTER TABLE `temporary_user`
	ALTER `firstName` DROP DEFAULT,
	ALTER `lastName` DROP DEFAULT;
ALTER TABLE `temporary_user`
	CHANGE COLUMN `firstName` `firstName` VARCHAR(50) NULL AFTER `profileEmail`,
	CHANGE COLUMN `lastName` `lastName` VARCHAR(50) NULL AFTER `firstName`;
