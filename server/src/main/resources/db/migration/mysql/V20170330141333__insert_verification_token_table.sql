CREATE TABLE IF NOT EXISTS `verification_token`  (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`token` VARCHAR(20) NOT NULL,
	`email` varchar(254) NOT NULL,
	`user_id` BIGINT(20) UNSIGNED NOT NULL,
	`expiry` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_verification_token_user` (`user_id`),
	CONSTRAINT `FK_verification_token_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;