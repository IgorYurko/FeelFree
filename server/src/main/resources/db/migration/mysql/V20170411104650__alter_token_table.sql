ALTER TABLE `verification_token`
	ALTER `email` DROP DEFAULT;
ALTER TABLE `verification_token`
	CHANGE COLUMN `email` `profileEmail` VARCHAR(254) NOT NULL AFTER `token`,
	ADD COLUMN `firstName` VARCHAR(50) NOT NULL AFTER `profileEmail`,
	ADD COLUMN `lastName` VARCHAR(50) NOT NULL AFTER `firstName`,
	ADD COLUMN `password` VARCHAR(60) NOT NULL AFTER `lastName`;
ALTER TABLE `verification_token`
	DROP COLUMN `user_id`,
	DROP INDEX `FK_verification_token_user`,
	DROP FOREIGN KEY `FK_verification_token_user`;
	RENAME TABLE `verification_token` TO `temporary_user`;
