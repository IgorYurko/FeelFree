ALTER TABLE `address` ALTER `region` DROP DEFAULT;
ALTER TABLE `address` CHANGE COLUMN `region` `region` VARCHAR(200) NULL AFTER `city`;