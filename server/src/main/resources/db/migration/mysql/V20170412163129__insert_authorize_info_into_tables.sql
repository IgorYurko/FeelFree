INSERT INTO `authorize` (`id`, `unique_name`, `target`) VALUES
	(1, '6315c251f88ce91ed01f7e91a5bfdeda', 'FEEL_FREE'),
	(3, '3e61cb5877cf1a45b6f40eae7fcad0c1', 'FEEL_FREE'),
	(5, '6c12533ac8c53b27cf5874d8ddb7c93d', 'FEEL_FREE'),
	(6, '21120d99713d45c123734b62c462ec7e', 'FEEL_FREE'),
	(7, '285b9138ecdfff718deda9ed40082092', 'FEEL_FREE'),
	(8, 'a744ec0194174fa06afd55e7e85a332a', 'FEEL_FREE'),
	(9, '07f01331e4ed8d034989208421ff02bc', 'FEEL_FREE'),
	(10, '31a880041fd0ee60865ef922352326d2', 'FEEL_FREE'),
	(11, '7e24a2a9e95831bee5daaf55ee717281', 'FACEBOOK'),
	(12, '6a779b7efb857b92931dfe6aaf9cdd25', 'FEEL_FREE');

INSERT INTO `user` (`id`, `first_name`, `last_name`, `profile_email`, `password`, `image_id`, `enabled`, `create_date`, `update_date`) VALUES
	(9, 'Igor', 'Social', 'igor.yurko.mail@gmail.com', '$2a$06$gkfGA3YHwI3uFPGQ3VSiSuZjQFxkXWtK17E.lUnbbLr9bwr8G78yC', 1, 1, '2017-04-12 12:11:37', '2017-04-12 12:11:38');

INSERT INTO `user_authorize` (`id`, `user_id`, `authorize_id`) VALUES
	(1, 1, 1),
	(2, 2, 3),
	(3, 3, 5),
	(4, 4, 6),
	(5, 5, 7),
	(6, 6, 8),
	(7, 7, 9),
	(8, 8, 10),
	(9, 9, 11),
	(10, 9, 12);

