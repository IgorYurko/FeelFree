ALTER TABLE `user` ADD COLUMN `facebook_id` VARCHAR(255) NULL AFTER `enabled`;
ALTER TABLE `user` ADD COLUMN `google_id` VARCHAR(255) NULL AFTER `facebook_id`;