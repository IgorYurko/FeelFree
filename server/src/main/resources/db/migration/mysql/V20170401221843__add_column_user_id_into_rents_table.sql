ALTER TABLE `rent` ADD COLUMN `user_id`  BIGINT UNSIGNED NOT NULL AFTER `check_out_date`;
UPDATE `feel_free`.`rent` SET `user_id`='1' WHERE  `id`=1;
UPDATE `feel_free`.`rent` SET `user_id`='1' WHERE  `id`=2;
UPDATE `feel_free`.`rent` SET `user_id`='2' WHERE  `id`=3;
ALTER TABLE `rent` ADD CONSTRAINT `FK_rent_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);