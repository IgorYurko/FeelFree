ALTER TABLE `image` ADD COLUMN `primary` TINYINT(1) NULL DEFAULT '0' AFTER `upload_date`;
ALTER TABLE `image` ALTER `primary` DROP DEFAULT;
ALTER TABLE `image` CHANGE COLUMN `primary` `primary` TINYINT(1) UNSIGNED NOT NULL AFTER `upload_date`;