ALTER TABLE `rent` ADD COLUMN `status` ENUM('CONFIRMED','REJECTED','WAITING') NULL AFTER `user_id`;
UPDATE `feel_free`.`rent` SET `status`='CONFIRMED' WHERE  `id`=1;
UPDATE `feel_free`.`rent` SET `status`='CONFIRMED' WHERE  `id`=2;
UPDATE `feel_free`.`rent` SET `status`='REJECTED' WHERE  `id`=3;

ALTER TABLE `rent` ALTER `status` DROP DEFAULT;
ALTER TABLE `rent` CHANGE COLUMN `status` `status` ENUM('CONFIRMED','REJECTED','WAITING') NOT NULL AFTER `user_id`;