ALTER TABLE `authorize` ALTER `target` DROP DEFAULT;
ALTER TABLE `authorize` CHANGE COLUMN `target` `target` ENUM('FEEL_FREE','FACEBOOK','TWITTER','GOOGLE') NOT NULL AFTER `unique_name`;