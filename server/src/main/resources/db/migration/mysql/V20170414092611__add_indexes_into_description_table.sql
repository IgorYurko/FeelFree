ALTER TABLE `description`
	ADD INDEX `description_all` (`room_count`,
															 `living_places_count`,
															 `wi_fi`,
															 `washing_machine`,
															 `refrigerator`,
															 `hair_dryer`,
															 `iron`,
															 `smoking`,
															 `animals`,
															 `kitchen_stuff`,
															 `conditioner`,
															 `balcony`,
															 `tv`,
															 `essentials`,
															 `shampoo`);