CREATE SEQUENCE address_seq;

CREATE TABLE IF NOT EXISTS address (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('address_seq'),
  country varchar(200) NOT NULL,
  city varchar(200) NOT NULL,
  region varchar(200) NOT NULL,
  street varchar(200) NOT NULL,
  app_number int NOT NULL,
  PRIMARY KEY (id)
) ;

CREATE SEQUENCE description_seq;

CREATE TABLE IF NOT EXISTS description (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('description_seq'),
  room_count smallint check (room_count > 0) DEFAULT NULL,
  living_places_count smallint check (living_places_count > 0) DEFAULT NULL,
  wi_fi smallint check (wi_fi > 0) NOT NULL,
  washing_machine smallint check (washing_machine > 0) NOT NULL,
  refrigerator smallint check (refrigerator > 0) NOT NULL,
  hair_dryer smallint check (hair_dryer > 0) NOT NULL,
  iron smallint check (iron > 0) NOT NULL,
  smoking smallint check (smoking > 0) NOT NULL,
  animals smallint check (animals > 0) NOT NULL,
  kitchen_stuff smallint check (kitchen_stuff > 0) NOT NULL,
  conditioner smallint check (conditioner > 0) NOT NULL,
  balcony smallint check (balcony > 0) NOT NULL,
  dishes smallint check (dishes > 0) NOT NULL,
  PRIMARY KEY (id)
) ;

CREATE SEQUENCE geolocation_seq;

CREATE TABLE IF NOT EXISTS geolocation (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('geolocation_seq'),
  latitude double precision NOT NULL,
  longitude double precision NOT NULL,
  PRIMARY KEY (id)
) ;

CREATE INDEX latitude ON geolocation (latitude);
CREATE INDEX longitude ON geolocation (longitude);

CREATE SEQUENCE price_seq;

CREATE TABLE IF NOT EXISTS price (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('price_seq'),
  value decimal(7,2) NOT NULL,
  period enum('DAY','MONTH') NOT NULL,
  PRIMARY KEY (id)
) ;

CREATE INDEX period ON price (period);

CREATE SEQUENCE announcement_seq;

CREATE TABLE IF NOT EXISTS announcement (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('announcement_seq'),
  uuid varchar(32) NOT NULL,
  title varchar(100) NOT NULL,
  short_description varchar(2000) DEFAULT NULL,
  geolocation_id bigint check (geolocation_id > 0) NOT NULL,
  description_id bigint check (description_id > 0) DEFAULT NULL,
  address_id bigint check (address_id > 0) DEFAULT NULL,
  price_id bigint check (price_id > 0) NOT NULL,
  hidden smallint check (hidden > 0) DEFAULT NULL,
  create_date timestamp(0) NOT NULL,
  update_date timestamp(0) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT uuid UNIQUE  (uuid)
 ,
  CONSTRAINT FK_announcement_address FOREIGN KEY (address_id) REFERENCES address (id),
  CONSTRAINT FK_announcement_announcement_descriptione FOREIGN KEY (description_id) REFERENCES description (id),
  CONSTRAINT FK_announcement_geolocation FOREIGN KEY (geolocation_id) REFERENCES geolocation (id),
  CONSTRAINT FK_announcement_price FOREIGN KEY (price_id) REFERENCES price (id)
) ;

CREATE INDEX FK_announcement_announcement_descriptione ON announcement (description_id);
CREATE INDEX FK_announcement_address ON announcement (address_id);
CREATE INDEX FK_announcement_geolocation ON announcement (geolocation_id);
CREATE INDEX FK_announcement_price ON announcement (price_id);
CREATE INDEX hidden ON announcement (hidden);

CREATE SEQUENCE image_seq;

CREATE TABLE IF NOT EXISTS image (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('image_seq'),
  value mediumblob NOT NULL,
  type varchar(50) NOT NULL,
  name char(36) NOT NULL,
  upload_date timestamp(0) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT name UNIQUE  (name)
) ;

CREATE SEQUENCE image_cache_seq;

CREATE TABLE IF NOT EXISTS image_cache (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('image_cache_seq'),
  value mediumblob NOT NULL,
  name varchar(32) NOT NULL,
  width smallint NOT NULL,
  height smallint NOT NULL,
  type varchar(50) NOT NULL,
  upload_date timestamp(0) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT name UNIQUE  (name,width,height)
)  ;

ALTER SEQUENCE image_cache_seq RESTART WITH 22;

CREATE SEQUENCE announcement_image_seq;

CREATE TABLE IF NOT EXISTS announcement_image (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('announcement_image_seq'),
  announcement_id bigint check (announcement_id > 0) NOT NULL,
  image_id bigint check (image_id > 0) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT announcement_id UNIQUE  (announcement_id,image_id)
 ,
  CONSTRAINT FK_announcement_image_announcement_image FOREIGN KEY (announcement_id) REFERENCES announcement (id),
  CONSTRAINT FK_announcement_image_image FOREIGN KEY (image_id) REFERENCES image (id)
) ;

CREATE INDEX FK_announcement_image_image ON announcement_image (image_id);

CREATE SEQUENCE user_seq;

CREATE TABLE IF NOT EXISTS user (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('user_seq'),
  first_name varchar(50) DEFAULT NULL,
  last_name varchar(50) DEFAULT NULL,
  profile_email varchar(254) NOT NULL,
  password varchar(60) NOT NULL,
  image_id bigint check (image_id > 0) DEFAULT NULL,
  enabled smallint NOT NULL,
  create_date timestamp(0) NOT NULL,
  update_date timestamp(0) NOT NULL,
  PRIMARY KEY (id)
 ,
  CONSTRAINT FK_user_image FOREIGN KEY (image_id) REFERENCES image (id)
) ;

CREATE INDEX FK_user_image ON "user" (image_id);

CREATE SEQUENCE message_seq;

CREATE TABLE IF NOT EXISTS message (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('message_seq'),
  value text NOT NULL,
  user_id bigint check (user_id > 0) NOT NULL,
  create_date timestamp(0) DEFAULT NULL,
  update_date timestamp(0) DEFAULT NULL,
  PRIMARY KEY (id)
 ,
  CONSTRAINT FK_message_user FOREIGN KEY (user_id) REFERENCES user (id)
) ;

CREATE INDEX FK_message_user ON message (user_id);

CREATE SEQUENCE rent_seq;

CREATE TABLE IF NOT EXISTS rent (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('rent_seq'),
  check_in_date timestamp(0) NOT NULL,
  check_out_date timestamp(0) NOT NULL,
  PRIMARY KEY (id)
) ;

CREATE INDEX check_in_date ON rent (check_in_date);
CREATE INDEX check_out_date ON rent (check_out_date);

CREATE SEQUENCE announcement_message_seq;

CREATE TABLE IF NOT EXISTS announcement_message (
  id bigint NOT NULL DEFAULT NEXTVAL ('announcement_message_seq'),
  announcement_id bigint check (announcement_id > 0) NOT NULL,
  message_id bigint check (message_id > 0) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT announcement_id UNIQUE  (announcement_id,message_id)
 ,
  CONSTRAINT FK_announcement_message_announcement FOREIGN KEY (announcement_id) REFERENCES announcement (id),
  CONSTRAINT FK_announcement_message_message FOREIGN KEY (message_id) REFERENCES message (id)
) ;

CREATE INDEX FK_announcement_message_message ON announcement_message (message_id);

CREATE SEQUENCE announcement_rent_seq;

CREATE TABLE IF NOT EXISTS announcement_rent (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('announcement_rent_seq'),
  announcement_id bigint check (announcement_id > 0) NOT NULL,
  rent_id bigint check (rent_id > 0) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT announcement_id UNIQUE  (announcement_id,rent_id)
 ,
  CONSTRAINT FK_announcement_rent_announcement_rent FOREIGN KEY (announcement_id) REFERENCES announcement (id),
  CONSTRAINT FK_announcement_rent_rent FOREIGN KEY (rent_id) REFERENCES rent (id)
) ;

CREATE INDEX FK_announcement_rent_rent ON announcement_rent (rent_id);

CREATE SEQUENCE email_seq;

CREATE TABLE IF NOT EXISTS email (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('email_seq'),
  value varchar(254) DEFAULT NULL,
  PRIMARY KEY (id)
) ;

CREATE SEQUENCE phone_seq;

CREATE TABLE IF NOT EXISTS phone (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('phone_seq'),
  value varchar(200) DEFAULT NULL,
  PRIMARY KEY (id)
) ;

CREATE SEQUENCE user_announcement_seq;

CREATE TABLE IF NOT EXISTS user_announcement (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('user_announcement_seq'),
  user_id bigint check (user_id > 0) NOT NULL,
  announcement_id bigint check (announcement_id > 0) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT user_id UNIQUE  (user_id,announcement_id)
 ,
  CONSTRAINT FK_user_announcement_announcement FOREIGN KEY (announcement_id) REFERENCES announcement (id),
  CONSTRAINT FK_user_announcement_user FOREIGN KEY (user_id) REFERENCES user (id)
) ;

CREATE INDEX FK_user_announcement_announcement ON user_announcement (announcement_id);

CREATE SEQUENCE user_email_seq;

CREATE TABLE IF NOT EXISTS user_email (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('user_email_seq'),
  user_id bigint check (user_id > 0) NOT NULL,
  email_id bigint check (email_id > 0) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT user_settings_id UNIQUE  (user_id,email_id)
 ,
  CONSTRAINT FK_user_email_email FOREIGN KEY (email_id) REFERENCES email (id),
  CONSTRAINT FK_user_email_user FOREIGN KEY (user_id) REFERENCES "user" (id)
) ;

CREATE INDEX FK_user_email_email ON user_email (email_id);

CREATE SEQUENCE user_phone_seq;

CREATE TABLE IF NOT EXISTS user_phone (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('user_phone_seq'),
  user_id bigint check (user_id > 0) NOT NULL,
  phone_id bigint check (phone_id > 0) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT user_settins_id UNIQUE  (user_id,phone_id)
 ,
  CONSTRAINT FK_user_phone_phone FOREIGN KEY (phone_id) REFERENCES phone (id),
  CONSTRAINT FK_user_phone_user FOREIGN KEY (user_id) REFERENCES user (id)
) ;

CREATE INDEX FK_user_phone_phone ON user_phone (phone_id);

CREATE SEQUENCE role_seq;

CREATE TABLE IF NOT EXISTS role (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('role_seq'),
  value varchar(30) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT uni_value UNIQUE  (value)
) ;

CREATE SEQUENCE user_role_seq;

CREATE TABLE IF NOT EXISTS user_role (
  id bigint check (id > 0) NOT NULL DEFAULT NEXTVAL ('user_role_seq'),
  user_id bigint check (user_id > 0) NOT NULL,
  role_id bigint check (role_id > 0) NOT NULL,
  PRIMARY KEY (id)
 ,
  CONSTRAINT FK_user_role_role FOREIGN KEY (role_id) REFERENCES role (id),
  CONSTRAINT FK_user_roles_user FOREIGN KEY (user_id) REFERENCES user (id)
)  ;

ALTER SEQUENCE user_role_seq RESTART WITH 9;

CREATE INDEX FK_user_roles_user ON user_role (user_id);
CREATE INDEX FK_user_roles_roles ON user_role (role_id);
