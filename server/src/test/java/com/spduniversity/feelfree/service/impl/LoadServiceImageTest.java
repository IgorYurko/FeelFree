package com.spduniversity.feelfree.service.impl;

import com.spduniversity.feelfree.config.Application;
import com.spduniversity.feelfree.config.ApplicationProfile;
import com.spduniversity.feelfree.domain.dto.LoadImageDTO;
import com.spduniversity.feelfree.domain.model.Image;
import com.spduniversity.feelfree.domain.repository.ImageRepository;
import com.spduniversity.feelfree.service.common.BlobService;
import com.spduniversity.feelfree.service.web.LoadService;
import com.spduniversity.feelfree.web.bean.LoadImageContainer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@ActiveProfiles(ApplicationProfile.TEST)
@Transactional
public class LoadServiceImageTest {

	@Resource
	private LoadService loadService;

	@Resource
	private ImageRepository imageRepository;

	@Resource
	private BlobService blobService;

	@Test
	public void whenExistImageThenCheckOnNull() throws Exception {
		LoadImageContainer loadImageContainer = new LoadImageContainer();
		loadImageContainer.setName(createImage());
		LoadImageDTO image = loadService.getImage(loadImageContainer);
		Assert.assertNotNull(image);
	}

	@Test
	public void whenNotExistImageThenCheckOnNull() throws Exception {
		LoadImageContainer loadImageContainer = new LoadImageContainer();
		loadImageContainer.setName("no exist name");
		LoadImageDTO image = loadService.getImage(loadImageContainer);
		Assert.assertNull(image);
	}

	@Test(expected = Exception.class)
	public void whenImageNameIsNullThenCheckOnNull() throws Exception {
		loadService.getImage(null);
	}

	private String createImage() {
		Image image = new Image();
		image.setValue(blobService.createBlob(new byte[12]));
		image.setType(MediaType.IMAGE_JPEG_VALUE);
		image.setUploadDate(LocalDateTime.now());
		imageRepository.save(image);

		return image.getName();
	}
}