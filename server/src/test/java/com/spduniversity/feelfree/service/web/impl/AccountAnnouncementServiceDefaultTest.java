package com.spduniversity.feelfree.service.web.impl;

import com.spduniversity.feelfree.config.Application;
import com.spduniversity.feelfree.config.ApplicationProfile;
import com.spduniversity.feelfree.service.web.AccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@ActiveProfiles(ApplicationProfile.DEV)
@Transactional
public class AccountAnnouncementServiceDefaultTest {
	
	@Resource
	private AccountService announcementService;
	
	@Test
	public void saveAnnouncement() throws Exception {
	}
}