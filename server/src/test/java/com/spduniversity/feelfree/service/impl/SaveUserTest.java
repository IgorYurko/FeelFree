package com.spduniversity.feelfree.service.impl;


import com.google.common.collect.Sets;
import com.spduniversity.feelfree.config.Application;
import com.spduniversity.feelfree.config.ApplicationProfile;
import com.spduniversity.feelfree.domain.model.*;
import com.spduniversity.feelfree.domain.repository.AnnouncementRepository;
import com.spduniversity.feelfree.domain.repository.ImageRepository;
import com.spduniversity.feelfree.domain.repository.UserRepository;
import com.spduniversity.feelfree.service.common.BlobService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@ActiveProfiles(ApplicationProfile.DEV)
@Transactional
public class SaveUserTest {
	
	@Resource
	private ImageRepository imageRepository;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private AnnouncementRepository announcementRepository;
	
	@Resource
	private BlobService blobService;
	
	@Test
	public void whenSaveUserThenCheckNotNull() throws Exception {
		
		Image image = getImage();
		User user = new User();
		
		Email email = getEmail("new@email.com");
		String  mainEmail = "mainEmail@gmail.com";
		Phone phone = getPhone("1234567");
		Announcement announcement = getAnnouncement(1L);
		
		user.setEmails(Sets.newHashSet(email));
		user.setAnnouncements(Sets.newHashSet(announcement));
		user.setCreateDate(LocalDateTime.now());
		user.setUpdateDate(LocalDateTime.now());
		user.setFirstName("FirstName");
		user.setLastName("LastName");
		user.setImage(image);
		user.setProfileEmail(mainEmail);
		user.setPhones(Sets.newHashSet(phone));
		user.setPassword("1234567890");
		
		userRepository.save(user);
		Assert.assertNotNull(user.getId());
	}
	
	private Announcement getAnnouncement(Long id) {
		return announcementRepository.findOne(id);
	}
	
	private Phone getPhone(String value) {
		Phone phone = new Phone();
		phone.setValue(value);
		return phone;
	}
	
	private Email getEmail(String value) {
		Email email = new Email();
		email.setValue(value);
		return email;
	}
	
	private Image getImage() {
		Image image = new Image();
		image.setValue(blobService.createBlob(new byte[12]));
		image.setType(MediaType.IMAGE_JPEG_VALUE);
		image.setUploadDate(LocalDateTime.now());
		imageRepository.save(image);
		return image;
	}
}
